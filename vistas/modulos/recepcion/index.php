<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'recepcion' . DS . 'ControlRecepcion.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';
require_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlEmpresa.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlResiduos.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlTipoResiduos.php';

$instancia_recepcion = ControlRecepcion::singleton_recepcion();
$instancia_tipo = ControlTipoResiduo::singleton_tipo_residuo();
$instancia = ControlVehiculo::singleton_vehiculo();
$instancia_residuo = ControlResiduos::singleton_residuo();
$instancia_usuario = ControlUsuario::singleton_usuario();
$instancia_empresa = ControlEmpresa::singleton_empresa();

$datos_vehiculo = $instancia->mostrarDatosVehiculosControl($id_super_empresa);
$datos_tipo = $instancia_tipo->mostrarTipoResiduoControl($id_super_empresa);
$datos_usuario = $instancia_usuario->mostrarDatosUsuariosControl($id_super_empresa);
$datos_empresa = $instancia_empresa->mostrarEmpresasControl($id_super_empresa);

?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						Recepcion de residuos
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
							<div class="dropdown-header">Acciones:</div>
							<a class="dropdown-item" href="<?= BASE_URL ?>programacion/index">Ver recepciones</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST" enctype="multipart/form-data">
						<input type="hidden" value="<?= $_SESSION['id'] ?>" name="id_log">
						<input type="hidden" value="<?= $_SESSION['super_empresa'] ?>" name="super_empresa">
						<div class="row p-3">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Placa del vehiculo</label>
									<input type="text" class="form-control" maxlength="10" minlength="1" name="vehiculo" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Documento del conductor</label>
									<input type="text" class="form-control numeros" maxlength="20" minlength="1" name="doc_conductor" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nombre del conductor</label>
									<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nom_conductor" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Coopiloto (opcional)</label>
									<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nom_coopiloto">
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label>Empresa</label>
									<select name="empresa" class="form-control" id="empresa" required>
										<option value="" selected>Seleccione una opcion...</option>
										<?php
										foreach ($datos_empresa as $empresa) {
											$id_empresa = $empresa['id_empresa'];
											$nombre_empresa = $empresa['nombre'];
											$nit = $empresa['nit'];
											$estado = $empresa['estado'];

											if ($estado == 'activo') {
										?>
												<option value="<?= $id_empresa ?>"><?= $nombre_empresa . ' (' . $nit . ')' ?></option>
										<?php
											}
										}
										?>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Sucursales</label>
									<select name="sucursal" disabled class="form-control" id="sucursales">
										<option value="" selected>Seleccione una opcion...</option>
									</select>
								</div>
							</div>
							<div class="col-lg-6 mb-2">
								<div class="form-group">
									<label>Ciudad</label>
									<input type="text" disabled id="ciudad" class="form-control">
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<div class="accordion" id="accordionExample">
										<?php
										$cont = 0;
										foreach ($datos_tipo as $datos) {

											$id_tipo = $datos['id_tipo'];
											$nombre = $datos['nombre'];
											$estado = $datos['estado'];

											$datos_residuo = $instancia_residuo->mostrarResiduosTipoIdControl($id_tipo);

											if ($estado == 'activo') {
										?>
												<div class="card border-left-danger shadow-none">
													<div class="card-header bg-white text-light" id="headingOne">
														<a href="#" class="text-decoration-none h6 mb-0 text-muted collapsed" data-toggle="collapse" data-target="#residuos<?= $id_tipo ?>" aria-expanded="false" aria-controls="residuos<?= $id_tipo ?>">
															<div class="row no-gutters align-items-center">
																<div class="col mr-2">
																	<?= $nombre ?>
																	<div class="h5 mb-0 font-weight-bold text-gray-800"></div>
																</div>
																<div class="col-auto">
																	<i class="fa fa-biohazard text-danger fa-2x cursor-pointer" data-toggle="collapse" data-target="#residuos<?= $id_tipo ?>" aria-expanded="false" aria-controls="residuos<?= $id_tipo ?>"></i>
																</div>
															</div>
														</a>
													</div>
													<div id="residuos<?= $id_tipo ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
														<div class="card rounded-0">
															<div class="card-body">
																<div class="row">
																	<?php
																	foreach ($datos_residuo as $residuos) {
																		$id_residuo = $residuos['id_residuo'];
																		$nombre = $residuos['nombre'];
																		$codigo = $residuos['codigo'];
																		$estado_residuo = $residuos['estado'];

																		if ($estado_residuo == 'activo') {
																	?>
																			<div class="col-lg-4">
																				<div class="form-group">
																					<div class="custom-control custom-checkbox">
																						<input type="checkbox" class="custom-control-input check" value="<?= $id_residuo ?>" id="<?= $id_residuo ?>" name="id_residuo[]">
																						<label class="custom-control-label" for="<?= $id_residuo ?>"><?= $nombre . ' (' . $codigo . ')' ?></label>
																					</div>
																					&nbsp;
																					<div class="input-group input-group-sm">
																						<input type="text" class="form-control col-lg-4" maxlength="50" disabled minlength="1" id="num_<?= $id_residuo ?>" name="cantidad[]">
																					</div>
																					&nbsp;
																				</div>
																			</div>

																	<?php
																		}
																	}
																	?>
																</div>
															</div>
														</div>
													</div>
												</div>
										<?php
											}
										}
										?>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label>Observaciones</label>
									<textarea class="form-control" name="observacion" maxlength="300" minlength="1"></textarea>
								</div>
							</div>
							<div class="col-lg-12 mt-2">
								<div class="form-group float-right">
									<button class="btn btn-success" type="submit">
										<i class="fa fa-save"></i>
										&nbsp;
										Registrar
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!--Alerta-->
	<div class="modal fade" id="alerta" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog modal-md modal-dialog-centered p-3" role="document">
			<div class="modal-content">
				<div class="modal-body border-0 text-center">
					<i class="fas fa-exclamation-triangle fa-2x text-warning"></i>
					<h4 class="text-warning">Adventercia</h4>
					<p class="mt-3">
						Le recomendamos realizar la reserva desde una computadora para que no se presenten inconvenientes.
					</p>
					<a href="<?= BASE_URL ?>inicio" class="btn btn-warning btn-sm mx-auto">
						Aceptar
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia_recepcion->guardarRecepcionControl();
}
?>
<script src="<?= PUBLIC_PATH ?>js/apartar/funcionesApartar.js"></script>