<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'categorias' . DS . 'ControlTipoCategorias.php';
$id_modulo = 1;

$instancia = ControlTipoCategorias::singleton_tipo_categorias();
$datos_tipo = $instancia->mostrarTipoDestinoControl($id_super_empresa);
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-success">
                        <a href="<?= BASE_URL ?>destinos/index" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-success"></i>
                        </a>
                        &nbsp;
                        Tipos de destino
                    </h4>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Acciones:</div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_tipo_destino">Agregar tipo de destino</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8"></div>
                        <div class="col-lg-4">
                            <form>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control filtro" placeholder="Buscar">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text rounded-right" id="basic-addon1">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-borderless table-sm" width="100%" cellspacing="0">
                                <thead>
                                    <tr class="text-center font-weight-bold">
                                        <th scope="col">#</th>
                                        <th scope="col">Nombre</th>
                                    </tr>
                                </thead>
                                <tbody class="buscar">
                                    <?php
                                    foreach ($datos_tipo as $datos) {
                                        $id_tipo = $datos['id'];
                                        $nombre = $datos['nombre'];
                                        $estado = $datos['activo'];

                                        $ver = ($estado != 0 && $_SESSION['rol'] != 4) ? '' : 'd-none';
                                        $ver_admin = ($_SESSION['rol'] == 4) ? $ver = '' : '';

                                        $act_inac = ($estado == 1) ? 'inactivar_tipo' : 'activar_tipo';
                                        $tooltip = ($estado == 1) ? 'Inactivar tipo de destino' : 'Activar tipo de destino';
                                        $icon = ($estado == 1) ? '<i class="fa fa-times"></i>' : '<i class="fa fa-check"></i>';
                                        $color = ($estado == 1) ? 'danger' : 'success';
                                    ?>
                                        <tr class="text-center <?= $ver ?> <?= $ver_admin ?>" id="tipo_destino<?= $id_tipo; ?>">
                                            <td><?= $id_tipo ?></td>
                                            <td><?= $nombre ?></td>
                                            <td>
                                                <button class="btn btn-sm btn-primary" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" data-target="#editar_tipo<?= $id_tipo ?>" title="Editar tipo de destino" data-trigger="hover">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-<?= $color ?> mr-2 ml-2 <?= $act_inac ?>" id="<?= $id_tipo ?>" data-toggle="tooltip" data-placement="bottom" title="<?= $tooltip ?>" data-trigger="hover">
                                                    <?= $icon ?>
                                                </button>
                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-secondary eliminar_tipo" id="<?= $id_tipo ?>" data-toggle="tooltip" data-placement="bottom" title="Eliminar tipo de destino" data-trigger="hover">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>



                                        <!--Editar tipo destino-->
                                        <div class="modal fade" id="editar_tipo<?= $id_tipo ?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
                                            <div class="modal-dialog modal-md p-2" role="document">
                                                <div class="modal-content">
                                                    <form method="POST">
                                                        <div class="modal-header p-3">
                                                            <h4 class="modal-title text-success font-weight-bold">Ediatr Tipo</h4>
                                                        </div>
                                                        <div class="modal-body border-0">
                                                            <div class="row p-3">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label>Descripcion</label>
                                                                        <input type="hidden" name="id_tipo_edit" value="<?= $id_tipo ?>">
                                                                        <input type="text" class="form-control letras" value="<?= $nombre ?>" maxlength="50" minlength="1" name="descripcion_edit" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer border-0">
                                                            <button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                                            <input type="submit" class="btn btn-success" value="Registrar">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <!---------------------------------- -->
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'tipo_destino' . DS . 'agregarTipo.php';

if (isset($_POST['descripcion'])) {
    $instancia->guardarTipoCategoriaControl();
}


if (isset($_POST['descripcion_edit'])) {
    $instancia->editarTipoCategoriaControl();
}

include_once VISTA_PATH . 'modulos' . DS . 'configuracion' . DS . 'alerta.php';
?>
<script type="text/javascript" src="<?= PUBLIC_PATH ?>js/categorias/funcionesTipoCategoria.js"></script>
<script type="text/javascript" src="<?= PUBLIC_PATH ?>js/configuracion/efectos.js"></script>