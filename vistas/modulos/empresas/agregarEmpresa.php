<!--Agregar empresa-->
<div class="modal fade" id="agregar_empresa" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-lg p-2" role="document">
		<div class="modal-content">
			<form method="POST">
				<input type="hidden" value="<?= $_SESSION['id'] ?>" name="id_log">
				<input type="hidden" value="<?= $_SESSION['super_empresa'] ?>" name="super_empresa">
				<div class="modal-header p-3">
					<h4 class="modal-title text-success font-weight-bold">Agregar Empresa</h4>
				</div>
				<div class="modal-body border-0">
					<div class="row p-3">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Nombre</label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nombre" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Ciudad</label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="ciudad" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Direccion</label>
								<input type="text" class="form-control" maxlength="50" minlength="1" name="direccion" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Nombre contacto</label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nom_contacto" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Telefono</label>
								<input type="text" class="form-control numeros" maxlength="50" minlength="1" name="telefono" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>NIT</label>
								<input type="text" class="form-control" maxlength="50" minlength="1" name="nit" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Tiempo de recoleccion</label>
								<input type="text" class="form-control numeros" maxlength="2" minlength="1" name="tiempo" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Correo electronico</label>
								<input type="text" class="form-control" maxlength="50" minlength="1" name="email" required>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
					<input type="submit" class="btn btn-success" value="Registrar">
				</div>
			</form>
		</div>
	</div>
</div>