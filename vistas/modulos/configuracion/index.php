<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
$id_log = $_SESSION['id'];
$id_modulo = 1;
$instancia_permisos = ControlPermiso::singleton_permiso();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3">
					<h4 class="m-0 font-weight-bold text-success">Configuraci&oacute;n</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<?php
						$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 1, 5);
						if ($permiso) { ?>
							<!--Usuarios-->
							<a class="col-xl-3 col-md-6 mb-4 text-decoration-none" href="<?= BASE_URL ?>usuarios/index">
								<div class="card border-left-success shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Usuarios</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-users fa-2x text-success"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 2, 5);
						if ($permiso) {
						?>
							<!--Vehiculos--->
							<a class="col-xl-3 col-md-6 mb-4 text-decoration-none" href="<?= BASE_URL ?>vehiculos/index">
								<div class="card border-left-primary shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Vehiculos</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-truck-moving fa-2x text-primary"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 3, 5);
						if ($permiso) {
						?>
							<!--Empresas--->
							<a class="col-xl-3 col-md-6 mb-4 text-decoration-none" href="<?= BASE_URL ?>empresas/index">
								<div class="card border-left-warning shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Empresas</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-building fa-2x text-warning"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 4, 5);
						if ($permiso) {
						?>
							<!--sedes--->
							<a class="col-xl-3 col-md-6 mb-4 text-decoration-none" href="<?= BASE_URL ?>sedes/index">
								<div class="card border-left-orange shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Sedes</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-building fa-2x text-orange"></i>
												<i class="far fa-building fa-2x text-orange"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 5, 5);
						if ($permiso) {
						?>
							<!--Residuos--->
							<a class="col-xl-3 col-md-6 mb-4 text-decoration-none" href="<?= BASE_URL ?>residuos/index">
								<div class="card border-left-danger shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Residuos</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-biohazard fa-2x text-danger"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						?>
						<!--Destinos--->
						<a class="col-xl-3 col-md-6 mb-4 text-decoration-none" href="<?= BASE_URL ?>destinos/index">
							<div class="card border-left-blue shadow-sm h-100 py-2">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div class="h5 mb-0 font-weight-bold text-gray-800">Destinos</div>
										</div>
										<div class="col-auto">
											<i class="fas fa-map-marker-alt fa-2x text-blue"></i>
										</div>
									</div>
								</div>
							</div>
						</a>
						<!--Tratamientos--->
						<a class="col-xl-3 col-md-6 mb-4 text-decoration-none" href="<?= BASE_URL ?>tratamientos/index">
							<div class="card border-left-purple shadow-sm h-100 py-2">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div class="h5 mb-0 font-weight-bold text-gray-800">Tratamientos</div>
										</div>
										<div class="col-auto">
											<i class="fas fa-dna fa-2x text-purple"></i>
										</div>
									</div>
								</div>
							</div>
						</a>
						<?php
						$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 6, 5);
						if ($permiso) {
						?>
							<!--Perfiles--->
							<a class="col-xl-3 col-md-6 mb-4 text-decoration-none" href="<?= BASE_URL ?>perfiles/index">
								<div class="card border-left-secondary shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Perfiles</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-user fa-2x text-gray-600"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'configuracion' . DS . 'alerta.php';
?>
<script type="text/javascript" src="<?= PUBLIC_PATH ?>js/configuracion/efectos.js"></script>