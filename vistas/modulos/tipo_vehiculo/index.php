<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlTipoVehiculo.php';
$id_modulo = 1;

$instancia = ControlTipoVehiculo::singleton_tipo_vehiculo();
$datos_tipo = $instancia->mostrarTipoVehiculoControl($id_super_empresa);
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?= BASE_URL ?>vehiculos/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Tipo de Vehiculos
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
							<div class="dropdown-header">Acciones:</div>
							<?php
							$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 16, 6);
							if ($permiso) {
							?>
								<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_tipo">Agregar Tipo</a>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8"></div>
						<div class="col-lg-4">
							<form>
								<div class="form-group">
									<div class="input-group mb-3">
										<input type="text" class="form-control filtro" placeholder="Buscar">
										<div class="input-group-prepend">
											<span class="input-group-text rounded-right" id="basic-addon1">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="table-responsive">
							<table class="table table-hover table-borderless table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold">
										<th scope="col">#</th>
										<th scope="col">Nombre</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									foreach ($datos_tipo as $datos) {
										$id_tipo = $datos['id_tipo'];
										$nombre = $datos['nombre'];
										$estado = $datos['estado'];

										$ver = ($estado != 'inactivo' && $_SESSION['rol'] != 4) ? '' : 'd-none';
										$ver_admin = ($_SESSION['rol'] == 4) ? $ver = '' : '';
									?>
										<tr class="text-center <?= $ver ?> <?= $ver_admin ?>" id="tipo<?= $id_tipo; ?>">
											<td><?= $id_tipo ?></td>
											<td><?= $nombre ?></td>
											<?php
											$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 16, 1);
											if ($permiso) {
											?>
												<td>
													<button class="btn btn-sm btn-primary editar_tipo" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" data-target="#editar_tipo<?= $id_tipo ?>" title="Editar tipo de vehiculo" data-trigger="hover">
														<i class="fa fa-edit"></i>
													</button>
												</td>
												<?php
											}
											$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 16, 2);
											if ($permiso) {
												if ($estado == 'activo') {
												?>
													<td>
														<button class="btn btn-sm btn-danger mr-2 ml-2 inactivar_tipo" id="<?= $id_tipo ?>" data-toggle="tooltip" data-placement="bottom" title="Inactivar tipo de Vehiculo" data-trigger="hover">
															<i class="fa fa-times"></i>
														</button>
													</td>
												<?php
												} else {
												?>
													<td>
														<button class="btn btn-sm btn-success ml-2 mr-2 activar_tipo" id="<?= $id_tipo ?>" data-toggle="tooltip" data-placement="bottom" title="Activar tipo de vehiculo" data-trigger="hover">
															<i class="fa fa-check"></i>
														</button>
													</td>
												<?php
												}
											}
											$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 16, 3);
											if ($permiso) {
												?>
												<td>
													<button class="btn btn-sm btn-secondary eliminar_tipo" id="<?= $id_tipo ?>" data-toggle="tooltip" data-placement="bottom" title="Eliminar tipo de vehiculo" data-trigger="hover">
														<i class="fa fa-trash"></i>
													</button>
												</td>
											<?php
											}
											?>
										</tr>

										<!--Editar usuario-->
										<div class="modal fade" id="editar_tipo<?= $id_tipo ?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
											<div class="modal-dialog modal-md p-2" role="document">
												<div class="modal-content">
													<form method="POST">
														<input type="hidden" name="id_tipo" value="<?= $id_tipo ?>">
														<div class="modal-header p-3">
															<h4 class="modal-title text-success font-weight-bold">Editar tipo de vehiculo</h4>
														</div>
														<div class="modal-body border-0">
															<div class="row  p-3">
																<div class="col-lg-12">
																	<div class="form-group">
																		<label>Nombre</label>
																		<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nombre_edit" value="<?= $nombre ?>">
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
															<input type="submit" class="btn btn-success" value="Guardar">
														</div>
													</form>
												</div>
											</div>
										</div>
									<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'tipo_vehiculo' . DS . 'agregarTipoVehiculo.php';

if (isset($_POST['nombre'])) {
	$instancia->registrarTipoVehiculoControl();
}

if (isset($_POST['nombre_edit'])) {
	$instancia->editarTipoVehiculoControl();
}
include_once VISTA_PATH . 'modulos' . DS . 'configuracion' . DS . 'alerta.php';
?>
<script type="text/javascript" src="<?= PUBLIC_PATH ?>js/configuracion/efectos.js"></script>
<script type="text/javascript" src="<?= PUBLIC_PATH ?>js/vehiculo/funcionesTipoVehiculo.js"></script>