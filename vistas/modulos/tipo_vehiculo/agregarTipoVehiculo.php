
<!--Agregar usuario-->
<div class="modal fade" id="agregar_tipo" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-md p-2" role="document">
		<div class="modal-content">
			<form method="POST" id="form_enviar">
				<div class="modal-header p-3">
					<h4 class="modal-title text-success font-weight-bold">Agregar tipo de vehiculo</h4>
				</div>
				<div class="modal-body border-0">
					<div class="row p-3">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Nombre</label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nombre" required>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
					<input type="submit" class="btn btn-success" value="Registrar">
				</div>
			</form>
		</div>
	</div>
</div>