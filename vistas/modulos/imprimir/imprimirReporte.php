<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'reportes' . DS . 'ControlReportes.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlTipoVehiculo.php';
require_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';

$instancia = ControlReportes::singleton_reportes();
$instancia_vehiculo = ControlVehiculo::singleton_vehiculo();
$instancia_tipo_vehiculo = ControlTipoVehiculo::singleton_tipo_vehiculo();
$instancia_usuario = ControlUsuario::singleton_usuario();
$instancia_permisos = ControlPermiso::singleton_permiso();

$id_super_empresa = $_SESSION['super_empresa'];

$datos_super_empresa = $instancia_permisos->datosSuperEmpresaControl($id_super_empresa);
if (isset($_GET['vehiculo'])) {

	$id_vehiculo = base64_decode($_GET['vehiculo']);

	$datos_reporte = $instancia->MostrarReportesIdControl($id_vehiculo, $id_super_empresa);
	$id_reporte = $datos_reporte['id_reporte'];
	$tipo_mant = $datos_reporte['tipo_mant'];
	$observacion = $datos_reporte['observacion'];
	$fecha_reportado = $datos_reporte['fecha_reportado'];
	$id_user = $datos_reporte['id_log'];

	$datos_tipo_mant = $instancia->mostrarTipoEstadoIdControl($tipo_mant);
	$nombre_tipo = $datos_tipo_mant['nombre'];

	$datos_vehiculo = $instancia_vehiculo->mostrarDatosVehiculosIdControl($id_vehiculo);
	$descripcion = $datos_vehiculo['descripcion'];
	$placa = $datos_vehiculo['placa'];
	$marca = $datos_vehiculo['marca'];
	$modelo = $datos_vehiculo['modelo'];
	$estado_vehiculo = $datos_vehiculo['estado'];
	$tipo_vehiculo = $datos_vehiculo['id_tipo'];


	$datos_tipo_vehiculo = $instancia_tipo_vehiculo->mostrarTipoVehiculoIdControl($tipo_vehiculo, $id_super_empresa);
	$nombre_tipo_vehiculo = $datos_tipo_vehiculo['nombre'];

	$datos_usuario = $instancia_usuario->mostrarDatosUsuariosIdControl($id_user, $id_super_empresa);
	$nombre_usuario = $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'];
	$documento_usuario = $datos_usuario['documento'];


	class MYPDF extends TCPDF
	{
		public function Header()
		{
			$this->setJPEGQuality(90);
			$this->Image(PUBLIC_PATH . 'img/encabezado.png', 5, 5, 200);
		}

		/*public function Footer() {
			$this->SetY(-15);
			$this->SetFillColor(127);
			$this->SetTextColor(127);
			$this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
			$this->Cell(0,10,'Pagina '.$this->PageNo(),0,0,'C');
		}*/
	}


	// create a PDF object
	$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	// set document (meta) information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Jesus Polo');
	$pdf->SetTitle('Reporte');
	$pdf->SetSubject('Reporte');
	$pdf->SetKeywords('Reporte');
	$pdf->AddPage();


	$pdf->Ln(40);
	$pdf->Cell(3);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(18, 5, 'Nombre: ', 0, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(35, 5, $nombre_usuario, 0, 0, 'L');

	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(18, 5, 'Empresa: ', 0, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(68, 5, $datos_super_empresa{
	'nombre'}, 0, 0, 'L');


	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(14, 5, 'Fecha: ', 0, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(40, 5, $fecha_reportado, 0, 0, 'L');


	$pdf->Ln(15);
	$pdf->Cell(3);
	$html = '
	<table border="1" align="center" cellpadding="3">
	<tr style="font-weight:bold;">
	<th>ID</th>
	<th colspan="2">Descripcion - Placa</th>
	<th>Marca</th>
	<th>Estado</th>
	<th>Observacion</th>
	</tr>
	<tr>
	<td>' . $id_vehiculo . '</td>
	<td colspan="2">' . $descripcion . " - " . $placa . '</td>
	<td>' . $marca . '</td>
	<td>' . $nombre_tipo . '</td>
	<td>' . $observacion . '</td>
	</tr>	
	<tr>
	<td></td>
	<td colspan="2"></td>
	<td></td>
	<td></td>
	<td></td>
	</tr>	
	<tr>
	<td></td>
	<td colspan="2"></td>
	<td></td>
	<td></td>
	<td></td>
	</tr>	
	</table>
	';

	$pdf->writeHTML($html, true, false, true, false, '');

	$pdf->Ln(5);
	$pdf->Cell(25);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(15, 5, $nombre_usuario, 0, 0, 'C');

	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Ln(-2);
	$pdf->Cell(65, 12, '__________________________', 0, 0, 'C');
	$pdf->Cell(65, 12, '__________________________', 0, 0, 'C');
	$pdf->Cell(65, 12, '__________________________', 0, 0, 'C');
	$pdf->Ln(10);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(65, 12, 'Reporte Realizado Por', 0, 0, 'C');
	$pdf->Cell(65, 12, 'V°B° Directora Administrativa', 0, 0, 'C');
	$pdf->Cell(65, 12, 'Reporte Remitido a', 0, 0, 'C');


	$pdf->Ln(20);
	$pdf->Cell(3);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(40, 5, 'Solicitud Recibida Por: ', 0, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(50, 5, '________________________', 0, 0, 'L');


	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(10, 5, 'Hora: ', 0, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(30, 5, '______________', 0, 0, 'L');


	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(15, 5, 'Fecha: ', 0, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(20, 5, '___________________', 0, 0, 'L');


	$pdf->Ln(8);
	$pdf->Cell(3);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(30, 5, 'Solucionado Por: ', 0, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(90, 5, '_________________________________________', 0, 0, 'L');


	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(15, 5, 'Fecha: ', 0, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(20, 5, '________________________', 0, 0, 'L');


	$pdf->Ln(8);
	$pdf->Cell(3);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(41, 5, 'Recibido Conforme Por: ', 0, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(80, 5, '______________________________________', 0, 0, 'L');


	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(15, 5, 'Fecha: ', 0, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(20, 5, '________________________', 0, 0, 'L');

	$pdf->Output('Reporte.pdf');
}
