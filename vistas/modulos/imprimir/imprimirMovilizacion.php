<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'programacion' . DS . 'ControlProgramacion.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlEmpresa.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlTipoVehiculo.php';
require_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlSede.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlResiduos.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlTipoResiduos.php';
require_once CONTROL_PATH . 'recepcion' . DS . 'ControlRecepcion.php';

$instancia = ControlProgramacion::singleton_programacion();
$instancia_empresa = ControlEmpresa::singleton_empresa();
$instancia_vehiculo = ControlVehiculo::singleton_vehiculo();
$instancia_tipo_vehiculo = ControlTipoVehiculo::singleton_tipo_vehiculo();
$instancia_usuario = ControlUsuario::singleton_usuario();
$instancia_sede = ControlSede::singleton_sede();
$instancia_tipo = ControlTipoResiduo::singleton_tipo_residuo();
$instancia_residuo = ControlResiduos::singleton_residuo();
$instancia_recepcion = ControlRecepcion::singleton_recepcion();
$id_super_empresa = $_SESSION['super_empresa'];

if (isset($_GET['reserva']) && isset($_GET['fecha_ini'])) {

	$id_reserva = base64_decode($_GET['reserva']);
	$fecha = base64_decode($_GET['fecha_ini']);

	$datos_consecutivo = $instancia->mostrarConsecutivoReservaControl($id_reserva);
	$consecutivo = $datos_consecutivo['numero'];

	$buscar = $instancia->mostrarProgramacionIdControl($id_reserva);

	$id_empresa = $buscar['id_empresa'];
	$id_vehiculo = $buscar['id_vehiculo'];
	$id_conductor = $buscar['id_conductor'];
	$id_sucursal = $buscar['id_sucursal'];
	$id_user = $buscar['id_user'];
	$hora_inicio = $buscar['hora_inicio'];
	$hora_fin = $buscar['hora_fin'];
	$fecha_apartado = $buscar['fecha_apartado'];
	$observacion = $buscar['observacion'];
	$confirmado = $buscar['confirmado'];
	$recepcion = $buscar['recepcion'];

	if ($id_sucursal != 0) {

		$datos_empresa = $instancia_sede->mostrarSedeIdControl($id_sucursal);
		$nombre_empresa = $datos_empresa['nombre'];
		$direccion_empresa = $datos_empresa['direccion'];
		$ciudad_empresa = $datos_empresa['ciudad'];
		$nom_contacto_empresa = $datos_empresa['nom_contacto'];
		$telefono_empresa = $datos_empresa['telefono'];
		$estado_empresa = $datos_empresa['estado'];
		$id_empresa_sucursal = $datos_empresa['id_empresa'];
		$datos_empresa_nit = $instancia_empresa->mostrarEmpresaIdControl($id_empresa_sucursal);
		$nit = $datos_empresa_nit['nit'];
		$sede = 'X';
	} else {
		$datos_empresa = $instancia_empresa->mostrarEmpresaIdControl($id_empresa);
		$nombre_empresa = $datos_empresa['nombre'];
		$direccion_empresa = $datos_empresa['direccion'];
		$ciudad_empresa = $datos_empresa['ciudad'];
		$nom_contacto_empresa = $datos_empresa['nom_contacto'];
		$telefono_empresa = $datos_empresa['telefono'];
		$estado_empresa = $datos_empresa['estado'];
		$nit = $datos_empresa['nit'];
		$sede = '';
	}


	$datos_vehiculo = $instancia_vehiculo->mostrarDatosVehiculosIdControl($id_vehiculo);
	$decripcion = $datos_vehiculo['descripcion'];
	$placa = $datos_vehiculo['placa'];
	$marca = $datos_vehiculo['marca'];
	$modelo = $datos_vehiculo['modelo'];
	$estado_vehiculo = $datos_vehiculo['estado'];
	$tipo_vehiculo = $datos_vehiculo['id_tipo'];


	$datos_tipo_vehiculo = $instancia_tipo_vehiculo->mostrarTipoVehiculoIdControl($tipo_vehiculo, $id_super_empresa);
	$nombre_tipo_vehiculo = $datos_tipo_vehiculo['nombre'];

	$datos_resiudo_reserva = $instancia->mostrarResiduosProgramacionControl($id_reserva);
	$datos_tipo_residuo = $instancia_tipo->mostrarTipoResiduoControl($id_super_empresa);

	if ($recepcion == 0) {
		$datos_conductor = $instancia_usuario->mostrarDatosUsuariosIdControl($id_conductor);
		$nombre_conductor = $datos_conductor['nombre'] . ' ' . $datos_conductor['apellido'];
		$documento_conductor = $datos_conductor['documento'];
	} else {
		$datos_recepcion = $instancia_recepcion->mostrarDatosRecepcionControl($id_reserva, $id_super_empresa);
		$nombre_conductor = $datos_recepcion['nom_conductor'];
		$documento_conductor = $datos_recepcion['doc_conductor'];
		$placa = $datos_recepcion['placa_vehiculo'];
		$nombre_tipo_vehiculo = 'No registrado';
	}

	$datos_usuario = $instancia_usuario->mostrarDatosUsuariosIdControl($id_user);
	$nombre_usuario = $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'];
	$documento_usuario = $datos_usuario['documento'];


	class MYPDF extends TCPDF
	{
		public function Header()
		{
			$this->setJPEGQuality(90);
			$this->Image(PUBLIC_PATH . 'img/logo.png', 15, 10, 35);
		}

		public function Footer()
		{
			$this->SetY(-15);
			$this->SetFillColor(127);
			$this->SetTextColor(127);
			$this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
			$this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
		}
	}


	// create a PDF object
	$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	// set document (meta) information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Jesus Polo');
	$pdf->SetTitle('Movilizacion');
	$pdf->SetSubject('Movilizacion');
	$pdf->SetKeywords('Movilizacion');
	$pdf->AddPage();

	$pdf->Cell(50);
	$pdf->SetFont('helvetica', '', 6);
	$pdf->SetTextColor(206, 205, 205);
	$pdf->MultiCell(50, 5, 'NIT: 900.226.891-1
		Resolución Ambiental 0978 de Sep 10/2009
		Cra 15 Sur No. 80 - 132 AV. Circunvalar • Tel: (5) 385 5610
		Cel: 320 543 0535 - 320 543 1698
		www.ecogreenrecycling.com • Barranquilla - Colombia.', 0, 'C', 0, 0, '', '', true);


	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->SetTextColor(0, 0, 0);
	$pdf->Cell(80, 12, 'CERTIFICADO DE MOVILIZACION', 0, 0, 'C');

	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 8);
	$pdf->Cell(8, 12, 'Version 1', 0, 0, 'C');

	$pdf->Ln(8);
	$pdf->Cell(135);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 12);
	$pdf->Cell(10, 12, 'No. ' . $consecutivo, 0, 0, 'C');

	$pdf->Ln(25);

	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(10);
	$pdf->Cell(8, 6, '', 1, 0, 'C');
	$pdf->Cell(35, 6, 'Aceite Usados', 1, 0, 'C');
	$pdf->Cell(8, 6, '', 1, 0, 'C');
	$pdf->Cell(40, 6, 'Peligrosos Industriales', 1, 0, 'C');
	$pdf->Cell(8, 6, '', 1, 0, 'C');
	$pdf->Cell(35, 6, 'Recuperables', 1, 0, 'C');
	$pdf->Cell(18);
	$pdf->Cell(15, 6, 'Sede', 1, 0, 'C');
	$pdf->Cell(8, 6, $sede, 1, 0, 'C');

	$pdf->Ln(15);
	$pdf->Cell(18);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(10, 6, 'Datos del generador:', 0, 0, 'C');

	$pdf->Ln(10);
	$pdf->Cell(10);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(15, 6, 'Fecha: ', 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(60, 6, $fecha, 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(20, 6, 'Ciudad: ', 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(80, 6, $ciudad_empresa, 1, 0, 'L');


	$pdf->Ln(8);
	$pdf->Cell(10);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(20, 6, 'Nombre: ', 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(70, 6, $nombre_empresa, 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(15, 6, 'Nit: ', 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(70, 6, $nit, 1, 0, 'L');


	$pdf->Ln(8);
	$pdf->Cell(10);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(20, 6, 'Direccion: ', 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(60, 6, $direccion_empresa, 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(20, 6, 'Telefono: ', 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(75, 6, $telefono_empresa, 1, 0, 'L');


	$pdf->Ln(15);
	$pdf->Cell(25);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(10, 6, 'Responsable de la operacion:', 0, 0, 'C');

	$pdf->Ln(10);
	$pdf->Cell(10);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(50, 6, 'Nombre de quien entrega: ', 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(80, 6, $nombre_usuario, 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(10, 6, 'C.C: ', 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(35, 6, $documento_usuario, 1, 0, 'L');

	$pdf->Ln(20);
	$pdf->Cell(10);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(175, 6, 'TIPOS DE ACEITE Y RESIDUO', 1, 0, 'C');

	$pdf->Ln(6);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(10);

	foreach ($datos_tipo_residuo as $datos) {
		$id_tipo_residuo = $datos['id_tipo'];
		$nombre = $datos['nombre'];

		$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
		$pdf->Cell(58.35, 6, $nombre, 1, 0, 'C');
	}

	$pdf->Ln(6);
	$pdf->Cell(10);

	$tabla = '
	<table style="font-size:8.5px; width:97.3%;" cellpadding="4">
	';

	foreach ($datos_resiudo_reserva as $residuos) {

		$id_residuo = $residuos['id_residuo'];
		$cantidad = $residuos['cantidad'];
		$nombre_residuo = $residuos['residuo'];
		$codigo = $residuos['codigo_residuo'];
		$id_tipo = $residuos['id_tipo_residuo'];

		if ($id_tipo == 2) {
			$td = '
			<td style="border: 1px solid black; border-collapse: collapse;">' . $nombre_residuo . ' (' . $codigo . ') = ' . $cantidad . '</td>
			<td ' . $colsapn . '></td>
			<td ' . $colsapn . '></td>
			';
		}
		if ($id_tipo == 3) {
			$td = '
			<td ' . $colsapn . '></td>
			<td style="border: 1px solid black; border-collapse: collapse;">' . $nombre_residuo . ' (' . $codigo . ') = ' . $cantidad . '</td>
			<td ' . $colsapn . '></td>
			';
		}
		if ($id_tipo == 4) {
			$td = '
			<td ' . $colsapn . '></td>
			<td ' . $colsapn . '></td>
			<td style="border: 1px solid black; border-collapse: collapse;">' . $nombre_residuo . ' (' . $codigo . ') = ' . $cantidad . '</td>
			';
		}


		$tabla .= '
		<tr align="center">
			' . $td . '
		</tr>
		';
	}

	$tabla .= '
	</table>
	';

	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->writeHTML($tabla, true, false, true, false, '');


	$pdf->Ln(15);
	$pdf->Cell(20);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(10, 6, 'Datos del Movilizador:', 0, 0, 'C');

	$pdf->Ln(8);
	$pdf->Cell(10);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(50, 6, 'Nombre del transportador: ', 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(80, 6, $nombre_conductor, 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(10, 6, 'C.C: ', 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(35, 6, $documento_conductor, 1, 0, 'L');

	$pdf->Ln(8);
	$pdf->Cell(10);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(20, 6, 'Placa: ', 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(30, 6, $placa, 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(40, 6, 'Tipo de vehiculo: ', 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(85, 6, $nombre_tipo_vehiculo, 1, 0, 'L');

	$pdf->Ln(8);
	$pdf->Cell(10);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(50, 20, 'Observaciones: ', 1, 0, 'L');
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->MultiCell(125, 20, $observacion, 1, 'L', 0, 0, '', '', true);


	$pdf->Ln(40);
	$pdf->Cell(90, 12, '_______________________________________', 0, 0, 'C');
	$pdf->Ln(8);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
	$pdf->Cell(80, 12, 'Firma de quien recibe', 0, 0, 'C');

	$pdf->Output('Movilizacion.pdf');
}
