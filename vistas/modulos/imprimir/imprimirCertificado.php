<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'programacion' . DS . 'ControlProgramacion.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlEmpresa.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlTipoVehiculo.php';
require_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlSede.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlResiduos.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlTipoResiduos.php';
require_once CONTROL_PATH . 'numeros.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';

$instancia               = ControlProgramacion::singleton_programacion();
$instancia_empresa       = ControlEmpresa::singleton_empresa();
$instancia_vehiculo      = ControlVehiculo::singleton_vehiculo();
$instancia_tipo_vehiculo = ControlTipoVehiculo::singleton_tipo_vehiculo();
$instancia_usuario       = ControlUsuario::singleton_usuario();
$instancia_sede          = ControlSede::singleton_sede();
$instancia_tipo          = ControlTipoResiduo::singleton_tipo_residuo();
$instancia_residuo       = ControlResiduos::singleton_residuo();
$instancia_permisos      = ControlPermiso::singleton_permiso();

$id_super_empresa = $_SESSION['super_empresa'];

$datos_super_empresa = $instancia_permisos->datosSuperEmpresaControl($id_super_empresa);

if (isset($_GET['reserva']) && isset($_GET['fecha'])) {

    $id_reserva = base64_decode($_GET['reserva']);
    $fecha      = base64_decode($_GET['fecha']);

    $datos_resiudo_reserva = $instancia->mostrarResiduosProgramacionControl($id_reserva);

    $fecha_separada = strtotime($datos_resiudo_reserva[0]['fechareg']);

    $anio_vence = date('Y', $fecha_separada);
    $mes        = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
    $mes_vence  = $mes[(date('m', strtotime($fecha)) * 1) - 1];
    $dia_vence  = date('d', $fecha_separada);

    $buscar         = $instancia->mostrarProgramacionIdControl($id_reserva);
    $cantidad_total = $instancia->sumarCantidadResiduosReservaControl($id_reserva);

    $id_empresa     = $buscar['id_empresa'];
    $id_vehiculo    = $buscar['id_vehiculo'];
    $id_conductor   = $buscar['id_conductor'];
    $id_sucursal    = $buscar['id_sucursal'];
    $id_user        = $buscar['id_user'];
    $hora_inicio    = $buscar['hora_inicio'];
    $hora_fin       = $buscar['hora_fin'];
    $fecha_apartado = $buscar['fecha_apartado'];
    $observacion    = $buscar['observacion'];
    $confirmado     = $buscar['confirmado'];
    $recepcion      = $buscar['recepcion'];

    if ($id_sucursal != 0) {

        $datos_sede          = $instancia_sede->mostrarSedeIdControl($id_sucursal);
        $nombre_sede         = $datos_sede['nombre'];
        $direccion_sede      = $datos_sede['direccion'];
        $ciudad              = $datos_sede['ciudad'];
        $nom_contacto_sede   = $datos_sede['nom_contacto'];
        $telefono_sede       = $datos_sede['telefono'];
        $estado_sede         = $datos_sede['estado'];
        $id_empresa_sucursal = $datos_sede['id_empresa'];
    } else {
        $datos_empresa = $instancia_empresa->mostrarEmpresaIdControl($id_empresa);
        $ciudad        = $datos_empresa['ciudad'];
    }

    $datos_empresa        = $instancia_empresa->mostrarEmpresaIdControl($id_empresa);
    $nombre_empresa       = $datos_empresa['nombre'];
    $direccion_empresa    = $datos_empresa['direccion'];
    $nom_contacto_empresa = $datos_empresa['nom_contacto'];
    $telefono_empresa     = $datos_empresa['telefono'];
    $estado_empresa       = $datos_empresa['estado'];
    $nit                  = $datos_empresa['nit'];
    $sede                 = '';

    $datos_vehiculo  = $instancia_vehiculo->mostrarDatosVehiculosIdControl($id_vehiculo);
    $decripcion      = $datos_vehiculo['descripcion'];
    $placa           = $datos_vehiculo['placa'];
    $marca           = $datos_vehiculo['marca'];
    $modelo          = $datos_vehiculo['modelo'];
    $estado_vehiculo = $datos_vehiculo['estado'];
    $tipo_vehiculo   = $datos_vehiculo['id_tipo'];

    $datos_tipo_vehiculo  = $instancia_tipo_vehiculo->mostrarTipoVehiculoIdControl($tipo_vehiculo, $id_super_empresa);
    $nombre_tipo_vehiculo = $datos_tipo_vehiculo['nombre'];

    $datos_tipo_residuo = $instancia_tipo->mostrarTipoResiduoControl($id_super_empresa);

    $datos_conductor     = $instancia_usuario->mostrarDatosUsuariosIdControl($id_conductor);
    $nombre_conductor    = $datos_conductor['nombre'] . ' ' . $datos_conductor['apellido'];
    $documento_conductor = $datos_conductor['documento'];

    $datos_usuario     = $instancia_usuario->mostrarDatosUsuariosIdControl($id_user);
    $nombre_usuario    = $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'];
    $documento_usuario = $datos_usuario['documento'];

    $datos_consecutivo = $instancia->mostrarConsecutivoCertReservaControl($id_reserva);
    $numero            = $datos_consecutivo['numero'];
    $nota              = $datos_consecutivo['nota'];

    $datos_consecutivo_movilizacion = $instancia->mostrarConsecutivoReservaControl($id_reserva);
    $numero_movilizacion            = ($datos_consecutivo_movilizacion['numero'] == '') ? 0 : $datos_consecutivo_movilizacion['numero'];

    class MYPDF extends TCPDF
    {
        public function Header()
        {
            $this->setJPEGQuality(90);
            $this->Image(PUBLIC_PATH . 'img/logo.png', 20, 10, 35);
            $this->setJPEGQuality(90);
            $this->Image(PUBLIC_PATH . 'img/iso.jpg', 150, 8, 35);
            $this->Ln(10);
            $this->Cell(90);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'B', 15);
            $this->Cell(12, 50, 'CERTIFICADO DE DISPOSICION FINAL', 0, 0, 'C');
        }
    }

    $pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Jesus Polo');
    $pdf->SetTitle('Certificado');
    $pdf->SetSubject('Certificado');
    $pdf->SetKeywords('Certificado');
    $pdf->AddPage();

    $pdf->Ln(10);
    $pdf->Cell(90);
    $pdf->Cell(12, 50, 'No. ' . $numero, 0, 0, 'C');

    $parrafo = '
    <p style="text-align:justify;">
    <span style="font-weight:bold;">' . $datos_super_empresa['nombre'] . ', NIT ' . $datos_super_empresa['nit'] . '.</span>
    Empresa legalmente autorizada como gestor de residuos
    peligrosos por el Departamento Técnico Administrativo del Medio Ambiente DAMAB- BARRANQUILLA,
    actualmente Establecimiento Público Ambiental- BARRANQUILLA VERDE mediante resolución No. 0978 del 10
    de septiembre de 2009, modificada por resolución No.1845 del 20 de Octubre de 2014 que autoriza la recolección,
    transporte, almacenamiento, tratamiento, aprovechamiento, disposición final de residuos sólidos peligrosos-fluidos
    aceitosos, certifica que ha recibido de la empresa <span style="font-weight:bold;">' . $nombre_empresa . '</span>
    identificada con NIT. <span style="font-weight:bold;">' . $nit . '</span> con sede en la ciudad de
    <span style="font-weight:bold;">' . $ciudad . '</span>, el siguiente material:</p>
    ';

    $pdf->Ln(40);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->Cell(10);
    $pdf->writeHTMLCell(170, 0, '', '', $parrafo, '', 1, 0, true, 'C', true);

    /*     $pdf->Ln(10);
    $pdf->Cell(10);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
    $pdf->Cell(170, 8, 'CERTIFICADO DE MOVILIZACION No. (' . $numero_movilizacion . ') FECHA: ' . $fecha, 1, 0, 'C'); */

    $pdf->Ln(8);
    $pdf->Cell(10);

    $tabla = '
    <table border="1" style="font-size:8.5px; width:93%;">
    <tr style="text-align:center; font-weight:bold;">
    <th>SERVICIO</th>
    <th>TIPO RESIDUO</th>
    <tr style="width:8%; text-align:center;">
    <th>Kg</th>
    <th>Gl</th>
    </tr>
    <th colspan="2" style="width:23%;">CANT.</th>
    <th>SEDE</th>
    <th style="width:10%;">CODIGO DE RESIDUO</th>
    <th style="width:15%;">TRATAMIENTO</th>
    <th style="width:15%;">DISPOSICION FINAL</th>
    </tr>
    ';

    foreach ($datos_resiudo_reserva as $residuos) {

        $servicio     = ($recepcion == 1) ? $residuos['servicio_recepcion'] : $residuos['servicio'];
        $tipo_residuo = $residuos['residuo'];
        $kilo         = $residuos['kilogramos'];
        $galones      = $residuos['galones'];
        $sede         = $residuos['ciudad'];
        $codigo       = $residuos['codigo_residuo'];
        $tratamiento  = $residuos['tratamiento'];
        $dispsicion   = $residuos['disposicion'];

        $tabla .= '
        <tr align="center">
        <td>' . $servicio . '</td>
        <td>' . $tipo_residuo . '</td>
        <td>' . $kilo . '</td>
        <td>' . $galones . '</td>
        <td>' . $sede . '</td>
        <td>' . $codigo . '</td>
        <td>' . $tratamiento . '</td>
        <td>' . $dispsicion . '</td>
        </tr>
        ';
    }

    $tabla .= '
    </table>
    ';

    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($tabla, true, false, true, false, '');

    $parrafo = '
    <p style="text-align:justify;">
    <span style="font-weight:bold;">Nota aclaratoria: </span>
    ' . $nota . '
    </p>
    ';

    $ln = 2;

    $pdf->Ln($ln + 5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->Cell(10);
    $pdf->writeHTMLCell(170, 0, '', '', $parrafo, '', 1, 0, true, 'C', true);

    $pdf->Ln($ln + 8);
    $pdf->Cell(10);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $html = "El peso total de residuos anteriormente listados es de " . convertirNumeroLetra($cantidad_total['valor_total']) . " kilogramos (" . $cantidad_total['valor_total'] . " kg).";
    $pdf->writeHTMLCell(170, 0, '', '', $html, '', 1, 0, true, 'L', true);

    $parrafo = '
    <p style="text-align:justify;">
    De acuerdo con el Decreto 1076 de 2015 del Ministerio de Ambiente, la responsabilidad de ECOGREEN
    RECYCLING SAS. Como receptor de los residuos peligrosos es solidaria con el generador y subsiste hasta tanto
    se obtiene el certificado de aprovechamiento/ eliminación/ disposición final definitiva de los residuos mencionados.</p>
    ';

    $pdf->Ln($ln + 2);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->Cell(10);
    $pdf->writeHTMLCell(170, 0, '', '', $parrafo, '', 1, 0, true, 'C', true);

    $pdf->Ln($ln);
    $pdf->Cell(10);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->Cell(10, 5, 'Se expide el presente certificado a los (' . $dia_vence . ') días del mes de ' . $mes_vence . ' de ' . $anio_vence . '. ', 0, 0, 'L');

    $pdf->setJPEGQuality(90);
    $pdf->Image(PUBLIC_PATH . 'img/firma.png', 25, 220 + $ln, 35, '', '', '', 'B', false, 30, '', false, false, 0, false, false, false);

    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 11);
    $pdf->Ln($ln - 7);
    $pdf->Cell(10);
    $pdf->Cell(120, 2, '___________________________', 0, 0, 'L');

    $pdf->Ln($ln + 2);
    $pdf->Cell(10);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
    $pdf->Cell(120, 2, 'ÁLVARO PADILLA', 0, 0, 'L');

    $pdf->Ln($ln + 2);
    $pdf->Cell(10);
    $pdf->Cell(120, 2, 'GERENTE ', 0, 0, 'L');
    $pdf->Output('Certificado_Disposicion_Final.pdf');
}
