<!--Agregar empresa-->
<div class="modal fade" id="agregar_sede" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-lg p-2" role="document">
		<div class="modal-content">
			<form method="POST">
				<input type="hidden" value="<?= $_SESSION['id'] ?>" name="id_log">
				<div class="modal-header p-3">
					<h4 class="modal-title text-success font-weight-bold">Agregar Sede</h4>
				</div>
				<div class="modal-body border-0">
					<div class="row p-3">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Empresa</label>
								<select name="id_empresa" class="form-control" id="id_empresa" required>
									<option value="" selected>Seleccione una opcion...</option>
									<?php
									foreach ($datos_empresa as $datos) {
										$id_empresa = $datos['id_empresa'];
										$nombre = $datos['nombre'];
										$nit = $datos['nit'];
									?>
										<option value="<?= $id_empresa ?>"><?= $nombre ?></option>
									<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Nombre</label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nombre_sede" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Ciudad</label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="ciudad_sede" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Direccion</label>
								<input type="text" class="form-control" maxlength="50" minlength="1" name="direccion_sede" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Nombre contacto</label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nom_contacto_sede" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Telefono</label>
								<input type="text" class="form-control numeros" maxlength="50" minlength="1" name="telefono_sede" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Nit</label>
								<input type="text" class="form-control" maxlength="30" minlength="1" name="nit" id="nitE" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Tiempo de recoleccion</label>
								<input type="text" class="form-control numeros" maxlength="2" minlength="1" name="tiempo_sede" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Correo electronico</label>
								<input type="text" class="form-control" maxlength="50" minlength="1" name="email_sede" required>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
					<input type="submit" class="btn btn-success" value="Registrar">
				</div>
			</form>
		</div>
	</div>
</div>