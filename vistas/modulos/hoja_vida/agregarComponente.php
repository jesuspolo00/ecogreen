<!--Agregar componente-->
<div class="modal fade" id="agregar_componente" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-md p-2" role="document">
		<div class="modal-content">
			<form method="POST">
				<input type="hidden" value="<?= $_SESSION['id']?>" name="id_log">
				<input type="hidden" value="<?= $id_vehiculo?>" name="id_vehiculo">
				<div class="modal-header p-3">
					<h4 class="modal-title text-success font-weight-bold">Agregar Componente</h4>
				</div>
				<div class="modal-body border-0">
					<div class="row p-3">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Descripcion</label>
								<input type="text" class="form-control letras" maxlength="20" minlength="1" name="descripcion_componente" required>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Fabricante</label>
								<input type="text" class="form-control letras" maxlength="20" minlength="1" name="fabricante" required>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Licencia</label>
								<input type="text" class="form-control letras" maxlength="20" minlength="1" name="licencia" required>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Fecha</label>
								<input type="date" class="form-control" name="fecha" required>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
					<input type="submit" class="btn btn-success" value="Registrar">
				</div>
			</form>
		</div>
	</div>
</div>