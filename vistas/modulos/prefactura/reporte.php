<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'prefactura' . DS . 'ControlPrefactura.php';

$instancia = ControlPrefactura::singleton_prefactura();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-success">
                        Reporte de prefacturas
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 mb-2">
                            <form method="POST">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-4">
                                        <input type="date" class="form-control filtro_change" name="fecha_ini" data-toggle="tooltip" data-placement="top" title="Fecha desde" data-trigger="hover" required>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <input type="date" class="form-control filtro_change" name="fecha_fin" data-toggle="tooltip" data-placement="top" title="Fecha hasta" data-trigger="hover" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="form-group mt-1">
                                            <button type="submit" class="btn btn-sm btn-primary">Buscar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive col-lg-12">
                            <table class="table table-hover table-borderless table-sm" width="100%" cellspacing="0">
                                <thead>
                                    <tr class="text-center font-weight-bold">
                                        <th scope="col">#</th>
                                        <th scope="col">Fecha</th>
                                        <th scope="col">No. Reserva</th>
                                        <th scope="col">Empresa</th>
                                        <th scope="col">NIT</th>
                                        <th scope="col">Direccion</th>
                                        <th scope="col">Ciudad</th>
                                        <th scope="col">Contacto</th>
                                        <th scope="col">Telefono</th>
                                        <th scope="col">Usuario Prefactura</th>
                                        <th scope="col">Subtotal</th>
                                        <th scope="col">Descuento</th>
                                        <th scope="col">IVA</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                </thead>
                                <?php
                                if (isset($_POST['fecha_ini'])) {
                                    $fecha_ini = $_POST['fecha_ini'];
                                    $fecha_fin = $_POST['fecha_fin'];
                                    $buscar = $instancia->buscarReservasPrefacturadasControl($fecha_ini, $fecha_fin);
                                ?>
                                    <tbody class="buscar">
                                        <?php

                                        if (count($buscar) > 0) {
                                            $visible = '';

                                            foreach ($buscar as $datos) {
                                                $id_prefactura = $datos['id'];
                                                $id_reserva = $datos['id_reserva'];
                                                $id_empresa = $datos['id_empresa'];
                                                $empresa = $datos['empresa'];
                                                $direccion = $datos['direccion'];
                                                $ciudad = $datos['ciudad'];
                                                $telefono = $datos['telefono'];
                                                $contacto = $datos['contacto'];
                                                $nit = $datos['nit'];
                                                $fechareg = date('Y-m-d', strtotime($datos['fechareg']));
                                                $usuario_prefactura = $datos['usuario_prefactura'];
                                                $usuario_reserva = $datos['usuario_reserva'];
                                                $usuario_concilia = $datos['usuario_concilia'];
                                                $usuario_confirma = $datos['usuario_confirma'];
                                                $total_subtotal = $datos['total_subtotal'];
                                                $total_descuento = $datos['total_descuento'];
                                                $total_iva = $datos['total_iva'];
                                                $total_final = $datos['total_final'];

                                        ?>
                                                <tr class="text-center">
                                                    <td><?= $id_prefactura ?></td>
                                                    <td><?= $fechareg ?></td>
                                                    <td><?= $id_reserva ?></td>
                                                    <td><?= $empresa ?></td>
                                                    <td><?= $nit ?></td>
                                                    <td><?= $direccion ?></td>
                                                    <td><?= $ciudad ?></td>
                                                    <td><?= $contacto ?></td>
                                                    <td><?= $telefono ?></td>
                                                    <td><?= $usuario_prefactura ?></td>
                                                    <td>$<?= number_format($total_subtotal) ?></td>
                                                    <td>$<?= number_format($total_descuento) ?></td>
                                                    <td>$<?= number_format($total_iva) ?></td>
                                                    <td>$<?= number_format($total_final) ?></td>
                                                </tr>
                                        <?php
                                            }
                                        } else {
                                            $visible = 'd-none';
                                        }
                                        ?>
                                    </tbody>
                                    <form action="<?= BASE_URL ?>prefactura/excel" method="GET">
                                        <input type="hidden" name="fecha_desde" value="<?= $fecha_ini ?>">
                                        <input type="hidden" name="fecha_hasta" value="<?= $fecha_fin ?>">
                                        <button class="btn btn-sm btn-success ml-3 mt-2 mb-2 <?= $visible ?>" data-toggle="tooltip" data-placement="right" title="Descargar Reporte" data-trigger="hover">
                                            <i class="fa fa-file-excel"></i>
                                            &nbsp;
                                            Descargar reporte
                                        </button>
                                    </form>
                                <?php
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?= PUBLIC_PATH ?>js/programacion/funcionesProgramacion.js"></script>