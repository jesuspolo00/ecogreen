<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'prefactura' . DS . 'ControlPrefactura.php';

$instancia = ControlPrefactura::singleton_prefactura();

if (isset($_GET['reserva'])) {

    $id_reserva = base64_decode($_GET['reserva']);
    $fecha_ini = base64_decode($_GET['fecha_ini']);
    $fecha_fin = base64_decode($_GET['fecha_fin']);

    $empresa_prefactura = $instancia->mostrarEmpresaPrefacturaControl($id_super_empresa);

    $datos_prefactura = $instancia->mostrarDatosPrefacturaControl($id_reserva);
    $subtotal = 0;
?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3">
                        <h4 class="m-0 font-weight-bold text-success">
                            <form action="<?= BASE_URL ?>programacion/index" method="POST">
                                <button type="submit" class="text-decoration-none border-0 bg-transparent">
                                    <i class="fa fa-arrow-left text-success"></i>
                                </button>
                                <input type="hidden" value="<?= $fecha_ini ?>" name="fecha_ini">
                                <input type="hidden" value="<?= $fecha_fin ?>" name="fecha_fin">
                                &nbsp;
                                Prefacturar reserva (<?= $descripcion . ' - ' . $placa ?>)
                            </form>
                        </h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" class="p-2" id="prefactura">
                            <div class="row">
                                <h5 class="ml-3 font-weight-bold text-success">Empresa a prefacturar <span class="text-danger">*</span></h5>
                                <div class="col-lg-12 mb-4 p-2 text-center form-inline">
                                    <?php
                                    foreach ($empresa_prefactura as $prefactura) {
                                        $id_emp_pre = $prefactura['id'];
                                        $nombre = $prefactura['nombre'];
                                        $iva_emp_pre = $prefactura['iva'];
                                    ?>
                                        <div class="col-lg-6 form-group mb-2">
                                            <div class="custom-control custom-radio">
                                                <input type="hidden" id="iva_<?= $id_emp_pre ?>" value="<?= $iva_emp_pre ?>">
                                                <input type="radio" name="empresa_prefactura" value="<?= $id_emp_pre ?>" required class="custom-control-input check_empresa" id="<?= $id_emp_pre ?>">
                                                <label class="custom-control-label" for="<?= $id_emp_pre ?>"><?= $nombre ?></label>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <?php
                                foreach ($datos_prefactura as $datos) {
                                    $id_residuo = $datos['id_residuo'];
                                    $id_tipo_residuo = $datos['id_tipo_residuo'];
                                    $residuo = $datos['residuo'];
                                    $kilogramos = $datos['kilogramos'];
                                    $galones = $datos['galones'];
                                    $listado = $datos['listado'];
                                    $valor = $datos['valor_unt'];
                                    $id_listado = $datos['id_td1'];

                                    $total = $kilogramos * $valor;
                                ?>
                                    <div class="col-lg-3 text-center mb-2">
                                        <div class="form-group">
                                            <h6 class="m-0 font-weight-bold text-success">Residuo</h6>
                                            <div class="input-group mb-3 input-group-sm">
                                                <input type="text" maxlength="50" class="form-control" value="<?= $residuo ?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-lg-2 text-center">
                                        <div class="form-group">
                                            <h6 class="m-0 font-weight-bold text-success">Tratamiento/Disposicion</h6>
                                            <div class="input-group mb-3 input-group-sm">
                                                <input type="text" maxlength="50" class="form-control" value="<?= $listado ?>" disabled>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="col-lg-2 text-center">
                                        <div class="form-group">
                                            <h6 class="m-0 font-weight-bold text-success">Cant. Conciliada (Kg)</h6>
                                            <div class="input-group mb-3 input-group-sm">
                                                <input type="text" maxlength="50" class="form-control text-center" value="<?= $kilogramos ?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 text-center">
                                        <div class="form-group">
                                            <h6 class="m-0 font-weight-bold text-success">Cant. Conciliada (Gl)</h6>
                                            <div class="input-group mb-3 input-group-sm">
                                                <input type="text" maxlength="50" class="form-control text-center" value="<?= $galones ?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 text-center">
                                        <div class="form-group">
                                            <h6 class="m-0 font-weight-bold text-success">Valor Unt.</h6>
                                            <div class="input-group mb-3 input-group-sm">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" class="form-control numeros valor_unt" required name="valor[]" value="<?= $valor ?>" maxlength="20">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 text-center">
                                        <div class="form-group">
                                            <h6 class="m-0 font-weight-bold text-success">Valor Total</h6>
                                            <div class="input-group mb-3 input-group-sm">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" class="form-control numeros" id="<?= $id_residuo ?>" disabled value="<?= number_format($total) ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="id_reserva" value="<?= $id_reserva ?>">
                                    <input type="hidden" name="id_log" value="<?= $_SESSION['id'] ?>">
                                    <input type="hidden" name="id_residuo[]" value="<?= $id_residuo ?>">
                                    <input type="hidden" name="id_tipo_residuo[]" value="<?= $id_tipo_residuo ?>">
                                    <input type="hidden" name="super_empresa" value="<?= $id_super_empresa ?>">
                                    <input type="hidden" name="cantidad[]" value="<?= $kilogramos ?>">
                                    <input type="hidden" name="galones[]" value="<?= $galones ?>">
                                    <input type="hidden" name="listado[]" value="<?= $id_listado ?>">
                                <?php
                                    $subtotal += $total;
                                }
                                ?>
                                <div class="col-lg-3">
                                    <label class="text-success font-weight-bold">Subtotal:</label>
                                    <div class="input-group mb-3 input-group-sm">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input type="text" class="form-control" id="subtotal" name="subtotal" readonly value="<?= number_format($subtotal) ?>">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label class="text-success font-weight-bold">IVA:</label>
                                    <div class="input-group mb-3 input-group-sm">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">%</span>
                                        </div>
                                        <input type="text" class="form-control numeros" id="iva" maxlength="2" readonly value="19" name="iva">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label class="text-success font-weight-bold">Descuento:</label>
                                    <div class="input-group mb-3 input-group-sm">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">%</span>
                                        </div>
                                        <input type="text" class="form-control numeros descuento" maxlength="2" minlength="1" name="descuento">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label class="text-success font-weight-bold">Total:</label>
                                    <div class="input-group mb-3 input-group-sm">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input type="text" class="form-control numeros" id="total_final" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <label class="text-success font-weight-bold">Nota:</label>
                                    <textarea name="nota" cols="30" rows="4" class="form-control"></textarea>
                                </div>
                                <div class="col-lg-6 mt-3">
                                    <p class="text-danger h6">
                                        <strong> Advertencia: </strong>el valor total sera calculado una vez sea prefacturada la reserva.</p>
                                    <p class="text-danger mt-d1 h6">Cualquier inconveniente favor comunicarse con el area de sistemas, gracias.</p>
                                </div>
                                <div class="col-lg-6 mt-3">
                                    <button class="btn btn-success float-right" type="submit">
                                        <i class="fas fa-receipt"></i>
                                        &nbsp;
                                        Prefacturar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php

    if (isset($_POST['descuento'])) {
        $instancia->guardarPreciosPrefacturaControl();
    }
}
include_once VISTA_PATH . 'script_and_final.php';
?>
<script type="text/javascript" src="<?= PUBLIC_PATH ?>js/prefactura/funcionesPrefactura.js"></script>