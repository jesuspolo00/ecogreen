<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlResiduos.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlTipoResiduos.php';
$id_modulo = 1;

$instancia_tipo = ControlTipoResiduo::singleton_tipo_residuo();
$instancia = ControlResiduos::singleton_residuo();

$datos_tipo = $instancia_tipo->mostrarTipoResiduoControl($id_super_empresa);
$datos_residuo = $instancia->mostrarResiduosControl($id_super_empresa);
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?= BASE_URL ?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Residuos
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
							<div class="dropdown-header">Acciones:</div>
							<?php
							$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 16, 6);
							if ($permiso) {
							?>
								<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_residuo">Agregar Residuo</a>
							<?php
							}
							$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 16, 5);
							if ($permiso) {
							?>
								<a class="dropdown-item" href="<?= BASE_URL ?>tipos_residuo/index">Tipos de Residuo</a>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8"></div>
						<div class="col-lg-4">
							<form>
								<div class="form-group">
									<div class="input-group mb-3">
										<input type="text" class="form-control filtro" placeholder="Buscar">
										<div class="input-group-prepend">
											<span class="input-group-text rounded-right" id="basic-addon1">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="table-responsive">
							<table class="table table-hover table-borderless table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold">
										<th scope="col">#</th>
										<th scope="col">Nombre</th>
										<th scope="col">Codigo</th>
										<th scope="col">Tipo residuo</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									foreach ($datos_residuo as $datos) {
										$id_residuo = $datos['id_residuo'];
										$nombre = $datos['nombre'];
										$codigo = $datos['codigo'];
										$estado = $datos['estado'];
										$id_tipo = $datos['id_tipo_residuo'];
										$valor_unitario = $datos['valor_unt'];

										$tipo_residuo = $instancia_tipo->mostrarTipoResiduoIdControl($id_tipo);
										$estado_tipo = $tipo_residuo['estado'];

										if ($estado_tipo == 'activo') {
											$ver = ($estado != 'inactivo' && $_SESSION['rol'] != 4) ? '' : 'd-none';
											$ver_admin = ($_SESSION['rol'] == 4) ? $ver = '' : '';
									?>
											<tr class="text-center <?= $ver ?> <?= $ver_admin ?>" id="tipo_residuo<?= $id_residuo; ?>">
												<td><?= $id_residuo ?></td>
												<td><?= $nombre ?></td>
												<td><?= $codigo ?></td>
												<td><?= $tipo_residuo['nombre'] ?></td>
												<?php
												$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 5, 1);
												if ($permiso) {
												?>
													<td>
														<button class="btn btn-sm btn-primary editar_residuo" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" data-target="#editar_residuo<?= $id_residuo ?>" title="Editar residuo" data-trigger="hover">
															<i class="fa fa-edit"></i>
														</button>
													</td>
													<?php
												}
												$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 5, 2);
												if ($permiso) {
													if ($estado == 'activo' && $tipo_residuo['estado'] == 'activo') {
													?>
														<td>
															<button class="btn btn-sm btn-danger mr-2 ml-2 inactivar_residuo" id="<?= $id_residuo ?>" data-toggle="tooltip" data-placement="bottom" title="Inactivar residuo" data-trigger="hover">
																<i class="fa fa-times"></i>
															</button>
														</td>
													<?php
													} else if ($estado == 'inactivo' && $tipo_residuo['estado'] == 'activo') {
													?>
														<td>
															<button class="btn btn-sm btn-success ml-2 mr-2 activar_residuo" id="<?= $id_residuo ?>" data-toggle="tooltip" data-placement="bottom" title="Activar residuo" data-trigger="hover">
																<i class="fa fa-check"></i>
															</button>
														</td>
													<?php
													}
												}
												$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 5, 3);
												if ($permiso) {
													?>
													<td>
														<button class="btn btn-sm btn-secondary eliminar_residuo" id="<?= $id_residuo ?>" data-toggle="tooltip" data-placement="bottom" title="Eliminar residuo" data-trigger="hover">
															<i class="fa fa-trash"></i>
														</button>
													</td>
												<?php
												}
												?>
											</tr>

											<!--Editar Tipo de residuo-->
											<div class="modal fade" id="editar_residuo<?= $id_residuo ?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
												<div class="modal-dialog modal-md p-2" role="document">
													<div class="modal-content">
														<form method="POST">
															<input type="hidden" value="<?= $id_residuo ?>" name="id_residuo">
															<div class="modal-header p-3">
																<h4 class="modal-title text-success font-weight-bold">Editar Tipo</h4>
															</div>
															<div class="modal-body border-0">
																<div class="row p-3">
																	<div class="col-lg-12">
																		<div class="form-group">
																			<label>Nombre</label>
																			<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nom_edit" value="<?= $nombre ?>" required>
																		</div>
																	</div>
																	<div class="col-lg-12">
																		<div class="form-group">
																			<label>Codigo</label>
																			<input type="text" class="form-control" maxlength="5" minlength="1" name="cod_edit" value="<?= $codigo ?>">
																		</div>
																	</div>
																	<div class="col-lg-12">
																		<div class="form-group">
																			<label>Valor Unitario<span class="text-danger">*</span></label>
																			<input type="text" class="form-control numeros" maxlength="10" minlength="1" name="valor_edit" value="<?= $valor_unitario ?>" required>
																		</div>
																	</div>
																	<div class="col-lg-12">
																		<div class="form-group">
																			<label>Tipo de residuo</label>
																			<select name="id_tipo_edit" class="form-control">
																				<option value="<?= $id_tipo ?>" selected class="d-none"><?= $tipo_residuo['nombre'] ?></option>
																				<?php
																				foreach ($datos_tipo as $key) {
																					$id_tipo_residuo = $key['id_tipo'];
																					$nombre =  $key['nombre'];
																					$estado = $key['estado'];

																					if ($estado == 'activo') {
																				?>
																						<option value="<?= $id_tipo_residuo ?>"><?= $nombre ?></option>
																				<?php
																					}
																				}
																				?>
																			</select>
																		</div>
																	</div>
																</div>
															</div>
															<div class="modal-footer border-0">
																<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
																<input type="submit" class="btn btn-success" value="Guardar Cambio">
															</div>
														</form>
													</div>
												</div>
											</div>
									<?php
										}
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'residuos' . DS . 'agregarResiduo.php';

if (isset($_POST['descripcion'])) {
	$instancia->guardarResiduoControl();
}

if (isset($_POST['nom_edit'])) {
	$instancia->editarResiduoControl();
}
include_once VISTA_PATH . 'modulos' . DS . 'configuracion' . DS . 'alerta.php';
?>
<script type="text/javascript" src="<?= PUBLIC_PATH ?>js/configuracion/efectos.js"></script>
<script src="<?= PUBLIC_PATH ?>js/residuos/funcionesResiduo.js"></script>