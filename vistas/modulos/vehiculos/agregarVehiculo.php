<!--Agregar vehiculo-->
<div class="modal fade" id="agregar_vehiculo" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-lg p-2" role="document">
		<div class="modal-content">
			<form method="POST">
				<input type="hidden" value="<?= $_SESSION['super_empresa'] ?>" name="super_empresa">
				<input type="hidden" value="<?= $_SESSION['id'] ?>" name="id_log">
				<div class="modal-header p-3">
					<h4 class="modal-title text-success font-weight-bold">Agregar Vehiculo</h4>
				</div>
				<div class="modal-body border-0">
					<div class="row p-3">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Descripcion</label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="descripcion" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Placa</label>
								<input type="text" class="form-control" maxlength="50" minlength="1" name="placa" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Marca</label>
								<input type="text" class="form-control" maxlength="50" minlength="1" name="marca" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Modelo</label>
								<input type="text" class="form-control" maxlength="50" minlength="1" name="modelo" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Tipo de vehiculo</label>
								<select class="form-control" name="id_tipo" required>
									<option selected value="">Seleccione una opcion...</option>
									<?php
									foreach ($datos_tipo as $datos) {
										$id_tipo = $datos['id_tipo'];
										$nombre = $datos['nombre'];
										$estado = $datos['estado'];

										if ($estado != 'inactivo') {
									?>
											<option value="<?= $id_tipo ?>"><?= $nombre ?></option>
									<?php
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Usuario a cargo</label>
								<select class="form-control" name="id_user" required>
									<option selected value="">Seleccione una opcion...</option>
									<?php
									foreach ($datos_user as $datos_usuario) {
										$id_user = $datos_usuario['id_user'];
										$nombre = $datos_usuario['nombre'];
										$apellido = $datos_usuario['apellido'];
										$estado = $datos_usuario['estado'];

										if ($estado != 'inactivo') {
									?>
											<option value="<?= $id_user ?>"><?= $nombre . ' ' . $apellido ?></option>
									<?php
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
					<input type="submit" class="btn btn-success" value="Registrar">
				</div>
			</form>
		</div>
	</div>
</div>