<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlTipoVehiculo.php';
require_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
$id_modulo = 1;
$instancia = ControlVehiculo::singleton_vehiculo();
$datos_vehiculo = $instancia->mostrarDatosVehiculosControl($id_super_empresa);

$instancia_tipo = ControlTipoVehiculo::singleton_tipo_vehiculo();
$datos_tipo = $instancia_tipo->mostrarTipoVehiculoControl($id_super_empresa);

$instancia_user = ControlUsuario::singleton_usuario();
$datos_user = $instancia_user->mostrarDatosUsuariosControl($id_super_empresa);
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?= BASE_URL ?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Vehiculos
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
							<div class="dropdown-header">Acciones:</div>
							<?php
							$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 15, 6);
							if ($permiso) {
							?>
								<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_vehiculo">Agregar Vehiculo</a>
							<?php
							}
							$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 15, 5);
							if ($permiso) {
							?>
								<a class="dropdown-item" href="<?= BASE_URL ?>tipo_vehiculo/index">Tipos de Vehiculo</a>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8"></div>
						<div class="col-lg-4">
							<form>
								<div class="form-group">
									<div class="input-group mb-3">
										<input type="text" class="form-control filtro" placeholder="Buscar">
										<div class="input-group-prepend">
											<span class="input-group-text rounded-right" id="basic-addon1">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="table-responsive">
							<table class="table table-hover table-borderless table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold">
										<th scope="col">#</th>
										<th scope="col">Descripcion</th>
										<th scope="col">Placa</th>
										<th scope="col">Marca</th>
										<th scope="col">Modelo</th>
										<th scope="col">Tipo</th>
										<th scope="col">Usuario a cargo</th>
										<th scope="col">Estado</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									foreach ($datos_vehiculo as $datos) {
										$id_inventario = $datos['id_inventario'];
										$descripcion = $datos['descripcion'];
										$placa = $datos['placa'];
										$marca = $datos['marca'];
										$modelo = $datos['modelo'];
										$marca = $datos['marca'];
										$id_tipo = $datos['id_tipo'];
										$id_user = $datos['id_user'];
										$estado = $datos['estado'];
										$estado_vehiculo = $datos['estado_vehiculo'];

										$tipo_vehiculo = $instancia_tipo->mostrarTipoVehiculoIdControl($id_tipo, $id_super_empresa);
										$nombre_tipo = $tipo_vehiculo['nombre'];
										$estado_tipo = $tipo_vehiculo['estado'];

										$user_datos = $instancia_user->mostrarDatosUsuariosIdControl($id_user);
										$nombre_user = $user_datos['nombre'];
										$apellido_user = $user_datos['apellido'];

										if ($estado == 1) {
											$nom_estado = 'Da&ntilde;ado';
										} else if ($estado == 2) {
											$nom_estado = 'Mantenimiento';
										} else if ($estado == 3) {
											$nom_estado = 'Otro';
										} else if ($estado == 4) {
											$nom_estado = 'Arreglado';
										} else {
											$nom_estado = 'Nuevo';
										}

										if ($estado_tipo == 'activo') {

											$ver = ($estado_vehiculo != 'inactivo' && $_SESSION['rol'] != 4) ? '' : 'd-none';
											$ver_admin = ($_SESSION['rol'] == 4) ? $ver = '' : '';
									?>
											<tr class="text-center <?= $ver ?> <?= $ver_admin ?>" id="vehiculo<?= $id_inventario; ?>">
												<td><?= $id_inventario ?></td>
												<td><a href="<?= BASE_URL ?>hoja_vida/index?vehiculo=<?= base64_encode($id_inventario) ?>"><?= $descripcion ?></a></td>
												<td><?= $placa ?></td>
												<td><?= $marca ?></td>
												<td><?= $modelo ?></td>
												<td><?= $nombre_tipo ?></td>
												<td><?= $nombre_user . ' ' . $apellido_user ?></td>
												<td><?= $nom_estado ?></td>
												<?php
												$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 2, 4);
												if ($permiso) {
													if ($estado == 5 || $estado == 4) {
												?>
														<td>
															<button class="btn btn-sm btn-warning reportar_mant" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" data-target="#reportar_mant<?= $id_inventario ?>" title="Reportar Vehiculo" data-trigger="hover">
																<i class="fas fa-wrench"></i>
															</button>
														</td>
													<?php
													} else {
													?>
														<td>
															<a href="<?= BASE_URL ?>imprimir/imprimirReporte?vehiculo=<?= base64_encode($id_inventario) ?>" class="btn btn-sm btn-primary descargar_reporte" target="_blank" data-tooltip="tooltip" data-placement="bottom" title="Descargar Reporte" data-trigger="hover">
																<i class="fas fa-download"></i>
															</a>
														</td>
													<?php
													}
												}
												$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 2, 2);
												if ($permiso) {
													if ($estado_vehiculo == 'activo') {
													?>
														<td>
															<button class="btn btn-sm btn-danger inactivar_vehiculo" id="<?= $id_inventario ?>" data-tooltip="tooltip" data-placement="bottom" title="Inactivar Vehiculo" data-trigger="hover">
																<i class="fas fa-times"></i>
															</button>
														</td>
													<?php
													} else {
													?>
														<td>
															<button class="btn btn-sm btn-success activar_vehiculo" id="<?= $id_inventario ?>" data-tooltip="tooltip" data-placement="bottom" title="Activar Vehiculo" data-trigger="hover">
																<i class="fas fa-check"></i>
															</button>
														</td>
													<?php
													}
												}
												$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 2, 3);
												if ($permiso) {
													?>
													<td>
														<button class="btn btn-sm btn-secondary eliminar_vehiculo" id="<?= $id_inventario ?>" data-toggle="tooltip" data-placement="bottom" title="Eliminar Vehiculo" data-trigger="hover">
															<i class="fa fa-trash"></i>
														</button>
													</td>
												<?php
												}
												?>
											</tr>


											<div class="modal fade" id="reportar_mant<?= $id_inventario ?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
												<div class="modal-dialog modal-lg p-2" role="document">
													<div class="modal-content">
														<form method="POST">
															<input type="hidden" value="<?= $_SESSION['id'] ?>" name="id_log">
															<input type="hidden" value="<?= $id_super_empresa ?>" name="super_empresa">
															<input type="hidden" value="<?= $id_inventario ?>" name="id_vehiculo">
															<div class="modal-header p-3">
																<h4 class="modal-title text-success font-weight-bold">Reportar Vehiculo</h4>
															</div>
															<div class="modal-body border-0">
																<div class="row p-3">
																	<div class="col-lg-6">
																		<div class="form-group">
																			<label>Descripcion</label>
																			<input type="text" class="form-control letras" disabled value="<?= $descripcion ?>">
																		</div>
																	</div>
																	<div class="col-lg-6">
																		<div class="form-group">
																			<label>Placa</label>
																			<input type="text" class="form-control" disabled value="<?= $placa ?>">
																		</div>
																	</div>
																	<div class="col-lg-6">
																		<div class="form-group">
																			<label>Marca</label>
																			<input type="text" class="form-control" disabled value="<?= $marca ?>">
																		</div>
																	</div>
																	<div class="col-lg-6">
																		<div class="form-group">
																			<label>Modelo</label>
																			<input type="text" class="form-control" disabled value="<?= $modelo ?>">
																		</div>
																	</div>
																	<div class="col-lg-6">
																		<div class="form-group">
																			<label>Tipo</label>
																			<input type="text" class="form-control" disabled value="<?= $nombre_tipo ?>">
																		</div>
																	</div>
																	<div class="col-lg-6">
																		<div class="form-group">
																			<label>Usuario a cargo</label>
																			<input type="text" class="form-control" disabled value="<?= $nombre_user . ' ' . $apellido_user ?>">
																		</div>
																	</div>
																	<div class="col-lg-12">
																		<div class="form-group">
																			<label>Tipo de reporte</label>
																			<select name="tipo_mant" class="form-control" required>
																				<option value="">Seleccione una opcion...</option>
																				<option value="1">Da&ntilde;o</option>
																				<option value="2">Mantenimiento</option>
																				<option value="3">Otro Soporte</option>
																			</select>
																		</div>
																	</div>
																	<div class="col-lg-12">
																		<div class="form-group">
																			<label>Observacion</label>
																			<textarea name="observacion" maxlength="500" id="" cols="30" rows="5" class="form-control"></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="modal-footer border-0">
																<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
																<input type="submit" class="btn btn-success" value="Reportar">
															</div>
														</form>
													</div>
												</div>
											</div>
									<?php
										}
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'vehiculos' . DS . 'agregarVehiculo.php';
if (isset($_POST['descripcion'])) {
	$instancia->registrarVehiculoControl();
}

if (isset($_POST['id_vehiculo'])) {
	$instancia->reportarVehiculoControl();
}
include_once VISTA_PATH . 'modulos' . DS . 'configuracion' . DS . 'alerta.php';
?>
<script type="text/javascript" src="<?= PUBLIC_PATH ?>js/configuracion/efectos.js"></script>
<script src="<?= PUBLIC_PATH ?>js/vehiculo/funcionesVehiculo.js"></script>