<!--Agregar vehiculo-->
<div class="modal fade" id="agregar_tratamiento" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-md p-2" role="document">
        <div class="modal-content">
            <form method="POST">
                <input type="hidden" value="<?= $_SESSION['id'] ?>" name="id_log">
                <input type="hidden" value="<?= $_SESSION['super_empresa'] ?>" name="super_empresa">
                <div class="modal-header p-3">
                    <h4 class="modal-title text-success font-weight-bold">Agregar Tratamiento</h4>
                </div>
                <div class="modal-body border-0">
                    <div class="row p-3">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control letras" maxlength="50" minlength="1" name="descripcion" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Tipo de tratamiento</label>
                                <select class="form-control" name="id_tipo" required>
                                    <option value="" selected>Seleccione una opcion...</option>
                                    <?php
                                    foreach ($datos_tipo as $key) {
                                        $id_tipo_destino = $key['id'];
                                        $nombre =  $key['nombre'];
                                        $estado = $key['activo'];

                                        $ver = ($estado == 1) ? '' : 'd-none';
                                    ?>
                                        <option value="<?= $id_tipo_destino ?>" class="<?= $ver ?>"><?= $nombre ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <input type="submit" class="btn btn-success" value="Registrar">
                </div>
            </form>
        </div>
    </div>
</div>