<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'apartar' . DS . 'ControlApartar.php';
require_once CONTROL_PATH . 'programacion' . DS . 'ControlProgramacion.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlEmpresa.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlTipoVehiculo.php';
require_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlSede.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlResiduos.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlTipoResiduos.php';

$instancia = ControlProgramacion::singleton_programacion();
$instancia_apartar = ControlApartar::singleton_apartar();
$instancia_empresa = ControlEmpresa::singleton_empresa();
$instancia_vehiculo = ControlVehiculo::singleton_vehiculo();
$instancia_tipo_vehiculo = ControlTipoVehiculo::singleton_tipo_vehiculo();
$instancia_usuario = ControlUsuario::singleton_usuario();
$instancia_sede = ControlSede::singleton_sede();
$instancia_tipo = ControlTipoResiduo::singleton_tipo_residuo();
$instancia_residuo = ControlResiduos::singleton_residuo();

if (isset($_GET['reserva'])) {

	$id_reserva = base64_decode($_GET['reserva']);
	$fecha_ini = base64_decode($_GET['fecha_ini']);
	$fecha_fin = base64_decode($_GET['fecha_fin']);

	$buscar = $instancia->mostrarProgramacionIdControl($id_reserva);

	$id_empresa = $buscar['id_empresa'];
	$id_vehiculo = $buscar['id_vehiculo'];
	$id_conductor = $buscar['id_conductor'];
	$id_coopiloto = $buscar['id_coopiloto'];
	$id_sucursal = $buscar['id_sucursal'];
	$id_user = $buscar['id_user'];
	$hora_inicio = $buscar['hora_inicio'];
	$hora_fin = $buscar['hora_fin'];
	$fecha_apartado = $buscar['fecha_apartado'];
	$observacion = $buscar['observacion'];
	$confirmado = $buscar['confirmado'];

	if ($id_sucursal != 0) {

		$datos_empresa = $instancia_sede->mostrarSedeIdControl($id_sucursal);
		$nombre_empresa = $datos_empresa['nombre'];
		$direccion_empresa = $datos_empresa['direccion'];
		$ciudad_empresa = $datos_empresa['ciudad'];
		$nom_contacto_empresa = $datos_empresa['nom_contacto'];
		$telefono_empresa = $datos_empresa['telefono'];
		$estado_empresa = $datos_empresa['estado'];
		$id_empresa_sucursal = $datos_empresa['id_empresa'];
		$datos_empresa_nit = $instancia_empresa->mostrarEmpresaIdControl($id_empresa_sucursal);

		$nombre_empresa_sede = $datos_empresa_nit['nombre'];
		$nit = $datos_empresa_nit['nit'];
	} else {
		$datos_empresa = $instancia_empresa->mostrarEmpresaIdControl($id_empresa);
		$nombre_empresa = $datos_empresa['nombre'];
		$direccion_empresa = $datos_empresa['direccion'];
		$ciudad_empresa = $datos_empresa['ciudad'];
		$nom_contacto_empresa = $datos_empresa['nom_contacto'];
		$telefono_empresa = $datos_empresa['telefono'];
		$estado_empresa = $datos_empresa['estado'];
		$nit = $datos_empresa['nit'];
		$nombre_empresa_sede = $datos_empresa['nombre'];
	}


	$datos_vehiculo = $instancia_vehiculo->mostrarDatosVehiculosIdControl($id_vehiculo);
	$descripcion = $datos_vehiculo['descripcion'];
	$placa = $datos_vehiculo['placa'];
	$marca = $datos_vehiculo['marca'];
	$modelo = $datos_vehiculo['modelo'];
	$estado_vehiculo = $datos_vehiculo['estado'];
	$tipo_vehiculo = $datos_vehiculo['id_tipo'];


	$datos_tipo_vehiculo = $instancia_tipo_vehiculo->mostrarTipoVehiculoIdControl($tipo_vehiculo, $id_super_empresa);
	$nombre_tipo_vehiculo = $datos_tipo_vehiculo['nombre'];

	$datos_resiudo_reserva = $instancia->mostrarResiduosProgramacionControl($id_reserva);
	$datos_tipo_residuo = $instancia_tipo->mostrarTipoResiduoControl($id_super_empresa);

	$datos_conductor = $instancia_usuario->mostrarDatosUsuariosIdControl($id_conductor);
	$nombre_conductor = $datos_conductor['nombre'] . ' ' . $datos_conductor['apellido'];
	$documento_conductor = $datos_conductor['documento'];

	$datos_usuario = $instancia_usuario->mostrarDatosUsuariosIdControl($id_user);
	$nombre_usuario = $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'];
	$documento_usuario = $datos_usuario['documento'];

	if ($id_coopiloto != 0) {
		$datos_coopiloto = $instancia_usuario->mostrarDatosUsuariosIdControl($id_coopiloto);
		$nombre_coopiloto = $datos_coopiloto['nombre'] . ' ' . $datos_coopiloto['apellido'];
		$documento_coopiloto = $datos_coopiloto['documento'];
	} else {
		$nombre_coopiloto = '';
	}

?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3">
						<h4 class="m-0 font-weight-bold text-success">
							<form action="<?= BASE_URL ?>programacion/index" method="POST">
								<button type="submit" class="text-decoration-none border-0 bg-transparent">
									<i class="fa fa-arrow-left text-success"></i>
								</button>
								<input type="hidden" value="<?= $fecha_ini ?>" name="fecha_ini">
								<input type="hidden" value="<?= $fecha_fin ?>" name="fecha_fin">
								&nbsp;
								Confirmar Reserva (<?= $descripcion . ' - ' . $placa ?>)
							</form>
						</h4>
					</div>
					<div class="card-body">
						<form method="POST">
							<input type="hidden" name="id_reserva" value="<?= $id_reserva ?>">
							<input type="hidden" name="id_log" value="<?= $_SESSION['id'] ?>">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Conductor</label>
										<input type="text" class="form-control letras" readonly value="<?= $nombre_conductor ?>">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Coopiloto</label>
										<input type="text" class="form-control letras" readonly value="<?= $nombre_coopiloto ?>">
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label>Empresa</label>
										<select name="empresa" class="form-control" id="empresa">
											<option value="<?= $id_empresa ?>" selected class="d-none"><?= $nombre_empresa_sede . ' (' . $nit . ')' ?></option>
											<?php
											$datos_empresa_todas = $instancia_empresa->mostrarEmpresasControl($id_super_empresa);
											foreach ($datos_empresa_todas as $empresa) {
												$id_empresa_todas = $empresa['id_empresa'];
												$nombre_empresa_todas = $empresa['nombre'];
												$nit_todas = $empresa['nit'];
												$estado_todas = $empresa['estado'];

												if ($estado_todas == 'activo') {
											?>
													<option value="<?= $id_empresa_todas ?>"><?= $nombre_empresa_todas . ' (' . $nit_todas . ')' ?></option>
											<?php
												}
											}
											?>
										</select>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Sede</label>
										<select name="sucursal" class="form-control" id="sucursales">
											<option value="<?= $id_sucursal ?>" selected class="d-none"><?= $nombre_empresa . ' (' . $nit . ')' ?></option>
										</select>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Ciudad</label>
										<input type="text" disabled class="form-control letras" value="<?= $ciudad_empresa ?>" id="ciudad">
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label>Fecha</label>
										<input type="date" class="form-control letras" readonly value="<?= $fecha_apartado ?>">
									</div>
								</div>
								<div class="col-lg-6 mb-4">
									<label>Hora inicio</label>
									<div class="input-group clock">
										<input type="time" class="form-control" name="hora_inicio" disabled id="hora_inicio" value="<?= $hora_inicio ?>">
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-time"></span>
										</span>
									</div>
								</div>
								<div class="col-lg-6 mb-4">
									<label>Hora fin</label>
									<div class="input-group clock">
										<input type="time" name="hora_fin" required class="form-control" id="hora_fin" value="<?= $hora_fin ?>">
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-time"></span>
										</span>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<div class="accordion" id="accordionExample">
											<?php
											$datos_tipo = $instancia_tipo->mostrarTipoResiduoControl($id_super_empresa);
											foreach ($datos_tipo as $datos) {

												$id_tipo = $datos['id_tipo'];
												$nombre = $datos['nombre'];
												$estado = $datos['estado'];

												$datos_residuo_tipo = $instancia_residuo->mostrarResiduosTipoIdControl($id_tipo);

												if ($estado == 'activo') {
											?>
													<div class="card border-left-danger shadow-none">
														<div class="card-header bg-white text-light" id="headingOne">
															<a href="#" class="text-decoration-none h6 mb-0 text-muted collapsed" data-toggle="collapse" data-target="#residuos<?= $id_tipo ?>" aria-expanded="false" aria-controls="residuos<?= $id_tipo ?>">
																<div class="row no-gutters align-items-center">
																	<div class="col mr-2">
																		<?= $nombre ?>
																		<div class="h5 mb-0 font-weight-bold text-gray-800"></div>
																	</div>
																	<div class="col-auto">
																		<i class="fa fa-biohazard text-danger fa-2x cursor-pointer" data-toggle="collapse" data-target="#residuos<?= $id_tipo ?>" aria-expanded="false" aria-controls="residuos<?= $id_tipo ?>"></i>
																	</div>
																</div>
															</a>
														</div>
														<div id="residuos<?= $id_tipo ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
															<div class="card rounded-0">
																<div class="card-body">
																	<div class="row">
																		<?php
																		foreach ($datos_residuo_tipo as $residuos) {
																			$id_residuo = $residuos['id_residuo'];

																			$datos_residuos_apartados = $instancia->mostrarResiduosTipoProgramacionControl($id_reserva, $id_residuo);
																			$id_residuos_apartado = $datos_residuos_apartados['id_residuo'];

																			if ($id_residuo == $id_residuos_apartado) {
																				$nombre = $residuos['nombre'];
																				$codigo = $residuos['codigo'];
																				$cantidad = $datos_residuos_apartados['cantidad'];
																				$visible = '';
																				$readonly = '';
																			} else {
																				$nombre = $residuos['nombre'];
																				$codigo = $residuos['codigo'];
																				$cantidad = '';
																				$visible = 'd-none';
																				$readonly = 'readonly';
																			}

																			$cantidad = ($_SESSION['rol'] == 5) ? '' : $cantidad;

																		?>
																			<div class="col-lg-4">
																				<div class="form-group">
																					<div class="custom-control custom-checkbox">
																						<input type="checkbox" class="custom-control-input check" value="<?= $id_residuo ?>" id="<?= $id_residuo ?>" checked name="id_residuo[]">
																						<label class="custom-control-label" for="<?= $id_residuo ?>"><?= $nombre . ' (' . $codigo . ')' ?></label>
																					</div>
																					&nbsp;
																					<div class="input-group input-group-sm">
																						<input type="number" class="form-control col-lg-4 numeros" <?= $readonly ?> value="<?= $cantidad ?>" id="num_<?= $id_residuo ?>" name="cantidad[]">
																					</div>
																					&nbsp;
																				</div>
																			</div>
																		<?php } ?>
																	</div>
																</div>
															</div>
														</div>
													</div>
											<?php
												}
											}
											?>
										</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label>Observaciones</label>
										<textarea class="form-control" name="observacion" maxlength="300" minlength="1">
											<?= trim($observacion); ?>
										</textarea>
									</div>
								</div>
								<div class="col-lg-12 mt-2">
									<div class="form-group float-right">
										<button class="btn btn-success float-right" type="submit">
											<i class="fas fa-check"></i>
											&nbsp;
											Confirmar
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
	if (isset($_POST['empresa'])) {
		$instancia_apartar->confirmarReservaControl();
	}
}
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?= PUBLIC_PATH ?>js/programacion/funcionesProgramacion.js"></script>