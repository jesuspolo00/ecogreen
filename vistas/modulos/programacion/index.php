<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'programacion' . DS . 'ControlProgramacion.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlEmpresa.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlSede.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';

$instancia          = ControlProgramacion::singleton_programacion();
$instancia_empresa  = ControlEmpresa::singleton_empresa();
$instancia_sede     = ControlSede::singleton_sede();
$instancia_vehiculo = ControlVehiculo::singleton_vehiculo();

$datos_vehiculo = $instancia_vehiculo->mostrarDatosVehiculosControl($id_super_empresa);

$permitido_facturar = [1, 4, 7];
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-success">
                        Programaci&oacute;n Diaria
                    </h4>
<!--                     <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Acciones:</div>
                            <a class="dropdown-item" href="<?=BASE_URL?>programacion/index">Ver recepciones</a>
                        </div>
                    </div> -->
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 mb-2">
                            <form method="POST">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-3">
                                        <select name="vehiculo" class="form-control filtro_change">
                                            <option value="" selected>Seleccione una opcion</option>
                                            <?php foreach ($datos_vehiculo as $vehiculo) {
                                                $id_vehiculo = $vehiculo['id_inventario'];
                                                $nombre      = $vehiculo['placa'];
                                                $estado      = $vehiculo['estado'];
                                                ?>
                                                <option value="<?=$id_vehiculo?>"><?=$nombre?></option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                    <div class="col-lg-3  mb-3">
                                        <input type="date" class="form-control filtro_change" name="fecha_ini" data-toggle="tooltip" data-placement="top" title="Fecha desde" data-trigger="hover" required>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <input type="date" class="form-control filtro_change" name="fecha_fin" data-toggle="tooltip" data-placement="top" title="Fecha hasta" data-trigger="hover" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="form-group mt-1">
                                            <button type="submit" class="btn btn-sm btn-primary">Buscar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive col-lg-12">
                            <table class="table table-hover table-borderless table-sm" width="100%" cellspacing="0">
                                <thead>
                                    <tr class="text-center font-weight-bold">
                                        <th></th>
                                        <th scope="col">#</th>
                                        <th scope="col">Vehiculo</th>
                                        <th scope="col">Empresa</th>
                                        <th scope="col">Sucursal</th>
                                        <th scope="col">Direccion</th>
                                        <th scope="col">Telefono</th>
                                        <th scope="col">Nombre contacto</th>
                                        <th scope="col">Fecha apartado</th>
                                        <th scope="col">Hora inicio</th>
                                        <th scope="col">Hora fin</th>
                                        <th scope="col">Observaciones</th>
                                    </tr>
                                </thead>
                                <?php
                                if (isset($_POST['fecha_ini'])) {
                                    $id_vehiculo = (!empty($_POST['vehiculo'])) ? $_POST['vehiculo'] : 0;
                                    $fecha_ini   = $_POST['fecha_ini'];
                                    $fecha_fin   = $_POST['fecha_fin'];
                                    $buscar      = $instancia->buscarReservaVehiculoControl($id_vehiculo, $fecha_ini, $fecha_fin);
                                    ?>
                                    <tbody class="buscar">
                                        <?php
                                        $visible_imprimir = 'd-none';
                                        if (count($buscar) > 0) {
                                            $url_imprimir = BASE_URL . 'imprimir/imprimir?vehiculo=' . base64_encode($id_vehiculo) . '&fecha_ini=' . base64_encode($fecha_ini)
                                            . '&fecha_fin=' . base64_encode($fecha_fin);
                                            $visible_imprimir = '';
                                        }
                                        foreach ($buscar as $datos) {

                                            $id_reserva   = $datos['id_reserva'];
                                            $empresa      = $datos['empresa'];
                                            $sucursal     = $datos['sucursal'];
                                            $direccion    = $datos['direccion'];
                                            $telefono     = $datos['telefono'];
                                            $contacto     = $datos['contacto'];
                                            $fecha        = $datos['fecha_apartado'];
                                            $hora_inicio  = $datos['hora_inicio'];
                                            $hora_fin     = $datos['hora_fin'];
                                            $observacion  = $datos['observacion'];
                                            $recepcion    = $datos['recepcion'];
                                            $confirmado   = $datos['confirmado'];
                                            $conciliacion = $datos['conciliacion'];
                                            $id_vehiculo  = $datos['id_vehiculo'];
                                            $vehiculo     = $datos['vehiculo'];
                                            $coopiloto    = $datos['coopiloto'];

                                            $datos_consecutivo      = $instancia->mostrarConsecutivoReservaControl($id_reserva);
                                            $datos_consecutivo_cert = $instancia->mostrarConsecutivoCertReservaControl($id_reserva);
                                            $prefacturado           = $instancia->consultarPrefacturaControl($id_reserva);

                                            $visible_movilizacion = 'd-none';
                                            $visible_confirmacion = 'd-none';
                                            $visible_certificado  = 'd-none';
                                            $visible_recepcion    = 'd-none';
                                            $visible_alert        = '';
                                            $icon                 = '';
                                            $tooltip              = '';
                                            $alert                = '';
                                            $url_confirmado       = '';

                                            /*-------------------------------------------------*/
                                            if ($datos_consecutivo['numero'] == '' && $_SESSION['rol'] != 5) {

                                                $url_movilizacion = BASE_URL . 'programacion/movilizacion?reserva=' .
                                                base64_encode($id_reserva) . '&fecha_ini=' . base64_encode($fecha_ini)
                                                . '&fecha_fin=' . base64_encode($fecha_fin);
                                                $visible_movilizacion = '';

                                                $target_movilizacion = '';
                                            } else if ($_SESSION['rol'] != 5) {

                                                $url_movilizacion = BASE_URL . 'imprimir/imprimirMovilizacion?reserva=' .
                                                base64_encode($id_reserva) . '&fecha_ini=' . base64_encode($fecha_ini)
                                                . '&fecha_fin=' . base64_encode($fecha_fin);
                                                $visible_movilizacion = '';

                                                $target_movilizacion = '_blank';
                                            }
                                            /*-------------------------------------------------*/
                                            if ($_SESSION['rol'] != 5 && $conciliacion == 'si' && $datos_consecutivo['numero'] != '') {

                                                $url_certificado_recepcion = BASE_URL . 'imprimir/imprimirCertificadoRecepcion?reserva=' .
                                                base64_encode($id_reserva) . '&fecha=' . base64_encode($fecha);
                                                $visible_recepcion = '';
                                            }
                                            /*-------------------------------------------------*/
                                            if ($confirmado == 'si' && $conciliacion == 'no' && $_SESSION['rol'] != 5 && $datos_consecutivo['numero'] != '') {

                                                $url_confirmado = BASE_URL . 'programacion/consiliacion?reserva=' .
                                                base64_encode($id_reserva) . '&fecha_ini=' . base64_encode($fecha_ini)
                                                . '&fecha_fin=' . base64_encode($fecha_fin);
                                                $visible_confirmacion = '';
                                                $tooltip              = 'Conciliar Reserva';
                                                $icon                 = '<i class="fas fa-clipboard-list"></i>';
                                            }

                                            if ($confirmado == 'no' && $datos_consecutivo['numero'] != '' && $_SESSION['rol'] == 5) {

                                                $url_confirmado = BASE_URL . 'programacion/confirmar?reserva=' .
                                                base64_encode($id_reserva) . '&fecha_ini=' . base64_encode($fecha_ini)
                                                . '&fecha_fin=' . base64_encode($fecha_fin);
                                                $visible_confirmacion = '';
                                                $tooltip              = 'Confirmar Reserva';
                                                $icon                 = '<i class="fa fa-check"></i>';
                                            }
                                            /*-------------------------------------------------*/
                                            if ($datos_consecutivo['numero'] != '' && $datos_consecutivo_cert['numero'] != '' && $_SESSION['rol'] != 5 && $conciliacion == 'si') {

                                                $url_certificado = BASE_URL . 'imprimir/imprimirCertificado?reserva=' .
                                                base64_encode($id_reserva) . '&fecha=' . base64_encode($fecha);
                                                $visible_certificado = '';

                                                $target_certificado = '_blank';
                                            } else if ($datos_consecutivo['numero'] != '' && $_SESSION['rol'] != 5 && $conciliacion == 'si') {

                                                $url_certificado = BASE_URL . 'programacion/certificado?reserva=' .
                                                base64_encode($id_reserva) . '&fecha_ini=' . base64_encode($fecha_ini)
                                                . '&fecha_fin=' . base64_encode($fecha_fin);
                                                $visible_certificado = '';
                                                $target_certificado  = '';
                                            }

                                            /*------------------------------------------------*/
                                            if ($_SESSION['rol'] == 5 && $confirmado == 'si') {
                                                $alert         = '<span class="badge badge-success">Confirmado</span>';
                                                $visible_alert = '';
                                            } else if ($_SESSION['rol'] == 5 && $confirmado == 'no' && $datos_consecutivo['numero'] == '') {
                                                $alert         = '<span class="badge badge-warning">Pendiente Confirmar</span>';
                                                $visible_alert = '';
                                            } else if ($_SESSION['rol'] == 5 && $confirmado == 'no') {
                                                $visible_alert = 'd-none';
                                            }

                                            if (in_array($_SESSION['rol'], $permitido_facturar) && $conciliacion == 'si' && $prefacturado['id'] == '') {
                                                $url_prefacturar = BASE_URL . 'prefactura/index?reserva=' .
                                                base64_encode($id_reserva) . '&fecha_ini=' . base64_encode($fecha_ini)
                                                . '&fecha_fin=' . base64_encode($fecha_fin);
                                                $visible_prefacturar = '';
                                                $target_prefacturar  = '';
                                                $color_pre           = 'btn-secondary';
                                                $icon_pre            = '<i class="fas fa-money-check-alt"></i>';
                                                $tooltip_pre         = 'Prefacturar';
                                            } else if (in_array($_SESSION['rol'], $permitido_facturar) && $conciliacion == 'si' && $prefacturado['id'] != '') {
                                                $url_prefacturar     = BASE_URL . 'imprimir/imprimirPrefactura?reserva=' . base64_encode($id_reserva);
                                                $visible_prefacturar = '';
                                                $target_prefacturar  = '_blank';
                                                $color_pre           = 'btn-info';
                                                $icon_pre            = '<i class="fas fa-file-invoice-dollar"></i>';
                                                $tooltip_pre         = 'Descargar Prefactura';
                                            } else {
                                                $visible_prefacturar = 'd-none';
                                            }

                                            $alert_recepcion = ($recepcion == 0) ? '<span class="badge badge-info">Reserva</span>' : '<span class="badge badge-danger">Recepcion</span>';

                                            ?>
                                            <tr class="text-center">
                                                <td class="d-none"><?=$id_vehiculo?></td>
                                                <td><?=$alert_recepcion?></td>
                                                <td><?=$id_reserva?></td>
                                                <td><?=$vehiculo?></td>
                                                <td><?=$empresa?></td>
                                                <td><?=$sucursal?></td>
                                                <td><?=$direccion?></td>
                                                <td><?=$telefono?></td>
                                                <td><?=$contacto?></td>
                                                <td><?=$fecha?></td>
                                                <td><?=$hora_inicio?></td>
                                                <td><?=$hora_fin?></td>
                                                <td><?=$observacion?></td>
                                                <td class="<?=$visible_alert?>">
                                                    <?=$alert?>
                                                </td>
                                                <td class="<?=$visible_movilizacion?>">
                                                    <a href="<?=$url_movilizacion?>" class="btn btn-sm btn-primary" target="<?=$target_movilizacion?>" data-toggle="tooltip" data-placement="bottom" title="Descargar Movilizacion" data-trigger="hover">
                                                        <i class="fa fa-download"></i>
                                                    </a>
                                                </td>
                                                <td class="<?=$visible_confirmacion?>">
                                                    <a href="<?=$url_confirmado?>" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="bottom" title="<?=$tooltip?>" data-trigger="hover">
                                                        <?=$icon?>
                                                    </a>
                                                </td>
                                                <td class="<?=$visible_certificado?>">
                                                    <a href="<?=$url_certificado?>" target="<?=$target_certificado?>" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Descargar Disposicion Final" data-trigger="hover">
                                                        <i class="fa fa-file-pdf"></i>
                                                    </a>
                                                </td>
                                                <td class="<?=$visible_recepcion?>">
                                                    <a href="<?=$url_certificado_recepcion?>" target="_blank" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="bottom" title="Descargar Certificado Recepcion" data-trigger="hover">
                                                        <i class="fa fa-file-pdf"></i>
                                                    </a>
                                                </td>
                                                <td class="<?=$visible_prefacturar?>">
                                                    <a href="<?=$url_prefacturar?>" class="btn <?=$color_pre?> btn-sm" target="<?=$target_prefacturar?>" data-toggle="tooltip" data-placement="bottom" title="<?=$tooltip_pre?>" data-trigger="hover">
                                                        <?=$icon_pre?>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                    <a href="<?=$url_imprimir?>" target="_blank" class="btn btn-sm btn-secondary float-left mt-3 ml-3 mb-3 <?=$visible_imprimir?>" data-tooltip="tooltip" data-placement="bottom" data-trigger="hover" title="Imprimir">
                                        <i class="fa fa-print"></i>
                                        &nbsp;
                                        Descargar Itinerario
                                    </a>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=PUBLIC_PATH?>js/programacion/funcionesProgramacion.js"></script>