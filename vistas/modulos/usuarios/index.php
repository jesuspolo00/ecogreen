<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
include_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
$id_modulo = 1;
$instancia = ControlUsuario::singleton_usuario();
$instancia_perfil = ControlPerfil::singleton_perfil();

$datos_perfil = $instancia_perfil->mostrarPerfilesControl($id_super_empresa);
$datos_usuarios = $instancia->mostrarDatosUsuariosControl($id_super_empresa);
$usuarios_activos = $instancia->contarUsuariosActivosControl($id_super_empresa);
$usuarios_inactivos = $instancia->contarUsuariosInactivosControl($id_super_empresa);
$usuarios_totales = $instancia->contarUsuariosTotalesControl($id_super_empresa);
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?= BASE_URL ?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Usuarios
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
							<div class="dropdown-header">Acciones:</div>
							<?php
							$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 1, 6);
							if ($permiso) {
							?>
								<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_usuario">Agregar Usuario</a>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8 form-inline">
							<h6 class="text-success ml-4">Usuarios Activos:
								<span class="text-dark"><?= $usuarios_activos['id'] ?></span>
							</h6>
							<h6 class="text-danger ml-4">Usuarios Inactivos:
								<span class="text-dark"><?= $usuarios_inactivos['id'] ?></span>
							</h6>
							<h6 class="text-primary ml-4">Usuarios Totales:
								<span class="text-dark"><?= $usuarios_totales['id'] ?></span>
							</h6>
						</div>
						<div class="col-lg-4">
							<form>
								<div class="form-group">
									<div class="input-group mb-3">
										<input type="text" class="form-control filtro" placeholder="Buscar">
										<div class="input-group-prepend">
											<span class="input-group-text rounded-right" id="basic-addon1">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-hover table-borderless table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">#</th>
									<th scope="col">Documento</th>
									<th scope="col">Nombre</th>
									<th scope="col">Apellido</th>
									<th scope="col">Correo</th>
									<th scope="col">Telefono</th>
									<th scope="col">Usuario</th>
									<th scope="col">Perfil</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_usuarios as $datos) {
									$id_user = $datos['id_user'];
									$cedula = $datos['documento'];
									$nombre = $datos['nombre'];
									$apellido = $datos['apellido'];
									$correo = $datos['correo'];
									$telefono = $datos['telefono'];
									$user = $datos['user'];
									$estado = $datos['estado'];
									$id_perfil_user = $datos['perfil'];
									$pass_old = $datos['pass'];
									$id_rol = $datos['perfil'];

									$perfil_user = $instancia_perfil->mostrarDatosPerfilControl($id_user, $id_super_empresa);

									$perfil = $perfil_user['nom_perfil'];

									if ($id_user != $_SESSION['id'] && $id_rol != 4) {
										$ver = ($estado != 'inactivo' && $_SESSION['rol'] != 4) ? '' : 'd-none';
										$ver_admin = ($_SESSION['rol'] == 4) ? $ver = '' : '';
								?>
										<tr class="text-center <?= $ver ?> <?= $ver_admin ?>" id="usuario<?= $id_user; ?>">
											<td><?= $id_user ?></td>
											<td><?= $cedula ?></td>
											<td><?= $nombre ?></td>
											<td><?= $apellido ?></td>
											<td><?= $correo ?></td>
											<td><?= $telefono ?></td>
											<td><?= $user ?></td>
											<td><?= $perfil ?></td>
											<?php
											$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 1, 1);
											if ($permiso) {
											?>
												<td>
													<button class="btn btn-sm btn-primary editar_user" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" data-target="#editar_user<?= $id_user ?>" title="Editar Usuario" data-trigger="hover">
														<i class="fa fa-user-edit"></i>
													</button>
												</td>
												<?php
											}
											$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 1, 2);
											if ($permiso) {
												if ($estado == 'activo') {
												?>
													<td>
														<button class="btn btn-sm btn-danger inactivar_user" id="<?= $id_user ?>" data-toggle="tooltip" data-placement="bottom" title="Inactivar Usuario" data-trigger="hover">
															<i class="fa fa-times"></i>
														</button>
													</td>
												<?php
												} else {
												?>
													<td>
														<button class="btn btn-sm btn-success activar_user" id="<?= $id_user ?>" data-toggle="tooltip" data-placement="bottom" title="Activar Usuario" data-trigger="hover">
															<i class="fa fa-check"></i>
														</button>
													</td>
												<?php
												}
											}
											$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 1, 3);
											if ($permiso) {
												?>
												<td>
													<button class="btn btn-sm btn-secondary eliminar_user" id="<?= $id_user ?>" data-toggle="tooltip" data-placement="bottom" title="Eliminar Usuario" data-trigger="hover">
														<i class="fa fa-trash"></i>
													</button>
												</td>
											<?php
											}
											?>
										</tr>

										<!--Editar usuario-->
										<div class="modal fade" id="editar_user<?= $id_user ?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
											<div class="modal-dialog modal-lg p-2" role="document">
												<div class="modal-content">
													<form method="POST" id="form_enviar_editare-<?= $id_user ?>">
														<input type="hidden" name="id_user" value="<?= $id_user ?>">
														<input type="hidden" name="pass_old" value="<?= $pass_old ?>">
														<div class="modal-header p-3">
															<h4 class="modal-title text-success font-weight-bold">Editar Usuario</h4>
														</div>
														<div class="modal-body border-0">
															<div class="row  p-3">
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Documento</label>
																		<input type="text" readonly class="form-control numeros" maxlength="50" minlength="1" name="documento_edit" value="<?= $cedula ?>" required>
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Nombre</label>
																		<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nombre_edit" required value="<?= $nombre ?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Apellido</label>
																		<input type="text" class="form-control letras" maxlength="50" minlength="1" name="apellido_edit" required value="<?= $apellido ?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Telefono</label>
																		<input type="text" class="form-control numeros" maxlength="50" minlength="1" name="telefono_edit" required value="<?= $telefono ?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Correo</label>
																		<input type="email" class="form-control" maxlength="100" minlength="1" name="correo" value="<?= $correo ?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Usuario</label>
																		<input type="text" class="form-control" maxlength="100" minlength="1" name="usuario_edit" required readonly value="<?= $user ?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Contrase&ntilde;a</label>
																		<input type="password" class="form-control" maxlength="16" minlength="8" name="password" id="pass_editare-<?= $id_user ?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Confirmar Contrase&ntilde;a</label>
																		<input type="password" class="form-control" maxlength="16" minlength="8" name="conf_password" id="conf_pass_editare-<?= $id_user ?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Perfil</label>
																		<select class="form-control" name="perfil_edit" required>
																			<option selected value="<?= $id_perfil_user ?>" class="d-none"><?= $perfil ?></option>
																			<?php
																			foreach ($datos_perfil as $perfiles) {
																				$id_perfil = $perfiles['id_perfil'];
																				$nom_perfil = $perfiles['nombre'];
																			?>
																				<option value="<?= $id_perfil ?>"><?= $nom_perfil ?></option>
																			<?php
																			}
																			?>
																		</select>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
															<input type="button" class="btn btn-success enviar_editar" value="Guardar Cambios" id="e-<?= $id_user ?>">
														</div>
													</form>
												</div>
											</div>
										</div>

								<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . DS . 'modulos' . DS . 'usuarios' . DS . 'agregarUsuario.php';
if (isset($_POST['documento'])) {
	$instancia->registrarUsuarioControl();
}

if (isset($_POST['nombre_edit'])) {
	$instancia->editarUsuarioControl();
}
include_once VISTA_PATH . 'modulos' . DS . 'configuracion' . DS . 'alerta.php';
?>
<script type="text/javascript" src="<?= PUBLIC_PATH ?>js/configuracion/efectos.js"></script>
<script src="<?= PUBLIC_PATH ?>js/usuario/funcionesUsuario.js"></script>
<script type="text/javascript" src="<?= PUBLIC_PATH ?>js/validaciones.js"></script>