<!--Agregar usuario-->
<div class="modal fade" id="agregar_usuario" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-lg p-2" role="document">
		<div class="modal-content">
			<form method="POST" id="form_enviar">
				<input type="hidden" value="<?= $_SESSION['super_empresa']?>" name="super_empresa">
				<input type="hidden" value="<?= $_SESSION['id']?>" name="id_log">
				<div class="modal-header p-3">
					<h4 class="modal-title text-success font-weight-bold">Agregar Usuario</h4>
				</div>
				<div class="modal-body border-0">
					<div class="row  p-3">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Documento</label>
								<input type="text" class="form-control numeros" maxlength="50" minlength="1" name="documento" id="doc_user" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Nombre</label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nombre" id="nombre" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Apellido</label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="apellido" id="apellido" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Telefono</label>
								<input type="text" class="form-control numeros" maxlength="50" minlength="1" name="telefono" id="telefono" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Correo</label>
								<input type="email" class="form-control" maxlength="50" minlength="1" name="correo" id="correo">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Usuario</label>
								<input type="text" class="form-control user" maxlength="50" minlength="1" name="usuario" id="user_name" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Contrase&ntilde;a</label>
								<input type="password" class="form-control" maxlength="16" minlength="8" name="password" id="password" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Confirmar Contrase&ntilde;a</label>
								<input type="password" class="form-control" maxlength="16" minlength="8" name="conf_password" id="conf_password" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Perfil</label>
								<select class="form-control" name="perfil" id="perfil" required>
									<option selected value="">Seleccione una opcion...</option>
									<?php
									foreach ($datos_perfil as $perfiles) {
										$id_perfil = $perfiles['id_perfil'];
										$nom_perfil = $perfiles['nombre'];

										if ($id_perfil != 4) {
									?>
											<option value="<?= $id_perfil ?>"><?= $nom_perfil ?></option>
									<?php
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-success" id="enviar_datos">Registrar</button>
				</div>
			</form>
		</div>
	</div>
</div>