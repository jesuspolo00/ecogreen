<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'reportes' . DS . 'ControlReportes.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';


$instancia = ControlReportes::singleton_reportes();
$datos_reporte_dano = $instancia->MostrarReportesDanoControl($id_super_empresa);
$datos_reporte_mant = $instancia->MostrarReportesMantControl($id_super_empresa);
$datos_reporte_otro = $instancia->MostrarReportesOtroControl($id_super_empresa);

$instancia_vehiculo = ControlVehiculo::singleton_vehiculo();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						Reportes
					</h4>
				</div>
				<div class="card-body">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item col-lg-4 text-center">
							<a class="nav-link active" id="dano-tab" data-toggle="tab" href="#dano" role="tab" aria-controls="dano" aria-selected="true">Da&ntilde;os</a>
						</li>
						<li class="nav-item col-lg-4 text-center">
							<a class="nav-link" id="profile-tab" data-toggle="tab" href="#mantenimiento" role="tab" aria-controls="profile" aria-selected="false">Mantenimientos</a>
						</li>
						<li class="nav-item col-lg-4 text-center">
							<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Otros</a>
						</li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active p-2" id="dano" role="tabpanel">
							<div class="row">
								<div class="col-lg-8"></div>
								<div class="col-lg-4 mt-3">
									<form>
										<div class="form-group">
											<div class="input-group mb-3">
												<input type="text" class="form-control filtro" placeholder="Buscar">
												<div class="input-group-prepend">
													<span class="input-group-text rounded-right" id="basic-addon1">
														<i class="fa fa-search"></i>
													</span>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="table-responsive">
									<table class="table table-hover table-borderless table-sm" width="100%" cellspacing="0">
										<thead>
											<tr class="text-center font-weight-bold">
												<th scope="col">#</th>
												<th scope="col">Descripcion</th>
												<th scope="col">Placa</th>
												<th scope="col">Modelo</th>
												<th scope="col">Marca</th>
												<th scope="col">Fecha Reportado</th>
												<th scope="col">Observacion</th>
											</tr>
										</thead>
										<tbody class="buscar">
											<?php
											foreach ($datos_reporte_dano as $datos) {
												$id_reporte = $datos['id_reporte'];
												$id_inventario = $datos['id_inventario'];
												$tipo_mant = $datos['tipo_mant'];
												$observacion = $datos['observacion'];
												$fecha_reportado = $datos['fecha_reportado'];
												$estado = $datos['estado'];

												if ($tipo_mant == 1) {
													$nom_estado = 'Da&ntilde;ado';
												} else if ($tipo_mant == 2) {
													$nom_estado = 'Mantenimiento';
												} else if ($tipo_mant == 3) {
													$nom_estado = 'Otro';
												} else if ($tipo_mant == 4) {
													$nom_estado = 'Arreglado';
												}

												$datos_vehiculo = $instancia_vehiculo->mostrarDatosVehiculosIdControl($id_inventario);
												$nombre_vehiculo = $datos_vehiculo['descripcion'];
												$placa = $datos_vehiculo['placa'];
												$modelo = $datos_vehiculo['modelo'];
												$marca = $datos_vehiculo['marca'];
												$id_tipo = $datos_vehiculo['id_tipo'];
												$id_user = $datos_vehiculo['id_user'];
											?>


												<tr class="text-center">
													<td><?= $id_reporte ?></td>
													<td><?= $nombre_vehiculo ?></td>
													<td><?= $placa ?></td>
													<td><?= $modelo ?></td>
													<td><?= $marca ?></td>
													<td><?= $fecha_reportado ?></td>
													<td><?= $observacion ?></td>
													<td>
														<a href="<?= BASE_URL ?>imprimir/imprimirReporte?vehiculo=<?= base64_encode($id_inventario) ?>" class="btn btn-sm btn-primary descargar_reporte" target="_blank" data-tooltip="tooltip" data-placement="bottom" title="Descargar Reporte" data-trigger="hover">
															<i class="fas fa-download"></i>
														</a>
													</td>
													<td>
														<button class="btn btn-sm btn-success reportar_arreglo" data-target="#reportar_arreglo<?= $id_reporte ?>" data-toggle="modal" data-tooltip="tooltip" data-trigger="hover" title="Reportar Solucion" data-placement="bottom">
															<i class="fas fa-tools"></i>
														</button>
													</td>
												</tr>


												<!--Reportar Solucion-->
												<div class="modal fade" id="reportar_arreglo<?= $id_reporte ?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
													<div class="modal-dialog modal-lg p-2" role="document">
														<div class="modal-content">
															<form method="POST">
																<input type="hidden" value="<?= $id_reporte ?>" name="id_reporte">
																<input type="hidden" value="<?= $id_inventario ?>" name="id_inventario">
																<input type="hidden" name="id_resp" value="<?= $_SESSION['id'] ?>">
																<div class="modal-header p-3">
																	<h4 class="modal-title text-success font-weight-bold">Reportar Solucion</h4>
																</div>
																<div class="modal-body border-0">
																	<div class="row p-3">
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Descripcion</label>
																				<input type="text" readonly value="<?= $nombre_vehiculo ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Placa</label>
																				<input type="text" readonly value="<?= $placa ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Modelo</label>
																				<input type="text" readonly value="<?= $modelo ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Marca</label>
																				<input type="text" readonly value="<?= $marca ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Fecha Reportado</label>
																				<input type="text" readonly value="<?= $fecha_reportado ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Tipo de reporte</label>
																				<input type="text" readonly value="<?= $nom_estado ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-12">
																			<div class="form-group">
																				<label>Observacion</label>
																				<textarea disabled class="form-control"><?= $observacion; ?></textarea>
																			</div>
																		</div>
																		<div class="col-lg-12">
																			<div class="form-group">
																				<label>Observacion de arreglo</label>
																				<textarea name="observacion_sol" cols="30" rows="5" class="form-control"></textarea>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer border-0">
																	<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
																	<input type="submit" class="btn btn-success" value="Reportar Solucion">
																</div>
															</form>
														</div>
													</div>
												</div>

											<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="tab-pane fade p-2" id="mantenimiento" role="tabpanel">
							<div class="row">
								<div class="col-lg-8"></div>
								<div class="col-lg-4 mt-3">
									<form>
										<div class="form-group">
											<div class="input-group mb-3">
												<input type="text" class="form-control filtro" placeholder="Buscar">
												<div class="input-group-prepend">
													<span class="input-group-text rounded-right" id="basic-addon1">
														<i class="fa fa-search"></i>
													</span>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="table-responsive">
									<table class="table table-hover table-borderless" width="100%" cellspacing="0">
										<thead>
											<tr class="text-center font-weight-bold">
												<th scope="col">#</th>
												<th scope="col">Descripcion</th>
												<th scope="col">Placa</th>
												<th scope="col">Modelo</th>
												<th scope="col">Marca</th>
												<th scope="col">Fecha Reportado</th>
												<th scope="col">Observacion</th>
											</tr>
										</thead>
										<tbody class="buscar">
											<?php
											foreach ($datos_reporte_mant as $datos) {
												$id_reporte = $datos['id_reporte'];
												$id_inventario = $datos['id_inventario'];
												$tipo_mant = $datos['tipo_mant'];
												$observacion = $datos['observacion'];
												$fecha_reportado = $datos['fecha_reportado'];
												$estado = $datos['estado'];

												if ($tipo_mant == 1) {
													$nom_estado = 'Da&ntilde;ado';
												} else if ($tipo_mant == 2) {
													$nom_estado = 'Mantenimiento';
												} else if ($tipo_mant == 3) {
													$nom_estado = 'Otro';
												} else if ($tipo_mant == 4) {
													$nom_estado = 'Arreglado';
												}

												$datos_vehiculo = $instancia_vehiculo->mostrarDatosVehiculosIdControl($id_inventario);
												$nombre_vehiculo = $datos_vehiculo['descripcion'];
												$placa = $datos_vehiculo['placa'];
												$modelo = $datos_vehiculo['modelo'];
												$marca = $datos_vehiculo['marca'];
												$id_tipo = $datos_vehiculo['id_tipo'];
												$id_user = $datos_vehiculo['id_user'];
											?>


												<tr class="text-center">
													<td><?= $id_reporte ?></td>
													<td><?= $nombre_vehiculo ?></td>
													<td><?= $placa ?></td>
													<td><?= $modelo ?></td>
													<td><?= $marca ?></td>
													<td><?= $fecha_reportado ?></td>
													<td><?= $observacion ?></td>
													<td>
														<a href="<?= BASE_URL ?>imprimir/imprimirReporte?vehiculo=<?= base64_encode($id_inventario) ?>" class="btn btn-sm btn-primary descargar_reporte" target="_blank" data-tooltip="tooltip" data-placement="bottom" title="Descargar Reporte" data-trigger="hover">
															<i class="fas fa-download"></i>
														</a>
													</td>
													<td>
														<button class="btn btn-sm btn-success reportar_arreglo" data-target="#reportar_arreglo<?= $id_reporte ?>" data-toggle="modal" data-tooltip="tooltip" data-trigger="hover" title="Reportar Solucion" data-placement="bottom">
															<i class="fas fa-tools"></i>
														</button>
													</td>
												</tr>


												<!--Reportar Solucion-->
												<div class="modal fade" id="reportar_arreglo<?= $id_reporte ?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
													<div class="modal-dialog modal-lg p-2" role="document">
														<div class="modal-content">
															<form method="POST">
																<input type="hidden" value="<?= $id_reporte ?>" name="id_reporte">
																<input type="hidden" value="<?= $id_inventario ?>" name="id_inventario">
																<input type="hidden" name="id_resp" value="<?= $_SESSION['id'] ?>">
																<div class="modal-header p-3">
																	<h4 class="modal-title text-success font-weight-bold">Reportar Solucion</h4>
																</div>
																<div class="modal-body border-0">
																	<div class="row p-3">
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Descripcion</label>
																				<input type="text" readonly value="<?= $nombre_vehiculo ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Placa</label>
																				<input type="text" readonly value="<?= $placa ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Modelo</label>
																				<input type="text" readonly value="<?= $modelo ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Marca</label>
																				<input type="text" readonly value="<?= $marca ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Fecha Reportado</label>
																				<input type="text" readonly value="<?= $fecha_reportado ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Tipo de reporte</label>
																				<input type="text" readonly value="<?= $nom_estado ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-12">
																			<div class="form-group">
																				<label>Observacion</label>
																				<textarea disabled class="form-control"><?= $observacion; ?></textarea>
																			</div>
																		</div>
																		<div class="col-lg-12">
																			<div class="form-group">
																				<label>Observacion de arreglo</label>
																				<textarea name="observacion_sol" cols="30" rows="5" class="form-control"></textarea>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer border-0">
																	<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
																	<input type="submit" class="btn btn-success" value="Reportar Solucion">
																</div>
															</form>
														</div>
													</div>
												</div>
											<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="tab-pane fade p-2" id="contact" role="tabpanel">
							<div class="row">
								<div class="col-lg-8"></div>
								<div class="col-lg-4 mt-3">
									<form>
										<div class="form-group">
											<div class="input-group mb-3">
												<input type="text" class="form-control filtro" placeholder="Buscar">
												<div class="input-group-prepend">
													<span class="input-group-text rounded-right" id="basic-addon1">
														<i class="fa fa-search"></i>
													</span>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="table-responsive">
									<table class="table table-hover table-borderless" width="100%" cellspacing="0">
										<thead>
											<tr class="text-center font-weight-bold">
												<th scope="col">#</th>
												<th scope="col">Descripcion</th>
												<th scope="col">Placa</th>
												<th scope="col">Modelo</th>
												<th scope="col">Marca</th>
												<th scope="col">Fecha Reportado</th>
												<th scope="col">Observacion</th>
											</tr>
										</thead>
										<tbody class="buscar">
											<?php
											foreach ($datos_reporte_otro as $datos) {
												$id_reporte = $datos['id_reporte'];
												$id_inventario = $datos['id_inventario'];
												$tipo_mant = $datos['tipo_mant'];
												$observacion = $datos['observacion'];
												$fecha_reportado = $datos['fecha_reportado'];
												$estado = $datos['estado'];

												if ($tipo_mant == 1) {
													$nom_estado = 'Da&ntilde;ado';
												} else if ($tipo_mant == 2) {
													$nom_estado = 'Mantenimiento';
												} else if ($tipo_mant == 3) {
													$nom_estado = 'Otro';
												} else if ($tipo_mant == 4) {
													$nom_estado = 'Arreglado';
												}

												$datos_vehiculo = $instancia_vehiculo->mostrarDatosVehiculosIdControl($id_inventario);
												$nombre_vehiculo = $datos_vehiculo['descripcion'];
												$placa = $datos_vehiculo['placa'];
												$modelo = $datos_vehiculo['modelo'];
												$marca = $datos_vehiculo['marca'];
												$id_tipo = $datos_vehiculo['id_tipo'];
												$id_user = $datos_vehiculo['id_user'];
											?>


												<tr class="text-center">
													<td><?= $id_reporte ?></td>
													<td><?= $nombre_vehiculo ?></td>
													<td><?= $placa ?></td>
													<td><?= $modelo ?></td>
													<td><?= $marca ?></td>
													<td><?= $fecha_reportado ?></td>
													<td><?= $observacion ?></td>
													<td>
														<a href="<?= BASE_URL ?>imprimir/imprimirReporte?vehiculo=<?= base64_encode($id_inventario) ?>" class="btn btn-sm btn-primary descargar_reporte" target="_blank" data-tooltip="tooltip" data-placement="bottom" title="Descargar Reporte" data-trigger="hover">
															<i class="fas fa-download"></i>
														</a>
													</td>
													<td>
														<button class="btn btn-sm btn-success reportar_arreglo" data-target="#reportar_arreglo<?= $id_reporte ?>" data-toggle="modal" data-tooltip="tooltip" data-trigger="hover" title="Reportar Solucion" data-placement="bottom">
															<i class="fas fa-tools"></i>
														</button>
													</td>
												</tr>


												<!--Reportar Solucion-->
												<div class="modal fade" id="reportar_arreglo<?= $id_reporte ?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
													<div class="modal-dialog modal-lg p-2" role="document">
														<div class="modal-content">
															<form method="POST">
																<input type="hidden" value="<?= $id_reporte ?>" name="id_reporte">
																<input type="hidden" value="<?= $id_inventario ?>" name="id_inventario">
																<input type="hidden" name="id_resp" value="<?= $_SESSION['id'] ?>">
																<div class="modal-header p-3">
																	<h4 class="modal-title text-success font-weight-bold">Reportar Solucion</h4>
																</div>
																<div class="modal-body border-0">
																	<div class="row p-3">
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Descripcion</label>
																				<input type="text" readonly value="<?= $nombre_vehiculo ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Placa</label>
																				<input type="text" readonly value="<?= $placa ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Modelo</label>
																				<input type="text" readonly value="<?= $modelo ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Marca</label>
																				<input type="text" readonly value="<?= $marca ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Fecha Reportado</label>
																				<input type="text" readonly value="<?= $fecha_reportado ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-6">
																			<div class="form-group">
																				<label>Tipo de reporte</label>
																				<input type="text" readonly value="<?= $nom_estado ?>" class="form-control">
																			</div>
																		</div>
																		<div class="col-lg-12">
																			<div class="form-group">
																				<label>Observacion</label>
																				<textarea disabled class="form-control"><?= $observacion; ?></textarea>
																			</div>
																		</div>
																		<div class="col-lg-12">
																			<div class="form-group">
																				<label>Observacion de arreglo</label>
																				<textarea name="observacion_sol" cols="30" rows="5" class="form-control"></textarea>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer border-0">
																	<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
																	<input type="submit" class="btn btn-success" value="Reportar Solucion">
																</div>
															</form>
														</div>
													</div>
												</div>
											<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . DS . 'modulos' . DS . 'usuarios' . DS . 'agregarUsuario.php';

if (isset($_POST['id_resp'])) {
	$instancia->reportarSolucionControl();
}
?>