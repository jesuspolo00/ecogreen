<!DOCTYPE html>
<html lang="en">

<head>
    <title>Ecogreen</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <meta name="description" content="Apartado de vehiculos ECOGREEN Recycling">
    <link rel="apple-touch-icon" sizes="57x57" href="<?=PUBLIC_PATH?>img/icono.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=PUBLIC_PATH?>img/icono.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=PUBLIC_PATH?>img/icono.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=PUBLIC_PATH?>img/icono.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=PUBLIC_PATH?>img/icono.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=PUBLIC_PATH?>img/icono.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=PUBLIC_PATH?>img/icono.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=PUBLIC_PATH?>img/icono.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=PUBLIC_PATH?>img/icono.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?=PUBLIC_PATH?>img/icono.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=PUBLIC_PATH?>img/icono.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?=PUBLIC_PATH?>img/icono.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=PUBLIC_PATH?>img/icono.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=PUBLIC_PATH?>img/icono.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Custom fonts for this template-->
    <link href="<?=PUBLIC_PATH?>vendor/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="<?=PUBLIC_PATH?>css/bootstrapClockPicker.css">
    <!-- Custom styles for this template-->
    <link href="<?=PUBLIC_PATH?>css/sb-admin-2.css" rel="stylesheet">
    <link href="<?=PUBLIC_PATH?>css/main.css" rel="stylesheet">
</head>

<body class="bg-gradient-success">
    <div id="ohsnap"></div>
    <div id="wrapper">