<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';
/*-----------------Variables de sesion---------------------*/
$id_super_empresa = $_SESSION['super_empresa'];
$id_log           = $_SESSION['id'];
$id_empresa_log   = $_SESSION['empresa'];
/*------------------------------------------------------------*/
$id_modulo          = 2;
$instancia_permisos = ControlPermiso::singleton_permiso();
$instancia          = ControlVehiculo::singleton_vehiculo();

$datos_super_empresa = $instancia_permisos->datosSuperEmpresaControl($id_super_empresa);
$datos_vehiculo      = $instancia->mostrarDatosVehiculosControl($id_super_empresa);
?>
<!-- Sidebar -->
<ul class="navbar-nav bg-white sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=BASE_URL?>inicio">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-seedling text-success"></i>
    </div>
    <div class="sidebar-brand-text mx-3 text-success mt-3">
      <?=$datos_super_empresa['nombre']?>
    </div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="<?=BASE_URL?>inicio">
      <i class="fas fa-home text-success"></i>
      <span class="text-muted">Inicio</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider bg-gray">

  <!-- Nav Item - Pages Collapse Menu -->
  <?php
$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 7, 5);
if ($permiso) {
    ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-list-ul text-success"></i>
        <span class="text-muted">Reservar Vehiculo</span>
      </a>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Vehiculos:</h6>
          <?php
foreach ($datos_vehiculo as $datos) {
        $id_vehiculo     = $datos['id_inventario'];
        $descripcion     = $datos['descripcion'];
        $placa           = $datos['placa'];
        $estado_vehiculo = $datos['estado_vehiculo'];
        $estado          = $datos['estado'];

        if ($estado_vehiculo == 'activo' && $estado == 5) {
            $activo = '';
        } else if ($estado_vehiculo == 'activo' && $estado == 4) {
            $activo = '';
        } else if ($estado == 4 && $estado_vehiculo == 'activo') {
            $activo = '';
        } else if ($estado == 5 && $estado_vehiculo == 'activo') {
            $activo = '';
        } else {
            $activo = 'd-none';
        }
        ?>
            <a class="collapse-item <?=$activo?>" href="<?=BASE_URL?>apartar/index?vehiculo=<?=base64_encode($id_vehiculo);?>"><?=$descripcion . ' (' . $placa . ')'?></a>
          <?php
}
    ?>
        </div>
      </div>
    </li>

  <?php }
$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 8, 5);
if ($permiso) {
    ?>
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?=BASE_URL?>recepcion/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-concierge-bell text-success"></i>
        <span class="text-muted">Recepcion</span>
      </a>
    </li>

  <?php }
$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 9, 5);
if ($permiso) {
    ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?=BASE_URL?>programacion/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-clock text-success"></i>
        <span class="text-muted">Programacion diaria</span>
      </a>
    </li>

  <?php }
$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 10, 5);
if ($permiso) {
    ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?=BASE_URL?>reportes/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-wrench text-success"></i>
        <span class="text-muted">Reportes de da&ntilde;os</span>
      </a>
    </li>

  <?php }
$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 11, 5);
if ($permiso) {
    ?>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?=BASE_URL?>reservas/index" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-clipboard-list text-success"></i>
        <span class="text-muted">Reportes de reservas</span>
      </a>
    </li>
  <?php }
$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 17, 5);
if ($permiso) {
    ?>
    <!--     <li class="nav-item">
      <a class="nav-link collapsed" href="<?=BASE_URL?>prefactura/reporte" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-coins text-success"></i>
        <span class="text-muted">Reportes de prefacturas</span>
      </a>
    </li> -->
  <?php }
$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 18, 5);
if ($permiso) {?>
      <li class="nav-item">
      <a class="nav-link collapsed" href="<?=BASE_URL?>certificados/index" aria-expanded="true" aria-controls="collapseUtilities">
      <i class="fas fa-check-circle text-success"></i>
        <span class="text-muted">Autorizar certificados</span>
      </a>
    </li>
  <?php }?>
  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block bg-gray">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow-none">


      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars text-success"></i>
      </button>

      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
          <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
              <div class="input-group">
                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-success" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>

        <!-- Nav Item - Alerts -->
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <!-- Counter - Alerts -->
            <span class="badge badge-danger badge-counter">3+</span>
          </a>
          <!-- Dropdown - Alerts -->
          <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
            <h6 class="dropdown-header">
              Alerts Center
            </h6>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="mr-3">
                <div class="icon-circle bg-primary">
                  <i class="fas fa-file-alt text-white"></i>
                </div>
              </div>
              <div>
                <div class="small text-gray-500">December 12, 2019</div>
                <span class="font-weight-bold">A new monthly report is ready to download!</span>
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="mr-3">
                <div class="icon-circle bg-success">
                  <i class="fas fa-donate text-white"></i>
                </div>
              </div>
              <div>
                <div class="small text-gray-500">December 7, 2019</div>
                $290.29 has been deposited into your account!
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="mr-3">
                <div class="icon-circle bg-warning">
                  <i class="fas fa-exclamation-triangle text-white"></i>
                </div>
              </div>
              <div>
                <div class="small text-gray-500">December 2, 2019</div>
                Spending Alert: We've noticed unusually high spending for your account.
              </div>
            </a>
            <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
          </div>
        </li>

        <!-- Nav Item - Messages -->
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-envelope fa-fw"></i>
            <!-- Counter - Messages -->
            <span class="badge badge-danger badge-counter">7</span>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">
              Message Center
            </h6>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                <div class="status-indicator bg-success"></div>
              </div>
              <div class="font-weight-bold">
                <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                <div class="small text-gray-500">Emily Fowler · 58m</div>
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                <div class="status-indicator"></div>
              </div>
              <div>
                <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                <div class="small text-gray-500">Jae Chun · 1d</div>
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                <div class="status-indicator bg-warning"></div>
              </div>
              <div>
                <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                <div class="small text-gray-500">Morgan Alvarez · 2d</div>
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                <div class="status-indicator bg-success"></div>
              </div>
              <div>
                <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                <div class="small text-gray-500">Chicken the Dog · 2w</div>
              </div>
            </a>
            <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
          </div>
        </li>

        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?=$_SESSION['nombre_admin'] . ' ' . $_SESSION['apellido']?></span>
            <img class="img-profile rounded-circle" src="<?=PUBLIC_PATH?>img/user.svg">
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <?php
$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 13, 5);
if ($permiso) {
    ?>
              <a class="dropdown-item" href="<?=BASE_URL?>perfil/index">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Perfil
              </a>
            <?php }
$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 12, 5);
if ($permiso) {
    ?>
              <a class="dropdown-item" href="<?=BASE_URL?>configuracion/index">
                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                Configuraci&oacute;n
              </a>
            <?php }
$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 14, 5);
if ($permiso) {
    ?>
              <a class="dropdown-item" href="#">
                <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                Activity Log
              </a>
            <?php }?>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?=BASE_URL?>salir">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Cerrar sesion
            </a>
          </div>
        </li>

      </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <?php
include_once VISTA_PATH . 'script_and_final.php';
?>