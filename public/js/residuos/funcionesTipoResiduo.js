$(document).ready(function(){

	var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');

	$(".inactivar_tipo").on(tipoEvento, function(){
		var id = $(this).attr('id');
		inactivarTipoResiduo(id);
	});


	$(".activar_tipo").on(tipoEvento, function(){
		var id = $(this).attr('id');
		activarTipoResiduo(id);
	});


	$(".eliminar_tipo").on(tipoEvento, function(){
		var id = $(this).attr('id');
		eliminarTipoResiduo(id);
	});


	function inactivarTipoResiduo(id){
		try {
			$.ajax({
				url: '../vistas/ajax/tipos_residuo/inactivarTipoResiduo.php',
				method: 'POST',
				data: {'id_tipo': id},
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						$('#'+id+'').removeAttr('title');
						$('#'+id+'').removeClass('btn-danger inactivar_tipo').addClass('btn-success activar_tipo');
						$('#'+id+' i').removeClass('fa-times').addClass('fa-check');
						ohSnap("Inactivado correctamente!", {color: "yellow", 'duration': '1000'});
						setTimeout(recargarPaginaTipo,2000);
					} else {
						ohSnap("ha ocurrido un error!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}


	function activarTipoResiduo(id){
		try {
			$.ajax({
				url: '../vistas/ajax/tipos_residuo/activarTipoResiduo.php',
				method: 'POST',
				data: {'id_tipo': id},
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						$('#'+id+'').removeAttr('title');
						$('#'+id+'').removeClass('btn-success activar_tipo').addClass('btn-danger inactivar_tipo');
						$('#'+id+' i').removeClass('fa-check').addClass('fa-times');
						ohSnap("Activado correctamente!", {color: "yellow", 'duration': '1000'});
						setTimeout(recargarPaginaTipo,2000);
					} else {
						ohSnap("ha ocurrido un error!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}

	function eliminarTipoResiduo(id){
		try {
			$.ajax({
				url: '../vistas/ajax/tipos_residuo/eliminarTipoResiduo.php',
				method: 'POST',
				data: {'id_tipo': id},
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						ohSnap("Eliminado correctamente!", {color: "green", 'duration': '1000'});
						$('#tipo_residuo'+id).fadeOut();
					} else {
						ohSnap("ha ocurrido un error!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}

	function recargarPaginaTipo(){
		window.location.replace("index");
	}

});