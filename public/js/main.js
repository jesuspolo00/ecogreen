$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-tooltip="tooltip"]').tooltip();

    $('#accordionSidebar').addClass('toggled');

    $(".user").focus();

    $('.filtro').keyup(function() {

        var rex = new RegExp($(this).val(), 'i');
        $('.buscar tr').hide();
        $('.buscar tr').filter(function() {
            return rex.test($(this).text());
        }).show();

    });

    $(".user").keyup(function() {
        minus(this);
        noespacios(this);
    });

    $('.filtro_change').change(function() {

        var rex = new RegExp($(this).val(), 'i');
        $('.buscar tr').hide();
        $('.buscar tr').filter(function() {
            return rex.test($(this).text());
        }).show();

    });


    $(".numeros").keypress(function(e) {
        soloNumeros(e);
    });

    $(".letras").keypress(function(e) {
        return soloLetras(e)
    });

    function soloNumeros(e) {
        var key = window.event ? e.which : e.keyCode;
        if (key < 48 || key > 57) {
            e.preventDefault();
        }
    }


    function minus(e) {
        e.value = e.value.toLowerCase();
    }

    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
        especiales = "8-37-39-46";

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $(".clock").removeClass('clock');
    }



    function noespacios(e) {
        let letras = e.value.replace(/ /g, "");
        e.value = letras;
    }

});