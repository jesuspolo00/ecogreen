$(document).ready(function(){

	var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');


	$(".inactivar_tipo").on(tipoEvento, function(){
		var id = $(this).attr('id');
		inactivarTipoVehiculo(id);
	});


	$(".activar_tipo").on(tipoEvento, function(){
		var id = $(this).attr('id');
		activarTipoVehiculo(id);
	});


	$(".eliminar_tipo").on(tipoEvento, function(){
		var id = $(this).attr('id');
		eliminarTipoVehiculo(id);
	});


	function inactivarTipoVehiculo(id){
		try {
			$.ajax({
				url: '../vistas/ajax/tipo_vehiculo/inactivarTipoVehiculo.php',
				method: 'POST',
				data: {'id_tipo': id},
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						$('#'+id+'').removeAttr('title');
						$('#'+id+'').removeClass('btn-danger inactivar_tipo').addClass('btn-success activar_tipo');
						$('#'+id+' i').removeClass('fa-times').addClass('fa-check');
						ohSnap("Inactivado correctamente!", {color: "yellow", 'duration': '1000'});
						setTimeout(recargarPaginaTipo,2000);
					} else {
						ohSnap("ha ocurrido un error!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}


	function activarTipoVehiculo(id){
		try {
			$.ajax({
				url: '../vistas/ajax/tipo_vehiculo/activarTipoVehiculo.php',
				method: 'POST',
				data: {'id_tipo': id},
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						$('#'+id+'').removeAttr('title');
						$('#'+id+'').removeClass('btn-success activar_tipo').addClass('btn-danger inactivar_tipo');
						$('#'+id+' i').removeClass('fa-check').addClass('fa-times');
						ohSnap("Activado correctamente!", {color: "green", 'duration': '1000'});
						setTimeout(recargarPaginaTipo,2000);
					} else {
						ohSnap("ha ocurrido un error!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}


	function eliminarTipoVehiculo(id){
		try {
			$.ajax({
				url: '../vistas/ajax/tipo_vehiculo/eliminarTipoVehiculo.php',
				method: 'POST',
				data: {'id_tipo': id},
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						ohSnap("Eliminado correctamente!", {color: "green", 'duration': '1000'});
						$('#tipo'+id).fadeOut();
					} else {
						ohSnap("ha ocurrido un error!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}

	function recargarPaginaTipo(){
		window.location.replace("index");
	}

});