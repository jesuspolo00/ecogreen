$(document).ready(function(){

	var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');


	$(".inactivar_vehiculo").on(tipoEvento, function(){
		var id = $(this).attr('id');
		inactivarVehiculo(id);
	});


	$(".activar_vehiculo").on(tipoEvento, function(){
		var id = $(this).attr('id');
		activarVehiculo(id);
	});


	$(".eliminar_vehiculo").on(tipoEvento, function(){
		var id = $(this).attr('id');
		eliminarVehiculo(id);
	});


	function inactivarVehiculo(id){
		try {
			$.ajax({
				url: '../vistas/ajax/vehiculos/inactivarVehiculo.php',
				method: 'POST',
				data: {'id_vehiculo': id},
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						$('#'+id+'').removeAttr('title');
						$('#'+id+'').removeClass('btn-danger inactivar_vehiculo').addClass('btn-success activar_vehiculo');
						$('#'+id+' i').removeClass('fa-times').addClass('fa-check');
						ohSnap("Inactivado correctamente!", {color: "yellow", 'duration': '1000'});
						setTimeout(recargarPagina,1050);
					} else {
						ohSnap("ha ocurrido un error!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}


	function activarVehiculo(id){
		try {
			$.ajax({
				url: '../vistas/ajax/vehiculos/activarVehiculo.php',
				method: 'POST',
				data: {'id_vehiculo': id},
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						$('#'+id+'').removeAttr('title');
						$('#'+id+'').removeClass('btn-success activar_vehiculo').addClass('btn-danger inactivar_vehiculo');
						$('#'+id+' i').removeClass('fa-check').addClass('fa-times');
						ohSnap("Activado correctamente!", {color: "green", 'duration': '1000'});
						setTimeout(recargarPagina,1050);
					} else {
						ohSnap("ha ocurrido un error!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}


	function eliminarVehiculo(id){
		try {
			$.ajax({
				url: '../vistas/ajax/vehiculos/eliminarVehiculo.php',
				method: 'POST',
				data: {'id_vehiculo': id},
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						ohSnap("Eliminado correctamente!", {color: "green", 'duration': '1000'});
						$('#vehiculo'+id).fadeOut();
					} else {
						ohSnap("ha ocurrido un error!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}

	function recargarPagina(){
		window.location.replace("index");
	}

});