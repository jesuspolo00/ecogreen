$(document).ready(function() {

    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');

    $(".inactivar_listado").on(tipoEvento, function() {
        var id = $(this).attr('id');
        inactivarDestino(id);
    });


    $(".activar_listado").on(tipoEvento, function() {
        var id = $(this).attr('id');
        activarDestino(id);
    });


    $(".eliminar_listado").on(tipoEvento, function() {
        var id = $(this).attr('id');
        eliminarDestino(id);
    });


    function inactivarDestino(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/categorias/inactivarListado.php',
                method: 'POST',
                data: { 'id_listado': id },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $('#' + id + '').removeAttr('title');
                        $('#' + id + '').removeClass('btn-danger inactivar_listado').addClass('btn-success activar_listado');
                        $('#' + id + ' i').removeClass('fa-times').addClass('fa-check');
                        ohSnap("Inactivado correctamente!", { color: "yellow", 'duration': '1000' });
                        setTimeout(recargarPaginaTipo, 2000);
                    } else {
                        ohSnap("ha ocurrido un error!", { color: "red", 'duration': '1000' });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }


    function activarDestino(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/categorias/activarListado.php',
                method: 'POST',
                data: { 'id_listado': id },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $('#' + id + '').removeAttr('title');
                        $('#' + id + '').removeClass('btn-success activar_listado').addClass('btn-danger inactivar_listado');
                        $('#' + id + ' i').removeClass('fa-check').addClass('fa-times');
                        ohSnap("Activado correctamente!", { color: "yellow", 'duration': '1000' });
                        setTimeout(recargarPaginaTipo, 2000);
                    } else {
                        ohSnap("ha ocurrido un error!", { color: "red", 'duration': '1000' });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function eliminarDestino(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/categorias/eliminarListado.php',
                method: 'POST',
                data: { 'id_listado': id },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        ohSnap("Eliminado correctamente!", { color: "green", 'duration': '1000' });
                        $('#destino' + id).fadeOut();
                    } else {
                        ohSnap("ha ocurrido un error!", { color: "red", 'duration': '1000' });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPaginaTipo() {
        window.location.replace("index");
    }

});