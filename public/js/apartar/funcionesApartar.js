$(document).ready(function() {

    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $('body').css('overflow', 'hidden');
        $("#alerta").modal({
            visible: "show",
            keyboard: false,
            backdrop: "static"
        });
    }


    $(".check").on(tipoEvento, function() {
        var id = $(this).attr('id');
        if ($(this).is(':checked')) {
            $("#num_" + id).attr('disabled', false);
        } else {
            $("#num_" + id).attr('disabled', true);
        }
    });


    $("#empresa").change(function() {
        var id = $(this).val();
        var campo = $(this);
        mostrarCiudadEmpresa(id, campo);
        mostrarSucursalesEmpresa(id, campo);
    });



    $(".coop").change(function() {
        var val = $(this).val();

        if (val != "") {
            $("#file_coop").attr('disabled', false);
        } else {
            $("#file_coop").attr('disabled', true);
        }
    });

    $("#file").change(function() {
        var id = $(this).attr('id');
        cambiarFile(id);
    });

    $("#file_coop").change(function() {
        var id = $(this).attr('id');
        cambiarFile(id);
    });

    $("#file_otros").change(function() {
        var id = $(this).attr('id');
        cambiarFile(id);
    });


    $("#hora_inicio").change(function() {
        var fecha_apartar = $("#fecha_apartar").val();
        var valor = $(this).val();
        var dt = new Date();
        var minutos = dt.getMinutes();

        if (minutos < 10) {
            minutos = '0' + dt.getMinutes();
        } else {
            minutos = dt.getMinutes();
        }

        var time = dt.getHours() + ":" + minutos;
        /*---------------Fecha Actual------------------------*/
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();

        var output = d.getFullYear() + '-' +
            (month < 10 ? '0' : '') + month + '-' +
            (day < 10 ? '0' : '') + day;
        /*-----------------------------------------------------*/
        if (valor < time && fecha_apartar == output) {
            $(".tooltip").hide();
            $(".tooltip").show();
            $("#hora_inicio").focus();
            $("#hora_inicio").attr('data-toggle', 'tooltip');
            $("#hora_inicio").addClass('border border-danger');
            $("#hora_inicio").tooltip({ title: "La hora debe ser mayor a la actual", trigger: "focus", placement: "right" });
            $("#hora_inicio").tooltip('show');
            $("#hora_fin").attr('disabled', true);
        } else {
            $("#hora_inicio").removeClass('border border-danger').addClass('border border-success');
            $(".tooltip").hide();
            $("#hora_fin").attr('disabled', false);
        }
    });


    $("#hora_fin").change(function() {
        var hora_inicio = $("#hora_inicio").val();
        var valor = $(this).val();
        var dt = new Date();
        var time = dt.getHours() + ":" + dt.getMinutes();
        if (valor <= hora_inicio) {
            $(".tooltip").hide();
            $(".tooltip").show();
            $("#hora_fin").focus();
            $("#hora_fin").attr('data-toggle', 'tooltip');
            $("#hora_fin").addClass('border border-danger');
            $("#hora_fin").tooltip({ title: "La hora debe ser mayor a la inicial", trigger: "focus", placement: "right" });
            $("#hora_fin").tooltip('show');
        } else {
            $("#hora_fin").removeClass('border border-danger').addClass('border border-success');
            $(".tooltip").hide();
        }
    });




    $("#fecha_apartar").change(function() {
        var valor = $(this).val();

        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();

        var output = d.getFullYear() + '-' +
            (month < 10 ? '0' : '') + month + '-' +
            (day < 10 ? '0' : '') + day;

        if (valor < output) {
            $(".tooltip").hide();
            $(".tooltip").show();
            $("#fecha_apartar").focus();
            $("#fecha_apartar").attr('data-toggle', 'tooltip');
            $("#fecha_apartar").addClass('border border-danger');
            $("#fecha_apartar").tooltip({ title: "La fecha debe ser mayor a la actual", trigger: "focus", placement: "right" });
            $("#fecha_apartar").tooltip('show');
            $("#hora_inicio").attr('disabled', true);
        } else {
            $("#fecha_apartar").removeClass('border border-danger').addClass('border border-success');
            $(".tooltip").hide();
            $("#hora_inicio").attr('disabled', false);
        }
    });




    /*function consultarHoras(valor) {
    	try {
    		$.ajax({
    			url: '../vistas/modulos/apartar/consultarHoras.php',
    			method: 'POST',
    			data: { 'fecha': valor },
    			cache: false,
    			success: function (resultado) {
    				if (resultado == 'ok') {
    					$(".tooltip").hide();
    					$(".tooltip").show();
    					$("#hora_fin").focus();
    					$("#hora_fin").attr('data-toggle', 'tooltip');
    					$("#hora_fin").addClass('border border-danger');
    					$("#hora_fin").tooltip({ title: "La fecha ya esta apartada", trigger: "focus", placement: "right" });
    					$("#hora_fin").tooltip('show');
    				} else {

    				}
    			}
    		});
    	} catch (evt) {
    		alert(evt.message);
    	}
    }*/


    function mostrarCiudadEmpresa(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/empresas/mostrarCiudadEmpresa.php',
                method: 'POST',
                data: { 'id_empresa': id },
                cache: false,
                success: function(resultado) {
                    if (resultado != '') {
                        $("#ciudad").val(resultado.replace(/['"]+/g, ''));
                    } else {
                        ohSnap("ha ocurrido un error!", { color: "red", 'duration': '1000' });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }



    function mostrarSucursalesEmpresa(id) {
        $('#sucursales').attr('disabled', false);
        $('#sucursales').html('');
        try {
            $.ajax({
                url: '../vistas/ajax/empresas/mostrarSucursalesEmpresa.php',
                method: 'POST',
                data: { 'id_empresa': id },
                cache: false,
                dataType: 'JSON',
                success: function(resultado) {
                    if (resultado != '') {
                        $('#sucursales').append('<option value="" selected>Seleccione una opcion...</option>');
                        resultado.forEach(function(element) {
                            $('#sucursales').append('<option value="' + element['id_sucursal'] + '">' + element['nombre'] + ' (' + element['nit'] + ')</option>')
                        });
                    } else {
                        $('#sucursales').attr('disabled', true);
                        ohSnap("No hay sucursales para mostrar!", { color: "red", 'duration': '1000' });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }


    function cambiarFile(id) {
        const input = document.getElementById(id);
        var fileSize = input.files[0]['size'];
        var siezekiloByte = parseInt(fileSize / 1024);
        if (input.files && input.files[0] && siezekiloByte < 3072) {
            $("#archivo_" + id).text(input.files[0]['name']);
            $(".tooltip").hide();
        } else {
            $(".tooltip").hide();
            $(".tooltip").show();
            $("#" + id).focus();
            $("#" + id).attr('data-toggle', 'tooltip');
            $("#" + id).addClass('border border-danger');
            $("#" + id).tooltip({ title: "Limite de 3mb excedido", trigger: "focus", placement: "bottom" });
            $("#" + id).tooltip('show');
            $("#" + id).val('');
            $("#archivo_" + id).text('');
        }
    }
});