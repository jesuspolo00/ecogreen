<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'programacion' . DS . 'ModeloProgramacion.php';

class ControlProgramacion
{

    private static $instancia;

    public static function singleton_programacion()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarProgramacionControl()
    {
        $mostrar = ModeloProgramacion::mostrarProgramacionModel();
        return $mostrar;
    }

    public function mostrarConsecutivoReservaControl($id)
    {
        $mostrar = ModeloProgramacion::mostrarConsecutivoReservaModel($id);
        return $mostrar;
    }

    public function mostrarConsecutivoCertReservaControl($id)
    {
        $mostrar = ModeloProgramacion::mostrarConsecutivoCertReservaModel($id);
        return $mostrar;
    }

    public function mostrarProgramacionIdControl($id)
    {
        $mostrar = ModeloProgramacion::mostrarProgramacionIdModel($id);
        return $mostrar;
    }

    public function mostrarResiduosProgramacionControl($id)
    {
        $mostrar = ModeloProgramacion::mostrarResiduosProgramacionModel($id);
        return $mostrar;
    }

    public function mostrarResiduosTipoProgramacionControl($id, $tipo)
    {
        $mostrar = ModeloProgramacion::mostrarResiduosTipoProgramacionModel($id, $tipo);
        return $mostrar;
    }

    public function buscarReservaVehiculoControl($id_vehiculo, $fecha_ini, $fecha_fin)
    {
        $sql    = ($id_vehiculo != 0) ? ' AND r.id_vehiculo = ' . $id_vehiculo : '';
        $buscar = ModeloProgramacion::buscarReservaVehiculoModel($sql, $fecha_ini, $fecha_fin);
        return $buscar;
    }

    public function buscarReservaFechaControl($fecha_ini, $fecha_fin)
    {
        $buscar = ModeloProgramacion::buscarReservaFechaModel($fecha_ini, $fecha_fin);
        return $buscar;
    }

    public function buscarReservaRecepcionFechaControl($fecha_ini, $fecha_fin)
    {
        $buscar = ModeloProgramacion::buscarReservaRecepcionFechaModel($fecha_ini, $fecha_fin);
        return $buscar;
    }

    public function buscarTipoResiduoReservaControl($id)
    {
        $buscar = ModeloProgramacion::buscarTipoResiduoReservaModel($id);
        return $buscar;
    }

    public function mostrarTipoResiduoReservaControl($id)
    {
        $buscar = ModeloProgramacion::mostrarTipoResiduoReservaControl($id);
        return $buscar;
    }

    public function mostrarCantidadResiduosReservaControl($id)
    {
        $buscar = ModeloProgramacion::mostrarCantidadResiduosReservaModel($id);
        return $buscar;
    }

    public function sumarCantidadResiduosReservaControl($id)
    {
        $buscar = ModeloProgramacion::sumarCantidadResiduosReservaModel($id);
        return $buscar;
    }

    public function mostrarApartadoResiduosReservaControl($id)
    {
        $mostrar = ModeloProgramacion::mostrarApartadoResiduosReservaControl($id);
        return $mostrar;
    }

    public function consultarPrefacturaControl($id)
    {
        $mostrar = ModeloProgramacion::consultarPrefacturaModel($id);
        return $mostrar;
    }

    public function certificadosSolicitadosControl()
    {
        $mostrar = ModeloProgramacion::certificadosSolicitadosModel();
        return $mostrar;
    }

    public function guardarConsecutivoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['reserva']) &&
            !empty($_POST['reserva']) &&
            isset($_POST['consecutivo']) &&
            !empty($_POST['consecutivo'])
        ) {

            $fechareg = date('Y-m-d H:i:s');

            $mostrar = ModeloProgramacion::validarConsecutivoReservaModel($_POST['consecutivo']);

            if ($mostrar['numero'] != '') {
                echo '
			<script>
			ohSnap("Consecutivo ya existe!", {color: "red"});
			</script>
			';
            } else {

                $datos = array(
                    'id_reserva' => $_POST['reserva'],
                    'fechareg'   => $fechareg,
                    'numero'     => $_POST['consecutivo'],
                    'id_log'     => $_POST['id_log'],
                );

                $guardar = ModeloProgramacion::guardarConsecutivoModel($datos);

                if ($guardar == true) {
                    echo '
				<script>
				ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
				setTimeout(recargarPagina,1050);

				function recargarPagina(){
					window.open("' . BASE_URL . 'imprimir/imprimirMovilizacion?reserva=' .
                    base64_encode($_POST['reserva']) . '&fecha_ini=' . base64_encode($_POST['fecha_ini'])
                    . '&fecha_fin=' . base64_encode($_POST['fecha_fin']) . '", "_blank");
					window.location.replace("index");
				}
				</script>
				';
                } else {
                    echo '
				<script>
				ohSnap("Ha ocurrido un error", {color: "red"});
				</script>
				';
                }
            }
        } else {
        }
    }

    public function guardarConsecutivoCertControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['reserva']) &&
            !empty($_POST['reserva']) &&
            isset($_POST['consecutivo']) &&
            !empty($_POST['consecutivo'])
        ) {

            $fechareg = date('Y-m-d H:i:s');

            $mostrar = ModeloProgramacion::validarConsecutivoCertReservaModel($_POST['consecutivo']);

            if ($mostrar['numero'] != '') {
                echo '
				<script>
				ohSnap("Consecutivo ya existe!", {color: "red"});
				</script>
				';
            } else {

                $datos = array(
                    'id_reserva' => $_POST['reserva'],
                    'fechareg'   => $fechareg,
                    'numero'     => $_POST['consecutivo'],
                    'id_log'     => $_POST['id_log'],
                    'nota'       => $_POST['nota'],
                );

                $guardar = ModeloProgramacion::guardarConsecutivoCertModel($datos);

                if ($guardar == true) {
                    echo '
					<script>
					ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
					setTimeout(recargarPagina,1050);

					function recargarPagina(){
						window.open("' . BASE_URL . 'imprimir/imprimirCertificado?reserva=' .
                    base64_encode($_POST['reserva']) . '&fecha=' . base64_encode($fechareg)
                        . '", "_blank");

						window.location.replace("index");
					}
					</script>
						';
                } else {
                    echo '
					<script>
					ohSnap("Ha ocurrido un error", {color: "red"});
					</script>
					';
                }
            }
        } else {
        }
    }

    public function solicitarCertificadoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_empresa']) &&
            !empty($_POST['id_empresa']) &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user']) &&
            isset($_POST['id_reserva']) &&
            !empty($_POST['id_reserva'])
        ) {

            $datos = array(
                'id_empresa'  => $_POST['id_empresa'],
                'id_sucursal' => $_POST['id_sucursal'],
                'id_user'     => $_POST['id_user'],
                'id_reserva'  => $_POST['id_reserva'],
            );

            $guardar = ModeloProgramacion::solicitarCertficadoModel($datos);

            if ($guardar == true) {
                echo '
				<script>
				ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
				setTimeout(recargarPagina,1050);

				function recargarPagina(){
					window.location.replace("inicio");
				}
				</script>
				';
            } else {
                echo '
				<script>
				ohSnap("Ha ocurrido un error", {color: "red"});
				</script>
				';
            }
        }
    }

    public function confirmarCertificadoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_solicitud']) &&
            !empty($_POST['id_solicitud']) &&
            isset($_POST['id_confirma']) &&
            !empty($_POST['id_confirma'])
        ) {

            $datos = array(
                'id_solicitud' => $_POST['id_solicitud'],
                'id_confirma'  => $_POST['id_confirma'],
            );

            $guardar = ModeloProgramacion::confirmarCertificadoModel($datos);

            if ($guardar == true) {
                echo '
				<script>
				ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
				setTimeout(recargarPagina,1050);

				function recargarPagina(){
					window.location.replace("index");
				}
				</script>
				';
            } else {
                echo '
				<script>
				ohSnap("Ha ocurrido un error", {color: "red"});
				</script>
				';
            }
        }
    }
}
