<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'recepcion' . DS . 'ModeloRecepcion.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreos.php';
require_once MODELO_PATH . 'empresas' . DS . 'ModeloEmpresa.php';
require_once MODELO_PATH . 'empresas' . DS . 'ModeloSede.php';
require_once MODELO_PATH . 'usuario' . DS . 'ModeloUsuario.php';
require_once MODELO_PATH . 'vehiculo' . DS . 'ModeloVehiculo.php';
require_once MODELO_PATH . 'residuos' . DS . 'ModeloResiduos.php';

class ControlRecepcion
{

  private static $instancia;

  public static function singleton_recepcion()
  {
    if (!isset(self::$instancia)) {
      $miclase = __CLASS__;
      self::$instancia = new $miclase;
    }
    return self::$instancia;
  }


  public function mostrarDatosRecepcionControl($id, $super_empresa)
  {
    $mostrar = ModeloRecepcion::mostrarDatosRecepcionModel($id, $super_empresa);
    return $mostrar;
  }


  public function guardarRecepcionControl()
  {
    if (
      $_SERVER['REQUEST_METHOD'] == 'POST' &&
      isset($_POST['id_log']) &&
      !empty($_POST['id_log']) &&
      isset($_POST['super_empresa']) &&
      !empty($_POST['super_empresa']) &&
      isset($_POST['vehiculo']) &&
      !empty($_POST['vehiculo']) &&
      isset($_POST['doc_conductor']) &&
      !empty($_POST['doc_conductor']) &&
      isset($_POST['nom_conductor']) &&
      !empty($_POST['nom_conductor']) &&
      isset($_POST['empresa']) &&
      !empty($_POST['empresa'])
    ) {

      $fechareg = date('Y-m-d H:i:s');

      $hora = date('H:i:s');


      if ($_POST['sucursal'] != '') {
        $sucursal = $_POST['sucursal'];
      } else {
        $sucursal = 0;
      }


      $datos = array(
        'id_vehiculo' => 0,
        'id_user' => $_POST['id_log'],
        'id_conductor' => 0,
        'id_empresa' => $_POST['empresa'],
        'id_sucursal' => $sucursal,
        'hora_inicio' => $hora,
        'hora_fin' => $hora,
        'fecha_apartado' => $fechareg,
        'observacion' => $_POST['observacion'],
        'fechareg' => $fechareg,
        'id_coopiloto' => 0
      );

      $guardar = ModeloRecepcion::guardarRecepcionModel($datos);

      if ($guardar['guardar'] == TRUE) {

        $id_reserva = $guardar['id'];

        $array_id = array();
        $array_id = $_POST['id_residuo'];

        $array_cantidad = array();
        $array_cantidad = $_POST['cantidad'];

        $it = new MultipleIterator();
        $it->attachIterator(new ArrayIterator($array_id));
        $it->attachIterator(new ArrayIterator($array_cantidad));

        foreach ($it as $a) {

          $mostrar_tipo_residuo = ModeloResiduos::mostrarResiduosIdModel($a[0]);
          $id_tipo_residuo = $mostrar_tipo_residuo['id_tipo_residuo'];

          $datos_apartado_residuo = array(
            'id_apartado' => $id_reserva,
            'id_residuo' => $a[0],
            'id_tipo_residuo' => $id_tipo_residuo,
            'cantidad' => $a[1],
            'fechareg' => $fechareg
          );

          $guardar_residuos = ModeloRecepcion::guardarResiduosRecepcionModel($datos_apartado_residuo);
        }

        if ($guardar_residuos == TRUE) {

          $datos_recepcion = array(
            'id_apartado' => $id_reserva,
            'vehiculo' => $_POST['vehiculo'],
            'user_log' => $_POST['id_log'],
            'super_empresa' => $_POST['super_empresa'],
            'documento' => $_POST['doc_conductor'],
            'conductor' => $_POST['nom_conductor'],
            'coopiloto' => $_POST['nom_coopiloto'],
            'fechareg' => $fechareg
          );


          $guardar_recepcion = ModeloRecepcion::guardarRecepcionDatosModel($datos_recepcion);

          if ($guardar_recepcion == TRUE) {
            echo '
        <script>
        ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
        setTimeout(recargarPagina,1050);

        function recargarPagina(){
          window.location.replace("index");
        }
        </script>
        ';
          } else {
            echo '
       <script>
       ohSnap("Datos recepcion no guardados!", {color: "red", "duration": "1000"});
       </script>
       ';
          }
        } else {
          echo '
     <script>
     ohSnap("Residuos no guardados!", {color: "red", "duration": "1000"});
     </script>
     ';
        }
      } else {
        echo '
  <script>
  ohSnap("Reserva no guardada!", {color: "red", "duration": "1000"});
  </script>
  ';
      }
    } else {
      echo '
 <script>
 ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
 </script>
 ';
    }
  }
}
