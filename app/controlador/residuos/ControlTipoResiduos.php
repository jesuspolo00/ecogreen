<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'residuos' . DS . 'ModeloTipoResiduos.php';

class ControlTipoResiduo
{

    private static $instancia;

    public static function singleton_tipo_residuo()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarTipoResiduoControl($super_empresa)
    {
        $mostrar = ModeloTipoResiduo::mostrarTipoResiduoModel($super_empresa);
        return $mostrar;
    }

    public function mostrarTipoResiduoIdControl($id)
    {
        $mostrar = ModeloTipoResiduo::mostrarTipoResiduoIdModel($id);
        return $mostrar;
    }

    public function guardarTipoResiduoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['super_empresa']) &&
            !empty($_POST['super_empresa']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['descripcion']) &&
            !empty($_POST['descripcion'])
        ) {

            $fechareg = date('Y-m-d H:i:s');

            $datos = array(
                'nombre'        => $_POST['descripcion'],
                'fechareg'      => $fechareg,
                'user_log'      => $_POST['id_log'],
                'super_empresa' => $_POST['super_empresa'],
            );

            $guardar = ModeloTipoResiduo::guardarTipoResiduoModel($datos);

            if ($guardar = true) {
                echo '
			<script>
			ohSnap("Registrado correctamente!", {color: "green", "duration": "1000"});
			setTimeout(recargarPagina,1050);

			function recargarPagina(){
				window.location.replace("index");
			}
			</script>
			';
            } else {
                echo '
			<script>
			ohSnap("Ha ocurrido un error!", {color: "red"});
			</script>
			';
            }
        } else {
        }
    }

    public function editarTipoResiduoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_tipo']) &&
            !empty($_POST['id_tipo']) &&
            isset($_POST['nom_edit']) &&
            !empty($_POST['nom_edit'])
        ) {

            $fechareg = date('Y-m-d H:i:s');

            $datos = array(
                'nombre'  => $_POST['nom_edit'],
                'id_tipo' => $_POST['id_tipo'],
            );

            $guardar = ModeloTipoResiduo::editarTipoResiduoModel($datos);

            if ($guardar = true) {
                echo '
		<script>
		ohSnap("Editado correctamente!", {color: "green", "duration": "1000"});
		setTimeout(recargarPagina,1050);

		function recargarPagina(){
			window.location.replace("index");
		}
		</script>
		';
            } else {
                echo '
		<script>
		ohSnap("Ha ocurrido un error!", {color: "red"});
		</script>
		';
            }
        } else {
        }
    }

    public function inactivarTipoResiduoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_tipo']) &&
            !empty($_POST['id_tipo'])
        ) {

            $id_tipo = filter_input(INPUT_POST, 'id_tipo', FILTER_SANITIZE_NUMBER_INT);
            $result  = ModeloTipoResiduo::inactivarTipoResiduoModelo($id_tipo);

            if ($result == true) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }

    public function activarTipoResiduoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_tipo']) &&
            !empty($_POST['id_tipo'])
        ) {

            $id_tipo = filter_input(INPUT_POST, 'id_tipo', FILTER_SANITIZE_NUMBER_INT);
            $result  = ModeloTipoResiduo::activarTipoResiduoModelo($id_tipo);

            if ($result == true) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }

    public function eliminarTipoResiduoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_tipo']) &&
            !empty($_POST['id_tipo'])
        ) {

            $id_tipo = filter_input(INPUT_POST, 'id_tipo', FILTER_SANITIZE_NUMBER_INT);
            $result  = ModeloTipoResiduo::eliminarTipoResiduoModelo($id_tipo);

            if ($result == true) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }
}
