<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'empresas' . DS . 'ModeloSede.php';

class ControlSede
{

	private static $instancia;

	public static function singleton_sede()
	{
		if (!isset(self::$instancia)) {
			$miclase = __CLASS__;
			self::$instancia = new $miclase;
		}
		return self::$instancia;
	}

	public function mostrarSedecontrol($super_empresa)
	{
		$mostrar = ModeloSede::mostrarSedesModel($super_empresa);
		return $mostrar;
	}

	public function mostrarSedeEmpresaIdControl($id)
	{
		$mostrar = ModeloSede::MostrarSedesEmpresa($id);
		return $mostrar;
	}

	public function mostrarSedeIdControl($id)
	{
		$mostrar = ModeloSede::mostrarSedeIdModel($id);
		return $mostrar;
	}

	public function mostrarempresaControl($id)
	{
		$mostrar = ModeloSede::mostrarSedeEmpresaIdModel($id);
		return $mostrar;
	}

	public function agregarSedeControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_log']) &&
			!empty($_POST['id_log']) &&
			isset($_POST['id_empresa']) &&
			!empty($_POST['id_empresa']) &&
			isset($_POST['nombre_sede']) &&
			!empty($_POST['nombre_sede']) &&
			isset($_POST['ciudad_sede']) &&
			!empty($_POST['ciudad_sede']) &&
			isset($_POST['direccion_sede']) &&
			!empty($_POST['direccion_sede']) &&
			isset($_POST['telefono_sede']) &&
			!empty($_POST['telefono_sede']) &&
			isset($_POST['nit']) &&
			!empty($_POST['nit']) &&
			isset($_POST['nom_contacto_sede']) &&
			!empty($_POST['nom_contacto_sede']) &&
			isset($_POST['tiempo_sede']) &&
			!empty($_POST['tiempo_sede']) &&
			isset($_POST['email_sede']) &&
			!empty($_POST['email_sede'])
		) {

			$fechareg = date('Y-m-d H:i:s');
			$fechaupdate = date('Y-m-d H:i:s');

			$datos = array(
				'nombre' => $_POST['nombre_sede'],
				'ciudad' => $_POST['ciudad_sede'],
				'direccion' => $_POST['direccion_sede'],
				'telefono' => $_POST['telefono_sede'],
				'nit' => $_POST['nit'],
				'nom_contacto' => $_POST['nom_contacto_sede'],
				'tiempo' => $_POST['tiempo_sede'],
				'email' => $_POST['email_sede'],
				'id_user' => $_POST['id_log'],
				'id_empresa' => $_POST['id_empresa'],
				'fechareg' => $fechareg,
				'fechaupdate' => $fechaupdate
			);

			$guardar = ModeloSede::agregarSedeModel($datos);

			if ($guardar == TRUE) {
				echo '
			<script>
			ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
			setTimeout(recargarPagina,1050);

			function recargarPagina(){
				window.location.replace("index");
			}
			</script>
			';
			} else {
				echo '
			<script>
			ohSnap("Ha ocurrido un error", {color: "red"});
			</script>
			';
			}
		}
	}


	public function editarSedeControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_sede']) &&
			!empty($_POST['id_sede']) &&
			isset($_POST['id_log']) &&
			!empty($_POST['id_log']) &&
			isset($_POST['nom_edit']) &&
			!empty($_POST['nom_edit']) &&
			isset($_POST['ciudad_edit']) &&
			!empty($_POST['ciudad_edit']) &&
			isset($_POST['dir_edit']) &&
			!empty($_POST['dir_edit']) &&
			isset($_POST['tel_edit']) &&
			!empty($_POST['tel_edit']) &&
			isset($_POST['nom_cont_edit']) &&
			!empty($_POST['nom_cont_edit']) &&
			isset($_POST['nit_edit']) &&
			!empty($_POST['nit_edit']) &&
			isset($_POST['tiempo_edit']) &&
			!empty($_POST['tiempo_edit']) &&
			isset($_POST['email_edit']) &&
			!empty($_POST['email_edit'])
		) {

			$fechaupdate = date('Y-m-d H:i:s');

			$datos = array(
				'id_sede' => $_POST['id_sede'],
				'nombre' => $_POST['nom_edit'],
				'ciudad' => $_POST['ciudad_edit'],
				'direccion' => $_POST['dir_edit'],
				'telefono' => $_POST['tel_edit'],
				'nom_contacto' => $_POST['nom_cont_edit'],
				'nit' => $_POST['nit_edit'],
				'tiempo' => $_POST['tiempo_edit'],
				'email' => $_POST['email_edit'],
				'id_user' => $_POST['id_log'],
				'fechaupdate' => $fechaupdate
			);

			$editar = ModeloSede::editarSedeModelo($datos);

			if ($editar == TRUE) {
				echo '
		<script>
		ohSnap("Editado correctamente!", {color: "green", "duration": "1000"});
		setTimeout(recargarPagina,1050);

		function recargarPagina(){
			window.location.replace("index");
		}
		</script>
		';
			} else {
				echo '
		<script>
		ohSnap("Ha ocurrido un error", {color: "red"});
		</script>
		';
			}
		}
	}


	public function mostrarSucursalesEmpresaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_empresa']) &&
			!empty($_POST['id_empresa'])
		) {

			$id_empresa = filter_input(INPUT_POST, 'id_empresa', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloSede::mostrarSucursalesEmpresaModel($id_empresa);
			return $result;
		}
	}


	public function eliminarSedeControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_sucursal']) &&
			!empty($_POST['id_sucursal'])
		) {


			$id_sucursal = filter_input(INPUT_POST, 'id_sucursal', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloSede::EliminarSedeModel($id_sucursal);

			if ($result == TRUE) {
				$r = "ok";
			} else {
				$r = "No";
			}
			return $r;
		}
	}

	public function InactivarSedeControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_sucursal']) &&
			!empty($_POST['id_sucursal'])
		) {

			$id_sucursal = filter_input(INPUT_POST, 'id_sucursal', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloSede::InactivarSedeModel($id_sucursal);

			if ($result == TRUE) {
				$r = "ok";
			} else {
				$r = "No";
			}
			return $r;
		}
	}

	public function activarSedeControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_sucursal']) &&
			!empty($_POST['id_sucursal'])
		) {

			$id_sucursal = filter_input(INPUT_POST, 'id_sucursal', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloSede::activarSedeModel($id_sucursal);

			if ($result == TRUE) {
				$r = "ok";
			} else {
				$r = "No";
			}
			return $r;
		}
	}

	public function mostrarNitEmpresaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_empresa']) &&
			!empty($_POST['id_empresa'])
		) {

			$id_empresa = filter_input(INPUT_POST, 'id_empresa', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloSede::mostrarNitEmpresaModel($id_empresa);
			return $result;
		}
	}
}
