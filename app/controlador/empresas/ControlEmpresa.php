<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'empresas' . DS . 'ModeloEmpresa.php';
require_once CONTROL_PATH . 'hash.php';

class ControlEmpresa
{

	private static $instancia;

	public static function singleton_empresa()
	{
		if (!isset(self::$instancia)) {
			$miclase = __CLASS__;
			self::$instancia = new $miclase;
		}
		return self::$instancia;
	}


	public function mostrarEmpresasControl($super_emrpesa)
	{
		$mostrar = ModeloEmpresa::mostrarEmpresasModel($super_emrpesa);
		return $mostrar;
	}

	public function mostrarEmpresaIdControl($id)
	{
		$mostrar = ModeloEmpresa::mostrarEmpresaIdModel($id);
		return $mostrar;
	}

	public function guardarEmpresaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_log']) &&
			!empty($_POST['id_log']) &&
			isset($_POST['super_empresa']) &&
			!empty($_POST['super_empresa']) &&
			isset($_POST['nombre']) &&
			!empty($_POST['nombre']) &&
			isset($_POST['ciudad']) &&
			!empty($_POST['ciudad']) &&
			isset($_POST['direccion']) &&
			!empty($_POST['direccion']) &&
			isset($_POST['telefono']) &&
			!empty($_POST['telefono']) &&
			isset($_POST['nom_contacto']) &&
			!empty($_POST['nom_contacto']) &&
			isset($_POST['nit']) &&
			!empty($_POST['nit']) &&
			isset($_POST['tiempo']) &&
			!empty($_POST['tiempo']) &&
			isset($_POST['email']) &&
			!empty($_POST['email'])
		) {

			$fechareg = date('Y-m-d H:i:s');
			$fechaupdate = date('Y-m-d H:i:s');
			$super_emrpesa = $_POST['super_empresa'];

			$datos = array(
				'nombre' => $_POST['nombre'],
				'ciudad' => $_POST['ciudad'],
				'direccion' => $_POST['direccion'],
				'telefono' => $_POST['telefono'],
				'nom_contacto' => $_POST['nom_contacto'],
				'nit' => $_POST['nit'],
				'tiempo' => $_POST['tiempo'],
				'email' => $_POST['email'],
				'id_user' => $_POST['id_log'],
				'super_empresa' => $super_emrpesa,
				'fechareg' => $fechareg,
				'fechaupdate' => $fechaupdate
			);

			$guardar = ModeloEmpresa::guardarEmpresaModel($datos);
			if ($guardar == TRUE) {
				echo '
				<script>
				ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
				setTimeout(recargarPagina,1050);

				function recargarPagina(){
					window.location.replace("index");
				}
				</script>
				';
			} else {
				echo '
				<script>
				ohSnap("Ha ocurrido un error", {color: "red"});
				</script>
				';
			}
		}
	}

	public function editarEmpresaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_empresa']) &&
			!empty($_POST['id_empresa']) &&
			isset($_POST['id_log']) &&
			!empty($_POST['id_log']) &&
			isset($_POST['nom_edit']) &&
			!empty($_POST['nom_edit']) &&
			isset($_POST['ciudad_edit']) &&
			!empty($_POST['ciudad_edit']) &&
			isset($_POST['dir_edit']) &&
			!empty($_POST['dir_edit']) &&
			isset($_POST['tel_edit']) &&
			!empty($_POST['tel_edit']) &&
			isset($_POST['nom_cont_edit']) &&
			!empty($_POST['nom_cont_edit']) &&
			isset($_POST['nit_edit']) &&
			!empty($_POST['nit_edit']) &&
			isset($_POST['tiempo_edit']) &&
			!empty($_POST['tiempo_edit']) &&
			isset($_POST['email_edit']) &&
			!empty($_POST['email_edit'])
		) {

			$fechaupdate = date('Y-m-d H:i:s');

			$datos = array(
				'id_empresa' => $_POST['id_empresa'],
				'nombre' => $_POST['nom_edit'],
				'ciudad' => $_POST['ciudad_edit'],
				'direccion' => $_POST['dir_edit'],
				'telefono' => $_POST['tel_edit'],
				'nom_contacto' => $_POST['nom_cont_edit'],
				'nit' => $_POST['nit_edit'],
				'tiempo' => $_POST['tiempo_edit'],
				'email' => $_POST['email_edit'],
				'id_user' => $_POST['id_log'],
				'fechaupdate' => $fechaupdate
			);

			$guardar = ModeloEmpresa::editarEmnpresaModelo($datos);
			if ($guardar == TRUE) {
				echo '
				<script>
				ohSnap("Editado correctamente!", {color: "green", "duration": "1000"});
				setTimeout(recargarPagina,1050);

				function recargarPagina(){
					window.location.replace("index");
				}
				</script>
				';
			} else {
				echo '
				<script>
				ohSnap("Ha ocurrido un error", {color: "red"});
				</script>
				';
			}
		}
	}

	public function inactivarEmpresaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_empresa']) &&
			!empty($_POST['id_empresa'])
		) {

			$id_empresa = filter_input(INPUT_POST, 'id_empresa', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloEmpresa::inactivarEmpresaModelo($id_empresa);

			if ($result == TRUE) {
				$r = "ok";
			} else {
				$r = "No";
			}
			return $r;
		}
	}


	public function activarEmpresaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_empresa']) &&
			!empty($_POST['id_empresa'])
		) {

			$id_empresa = filter_input(INPUT_POST, 'id_empresa', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloEmpresa::activarEmpresaModelo($id_empresa);

			if ($result == TRUE) {
				$r = "ok";
			} else {
				$r = "No";
			}
			return $r;
		}
	}



	public function eliminarEmpresaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_empresa']) &&
			!empty($_POST['id_empresa'])
		) {


			$id_empresa = filter_input(INPUT_POST, 'id_empresa', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloEmpresa::eliminarEmpresaModelo($id_empresa);

			if ($result == TRUE) {
				$r = "ok";
			} else {
				$r = "No";
			}
			return $r;
		}
	}



	public function mostrarCiudadEmpresaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_empresa']) &&
			!empty($_POST['id_empresa'])
		) {

			$id_empresa = filter_input(INPUT_POST, 'id_empresa', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloEmpresa::mostrarCiudadEmpresaModel($id_empresa);
			return $result;
		}
	}
}
