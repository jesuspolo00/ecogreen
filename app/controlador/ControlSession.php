<?php
require_once MODELO_PATH . 'IngresoModel.php';
require_once CONTROL_PATH . 'Session.php';
require_once CONTROL_PATH . 'hash.php';
require_once LIB_PATH . 'PHPMailer/PHPMailerAutoload.php';
require_once MODELO_PATH . 'configMail.php';
@session_start();

class ingresoClass
{

    private static $instancia;
    private $objsession;

    public static function singleton_ingreso()
    {

        if (!isset(self::$instancia)) {

            $miclase = __CLASS__;

            self::$instancia = new $miclase;
        }

        return self::$instancia;
    }


    public function ingresaruser()
    {
        if (
            isset($_POST['user']) &&
            !empty($_POST['user']) &&
            isset($_POST['pass']) &&
            !empty($_POST['pass'])
        ) {

            $user = filter_var($_POST['user']);
            $pass = filter_var($_POST['pass']);
            $rslt = IngresoModel::verificarUser($user);

            if ($rslt) {
                if ($rslt['estado'] == 'activo') {
                    if ($rslt['user'] === $user) {
                        $hash = $rslt['pass'];
                        if (Hash::verificar($hash, $pass)) {
                            $this->objsession = new Session;
                            $this->objsession->iniciar();
                            $this->objsession->SetSession('id', $rslt['id_user']);
                            $this->objsession->SetSession('nombre_admin', $rslt['nombre']);
                            $this->objsession->SetSession('apellido', $rslt['apellido']);
                            $this->objsession->SetSession('rol', $rslt['perfil']);
                            $this->objsession->SetSession('empresa', $rslt['id_empresa']);
                            $this->objsession->SetSession('super_empresa', $rslt['id_super_empresa']);
                            $location = ($rslt['id_empresa'] != 0) ? 'inicio' : 'inicio';
                            header('Location:'.$location);
                        } else {
                            $er = '1';
                            $error = base64_encode($er);
                            header('Location:login?er=' . $error);
                        }
                    } else {
                        $er = '1';
                        $error = base64_encode($er);
                        header('Location:login?er=' . $error);
                    }
                } else {
                    $er = '4';
                    $error = base64_encode($er);
                    header('Location:login?er=' . $error);
                }
            } else {
                $er = '3';
                $error = base64_encode($er);
                header('Location:login?er=' . $error);
            }
        } else {
        }
    }



    public function restablecerPassword()
    {
        if (
            isset($_POST['mail']) &&
            !empty($_POST['mail'])
        ) {

            $mail = filter_var($_POST['mail']);

            $verificar_mail = IngresoModel::verificarCorreo($mail);

            if ($verificar_mail) {
                /*echo $verificar_mail['correo'];
        echo $verificar_mail['user'];
        echo $verificar_mail['documento'];*/

                $mail = new PHPMailer();

                $mail->IsSMTP();
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = SMTP;
                $mail->Host = SERVER;
                $mail->Port = PORT;

                $mail->Username = USER;
                $mail->Password = PASS;


                $mail->From     = USER;
                $mail->FromName = NOMBRE;

                $mail->Subject  = 'Restablecer contrase&ntilde;a';
                $mail->AddAddress($verificar_mail['correo'], $verificar_mail['user']);

                $body = '
        <div 
        style="
        width:90%;
        background-color: #1cc88a;
        background-image: -webkit-gradient(linear, left top, left bottom, color-stop(10%, #1cc88a), to(#13855c));
        background-image: linear-gradient(180deg, #1cc88a 10%, #13855c 100%);
        background-size: cover; margin: auto auto; 
        height: auto;
        padding: 10px;
        ">
        <div 
        style="
        margin-top:5%;
        background-color: transparent; 
        padding: 5px; 
        height: auto;
        width: 100%; 
        margin: auto; 
        text-align: center;
        ">
        <img src="http://www.ecogreenrecycling.com/wp-content/uploads/EcoGreen_presentacion_w-05.png" style="width: 60%; height: auto;">
        </div>
        <div style="
        margin-top:10%;
        height: auto;
        margin-bottom: 10%;
        ">

        <p 
        style="
        text-align: center;
        color: #fff;
        font-size: 2em;
        ">
        NUESTRA EMPRESA
        </p>
        <p style="
        text-align: center;
        color: #fff;
        font-size: 1.3em;
        ">
        Nuestra planta cuenta con los equipos y la infraestructura necesaria, para aceptar y procesar desechos portuarios, RESPELS y aguas residuales de diversas industrias, cumpliendo con los m&aacute;s altos est&aacute;ndares de seguridad ambiental utilizando la &uacute;ltima tecnolog&iacute;a.
        </p>
        </div>
        <div
        style="
        width: 100%;
        margin-top:10%;
        height: auto;
        text-align: center;
        margin-bottom: 5%;
        "
        >
        <a href="#" style="background-color: #1cc88a; border-radius: 3em; padding: 15px; font-size:1.2em; text-decoration: none; color: #fff;">Ir al enlace</a>
        </div>
        </div>
        ';


                $mail->MsgHTML($body);


                if (!$mail->send()) {
                    echo '<script>
            ohSnap("Error al enviar mail", {color: "red"});
            </script>';
                } else {
                    echo '<script>
            ohSnap("Enviado correctamente", {color: "green"});
            </script>';
                }
            } else {
                echo '<script>
        ohSnap("Correo no registrado", {color: "red", "duration":"4000"});
        $("#mail").css("border", "1px solid  #cb4335");
        </script>';
            }
        } else {
        }
    }
}
