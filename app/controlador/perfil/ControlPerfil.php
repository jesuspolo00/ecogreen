<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'perfil' . DS . 'ModeloPerfil.php';
require_once CONTROL_PATH . 'hash.php';

class ControlPerfil
{

	private static $instancia;

	public static function singleton_perfil()
	{
		if (!isset(self::$instancia)) {
			$miclase = __CLASS__;
			self::$instancia = new $miclase;
		}
		return self::$instancia;
	}

	public function mostrarDatosPerfilControl($id, $super_empresa)
	{
		$datos = ModeloPerfil::mostrarDatosPerfilModel($id, $super_empresa);
		return $datos;
	}


	public function mostrarPerfilesControl($super_empresa)
	{
		$datos = ModeloPerfil::mostrarPerfilesModel($super_empresa);
		return $datos;
	}


	public function guardarPerfilControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['super_empresa']) &&
			!empty($_POST['super_empresa']) &&
			isset($_POST['id_log']) &&
			!empty($_POST['id_log']) &&
			isset($_POST['descripcion']) &&
			!empty($_POST['descripcion'])
		) {

			$fechareg = date('Y-m-d H:i:s');

			$datos = array(
				'nombre' => $_POST['descripcion'],
				'fechareg' => $fechareg,
				'user_log' => $_POST['id_log'],
				'super_empresa' => $_POST['super_empresa']
			);

			$guardar = ModeloPerfil::guardarPerfilModelo($datos);
			if ($guardar == TRUE) {
				echo '
				<script>
				ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
				setTimeout(recargarPagina,1050);

				function recargarPagina(){
					window.location.replace("index");
				}
				</script>
				';
			} else {
				echo '
				<script>
				ohSnap("Ha ocurrido un error", {color: "red"});
				</script>
				';
			}
		}
	}


	public function editarPerfilesControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_perfil']) &&
			!empty($_POST['id_perfil']) &&
			isset($_POST['nom_edit']) &&
			!empty($_POST['nom_edit'])
		) {
			$datos = array(
				'id_perfil' => $_POST['id_perfil'],
				'nombre' => $_POST['nom_edit']
			);

			$guardar = ModeloPerfil::editarPerfilesModel($datos);
			if ($guardar['guardar'] == TRUE) {
				echo '
		<script>
		ohSnap("Editado correctamente!", {color: "green", "duration": "1000"});
		setTimeout(recargarPagina,1050);

		function recargarPagina(){
			window.location.replace("index");
		}
		</script>
		';
			} else {
				echo '
		<script>
		ohSnap("Ha ocurrido un error", {color: "red"});
		</script>
		';
			}
		}
	}


	public function editarPerfilControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_user']) &&
			!empty($_POST['id_user']) &&
			isset($_POST['pass_old']) &&
			!empty($_POST['pass_old']) &&
			isset($_POST['documento']) &&
			!empty($_POST['documento']) &&
			isset($_POST['nombre']) &&
			!empty($_POST['nombre']) &&
			isset($_POST['correo']) &&
			!empty($_POST['correo']) &&
			isset($_POST['telefono']) &&
			!empty($_POST['telefono']) &&
			isset($_POST['usuario']) &&
			!empty($_POST['usuario'])
		) {

			$pass = $_POST['password'];
			$conf_pass = $_POST['conf_password'];

			if ($pass == $conf_pass && $pass != "" && $conf_pass != "") {
				$clavecifrada = Hash::hashpass($pass);

				$datos = array(
					'id_user' => $_POST['id_user'],
					'documento' => $_POST['documento'],
					'nombre' => $_POST['nombre'],
					'apellido' => $_POST['apellido'],
					'correo' => $_POST['correo'],
					'telefono' => $_POST['telefono'],
					'usuario' => $_POST['usuario'],
					'pass' => $clavecifrada,
					'perfil' => $_POST['perfil'],
				);

				$guardar = ModeloPerfil::editarPerfilModel($datos);

				if ($guardar == TRUE) {
					echo '
						<script>
						ohSnap("Modificado correctamente!", {color: "green", "duration": "1000"});
						setTimeout(recargarPagina,1050);

						function recargarPagina(){
							window.location.replace("index");
						}
						</script>
						';
				} else {
					echo '
						<script>
						ohSnap("Ha ocurrido un error", {color: "red"});
						</script>
						';
				}
			} else {
				$datos = array(
					'id_user' => $_POST['id_user'],
					'documento' => $_POST['documento'],
					'nombre' => $_POST['nombre'],
					'apellido' => $_POST['apellido'],
					'correo' => $_POST['correo'],
					'telefono' => $_POST['telefono'],
					'usuario' => $_POST['usuario'],
					'pass' => $_POST['pass_old'],
					'perfil' => $_POST['perfil'],
				);

				$guardar = ModeloPerfil::editarPerfilModel($datos);

				if ($guardar == TRUE) {
					echo '
						<script>
						ohSnap("Modificado correctamente!", {color: "green", "duration": "1000"});
						setTimeout(recargarPagina,1050);

						function recargarPagina(){
							window.location.replace("index");
						}
						</script>
						';
				} else {
					echo '
						<script>
						ohSnap("Ha ocurrido un error!", {color: "red"});
						</script>
						';
				}
			}
		} else {
		}
	}




	public function eliminarPerfilControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_perfil']) &&
			!empty($_POST['id_perfil'])
		) {

			$id_perfil = filter_input(INPUT_POST, 'id_perfil', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloPerfil::eliminarPerfilModelo($id_perfil);

			if ($result == TRUE) {
				$r = "ok";
			} else {
				$r = "No";
			}
			return $r;
		}
	}
}
