<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'conciliacion' . DS . 'ModeloConciliacion.php';
require_once MODELO_PATH . 'apartar' . DS . 'ModeloApartar.php';

class ControlConciliacion
{

    private static $instancia;

    public static function singleton_conciliacion()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function consiliarReservaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_reserva']) &&
            !empty($_POST['id_reserva']) &&
            isset($_POST['id_residuo']) &&
            !empty($_POST['id_residuo']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['id_tipo_residuo']) &&
            !empty($_POST['id_tipo_residuo'])
        ) {

            $array_residuos = array();
            $array_residuos = $_POST['id_residuo'];

            $array_kilos = array();
            $array_kilos = $_POST['kilos'];

            $array_galones = array();
            $array_galones = $_POST['galones'];

            $array_tipo_residuo = array();
            $array_tipo_residuo = $_POST['id_tipo_residuo'];

            $array_tratamiento = array();
            $array_tratamiento = $_POST['tratamiento'];

            $array_dispocision = array();
            $array_dispocision = $_POST['dispocision'];

            $array_destino = array();
            $array_destino = $_POST['destino'];

            $array_destino_final = array();
            $array_destino_final = $_POST['destino_final'];

            $it = new MultipleIterator();
            $it->attachIterator(new ArrayIterator($array_residuos));
            $it->attachIterator(new ArrayIterator($array_kilos));
            $it->attachIterator(new ArrayIterator($array_galones));
            $it->attachIterator(new ArrayIterator($array_tipo_residuo));
            $it->attachIterator(new ArrayIterator($array_tratamiento));
            $it->attachIterator(new ArrayIterator($array_dispocision));
            $it->attachIterator(new ArrayIterator($array_destino));
            $it->attachIterator(new ArrayIterator($array_destino_final));

            $eliminar_residuos = ModeloApartar::eliminarResiduosApartadoModel($_POST['id_reserva']);
            if ($eliminar_residuos == TRUE) {

                foreach ($it as $datos) {

                    $galones = ($datos[2] == '') ? 0 : $datos[2];

                    $datos_residuos = array(
                        'id_apartado' => $_POST['id_reserva'],
                        'id_residuo' => $datos[0],
                        'id_tipo_residuo' => $datos[3],
                        'cantidad' => $datos[1],
                        'id_td1' => $datos[4],
                        'id_td2' => $datos[5],
                        'id_td3' => $datos[6],
                        'id_td4' => $datos[7],
                        'kilogramos' => $datos[1],
                        'galones' => $galones,
                        'id_log' => $_POST['id_log']
                    );

                    $conciliar = ModeloConciliacion::consiliarReservaModel($datos_residuos);
                    $conciliar_log = ModeloConciliacion::consiliarReservaLogModel($datos_residuos);
                }

                if ($conciliar == TRUE) {
                    $conciliar_si = ModeloConciliacion::consiliarReservaSiModel($_POST['id_reserva']);
                    echo '
                    <script>
                        ohSnap("Conciliacion Correcta", {color: "green"});
                        setTimeout(recargarPagina,1050);

                        function recargarPagina(){
                            window.location.replace("index");
                        }
                    </script>
                    ';
                } else {
                    echo '
                    <script>
                        ohSnap("Error", {color: "red"});
                    </script>
                    ';
                }
            }
        }
    }
}
