<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'vehiculo' . DS . 'ModeloTipoVehiculo.php';

class ControlTipoVehiculo
{

	private static $instancia;

	public static function singleton_tipo_vehiculo()
	{
		if (!isset(self::$instancia)) {
			$miclase         = __CLASS__;
			self::$instancia = new $miclase;
		}
		return self::$instancia;
	}

	public function mostrarTipoVehiculoControl($super_empresa)
	{
		$datos = ModeloTipoVehiculo::mostrarTipoVehiculoModelo($super_empresa);
		return $datos;
	}

	public function mostrarTipoVehiculoIdControl($id, $super_empresa)
	{
		$datos = ModeloTipoVehiculo::mostrarTipoVehiculoIdModelo($id, $super_empresa);
		return $datos;
	}

	public function registrarTipoVehiculoControl()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['nombre']) &&
			!empty($_POST['nombre'])
		) {
			$datos   = array('nombre' => $_POST['nombre']);
		$guardar = ModeloTipoVehiculo::registrarTipoVehiculoModelo($datos);

		if ($guardar == true) {
			echo '
			<script>
			ohSnap("Registrado correctamente!", {color: "green", "duration": "1000"});
			setTimeout(recargarPagina,1050);

			function recargarPagina(){
				window.location.replace("index");
			}
			</script>
			';
		} else {
			echo '
			<script>
			ohSnap("Ha ocurrido un error!", {color: "red"});
			</script>
			';
		}
	} else {

	}
}

public function editarTipoVehiculoControl()
{
	if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
		isset($_POST['id_tipo']) &&
		!empty($_POST['id_tipo']) &&
		isset($_POST['nombre_edit']) &&
		!empty($_POST['nombre_edit'])
	) {

		$datos = array('id_tipo' => $_POST['id_tipo'],
			'nombre'                 => $_POST['nombre_edit']);

	$editar = ModeloTipoVehiculo::editarTipoVehiculoModelo($datos);

	if ($editar == true) {
		echo '
		<script>
		ohSnap("Editado correctamente!", {color: "green"});
		setTimeout(recargarPagina,2000);

		function recargarPagina(){
			window.location.replace("index");
		}
		</script>
		';
	} else {
		echo '
		<script>
		ohSnap("Ha ocurrido un error!", {color: "red"});
		</script>
		';
	}
} else {
}
}

public function inactivarTipoVehiculoControl()
{
	if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
		isset($_POST['id_tipo']) &&
		!empty($_POST['id_tipo'])
	) {

		$id_tipo = filter_input(INPUT_POST, 'id_tipo', FILTER_SANITIZE_NUMBER_INT);
	$result  = ModeloTipoVehiculo::inactivarTipoVehiculoModelo($id_tipo);

	if ($result == true) {
		$r = "ok";
	} else {
		$r = "No";
	}
	return $r;
}
}

public function activarTipoVehiculoControl()
{
	if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
		isset($_POST['id_tipo']) &&
		!empty($_POST['id_tipo'])
	) {

		$id_tipo = filter_input(INPUT_POST, 'id_tipo', FILTER_SANITIZE_NUMBER_INT);
	$result  = ModeloTipoVehiculo::activarTipoVehiculoModelo($id_tipo);

	if ($result == true) {
		$r = "ok";
	} else {
		$r = "No";
	}
	return $r;
}
}

public function eliminarTipoVehiculoControl()
{
	if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
		isset($_POST['id_tipo']) &&
		!empty($_POST['id_tipo'])
	) {

		$id_tipo = filter_input(INPUT_POST, 'id_tipo', FILTER_SANITIZE_NUMBER_INT);
	$result  = ModeloTipoVehiculo::eliminarTipoVehiculoModelo($id_tipo);

	if ($result == true) {
		$r = "ok";
	} else {
		$r = "No";
	}
	return $r;
}
}

}
