<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'vehiculo' . DS . 'ModeloVehiculo.php';
require_once MODELO_PATH . 'hoja_vida' . DS . 'ModeloHojaVida.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreos.php';
require_once MODELO_PATH . 'usuario' . DS . 'ModeloUsuario.php';

class ControlVehiculo
{

    private static $instancia;

    public static function singleton_vehiculo()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarDatosVehiculosControl($super_empresa)
    {
        $datos = ModeloVehiculo::mostrarDatosVehiculosModel($super_empresa);
        return $datos;
    }

    public function mostrarDatosVehiculosIdControl($id)
    {
        $datos = ModeloVehiculo::mostrarDatosVehiculosIdModel($id);
        return $datos;
    }

    public function registrarVehiculoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['descripcion']) &&
            !empty($_POST['descripcion']) &&
            isset($_POST['placa']) &&
            !empty($_POST['placa']) &&
            isset($_POST['marca']) &&
            !empty($_POST['marca']) &&
            isset($_POST['modelo']) &&
            !empty($_POST['modelo']) &&
            isset($_POST['id_tipo']) &&
            !empty($_POST['id_tipo']) &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {

            $fechareg = date('Y-m-d H:i:s');

            $datos = array(
                'descripcion'      => $_POST['descripcion'],
                'placa'            => $_POST['placa'],
                'marca'            => $_POST['marca'],
                'modelo'           => $_POST['modelo'],
                'id_tipo'          => $_POST['id_tipo'],
                'id_user'          => $_POST['id_user'],
                'fechareg'         => $fechareg,
                'id_super_empresa' => $_POST['super_empresa'],
                'id_log'           => $_POST['id_log'],
            );

            $guardar = ModeloVehiculo::registrarVehiculoModel($datos);

            if ($guardar['guardar'] == true) {

                $datos_hoja = array(
                    'id_vehiculo'       => $guardar['id'],
                    'mantenimiento'     => 0,
                    'fecha_adquisicion' => $fechareg,
                    'fecha_vence'       => '0000-00-00',
                    'fechareg'          => $fechareg,
                    'fecha_update'      => $fechareg,
                    'id_log'            => $_POST['id_log'],
                );

                $guardar_hoja = ModeloHojaVida::registrarHojaVidaModel($datos_hoja);

                if ($guardar_hoja == true) {
                    echo '
				<script>
				ohSnap("Registrado correctamente!", {color: "green", "duration": "1000"});
				setTimeout(recargarPagina,1050);

				function recargarPagina(){
					window.location.replace("index");
				}
				</script>
				';
                } else {
                    echo '
				<script>
				ohSnap("Ha ocurrido un error!", {color: "red"});
				</script>
				';
                }
            } else {
                echo '
			<script>
			ohSnap("Ha ocurrido un error!", {color: "red"});
			</script>
			';
            }
        } else {
        }
    }

    public function reportarVehiculoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['super_empresa']) &&
            !empty($_POST['super_empresa']) &&
            isset($_POST['id_vehiculo']) &&
            !empty($_POST['id_vehiculo']) &&
            isset($_POST['tipo_mant']) &&
            !empty($_POST['tipo_mant'])
        ) {

            $observacion     = $_POST['observacion'];
            $fecha_reportado = date('Y-m-d H:i:s');
            $super_empresa   = $_POST['super_empresa'];

            $datos = array(
                'id_log'          => $_POST['id_log'],
                'id_inventario'   => $_POST['id_vehiculo'],
                'tipo_mant'       => $_POST['tipo_mant'],
                'super_empresa'   => $super_empresa,
                'observacion'     => $observacion,
                'fecha_reportado' => $fecha_reportado,
            );

            $reportar = ModeloVehiculo::reportarVehiculoModelo($datos);

            if ($reportar == true) {

                $datos_actualizar = array(
                    'id_inventario' => $_POST['id_vehiculo'],
                    'tipo_mant'     => $_POST['tipo_mant'],
                );

                $actualizar_estado = ModeloVehiculo::actualizarEstadoVehiculoModelo($datos_actualizar);

                $datos_vehiculo  = ModeloVehiculo::mostrarDatosVehiculosIdModel($_POST['id_vehiculo'], $super_empresa);
                $nombre_vehiculo = $datos_vehiculo['descripcion'];
                $placa           = $datos_vehiculo['placa'];
                $modelo          = $datos_vehiculo['modelo'];
                $marca           = $datos_vehiculo['marca'];

                $datos_usuario  = ModeloUsuario::mostrarDatosUsuariosIdModel($_POST['id_log']);
                $nombre_usuario = $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'];

                if ($observacion != "") {
                    $html = '<h3 style="font-weight:bold;">Observacion:</h3>
							<p style="font-size:15px;">' . $observacion . '</p>
							';
                } else {
                    $html = '';
                }

                $mensaje = '
					<p style="font-size:15px;">
					El usuario <span style="font-weight:bold; text-transform:uppercase;">' . $nombre_usuario . '
					</span> ha reportado el vehiculo <span style="font-weight:bold; text-transform:uppercase;">' . $nombre_vehiculo . '
					</span> registrado con placa <span style="font-weight:bold; text-transform:uppercase;">' . $placa . '</span>
					</p>
					' . $html . '
					';

                $datos_correo = array(
                    'asunto'      => 'Reporte de vehiculo',
                    'correo'      => 'jesuspolo00@gmail.com',
                    //'correo' => 'mantenimiento@ecogreenrecycling.co',
                    'user'        => $nombre_usuario,
                    'mensaje'     => $mensaje,
                    'id_apartado' => 0,
                );

                /* $enviar_correo = Correo::enviarCorreoModel($datos_correo);

                if ($enviar_correo == TRUE) { */
                echo '
					<script>
					ohSnap("Reportado correctamente!", {color: "green"});
					setTimeout(recargarPagina,2000);

					function recargarPagina(){
						window.location.replace("index");
					}
					</script>
					';
                //}
            } else {
                echo '
				<script>
				ohSnap("Ha ocurrido un error!", {color: "red"});
				</script>
				';
            }
        } else {
        }
    }

    public function inactivarVehiculoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_vehiculo']) &&
            !empty($_POST['id_vehiculo'])
        ) {

            $fecha_inactivo = date('Y-m-d H:i:s');
            $id_vehiculo    = filter_input(INPUT_POST, 'id_vehiculo', FILTER_SANITIZE_NUMBER_INT);

            $datos = array(
                'id_vehiculo'    => $id_vehiculo,
                'fecha_inactivo' => $fecha_inactivo,
            );

            $result = ModeloVehiculo::inactivarVehiculoModelo($datos);

            if ($result == true) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }

    public function activarVehiculoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_vehiculo']) &&
            !empty($_POST['id_vehiculo'])
        ) {

            $fecha_activo = date('Y-m-d H:i:s');
            $id_vehiculo  = filter_input(INPUT_POST, 'id_vehiculo', FILTER_SANITIZE_NUMBER_INT);

            $datos = array(
                'id_vehiculo'  => $id_vehiculo,
                'fecha_activo' => $fecha_activo,
            );

            $result = ModeloVehiculo::activarVehiculoModelo($datos);

            if ($result == true) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }

    public static function eliminarVehiculoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_vehiculo']) &&
            !empty($_POST['id_vehiculo'])
        ) {

            $id_vehiculo = filter_input(INPUT_POST, 'id_vehiculo', FILTER_SANITIZE_NUMBER_INT);
            $result      = ModeloVehiculo::eliminarVehiculoModelo($id_vehiculo);

            if ($result == true) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }
}
