<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'permisos' . DS . 'ModeloPermisos.php';

class ControlPermiso
{

    private static $instancia;

    public static function singleton_permiso()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function consultarPermisoControl($id_user, $id_modulo, $id_opcion, $id_accion)
    {
        $datos = array(
            'id_user'   => $id_user,
            'id_modulo' => $id_modulo,
            'id_opcion' => $id_opcion,
            'id_accion' => $id_accion,
        );
        $permiso = ModeloPermiso::consultarPermisoModel($datos);
        return $permiso;
    }

    public function datosSuperEmpresaControl($super_empresa)
    {
        $datos = ModeloPermiso::datosSuperEmpresaModel($super_empresa);
        return $datos;
    }
}
