<?php
date_default_timezone_get('America/Bogota');
require_once MODELO_PATH . 'categorias' . DS . 'ModeloTipoCategorias.php';

class ControlTipoCategorias
{

	private static $instancia;

	public static function singleton_tipo_categorias()
	{
		if (!isset(self::$instancia)) {
			$miclase = __CLASS__;
			self::$instancia = new $miclase;
		}
		return self::$instancia;
	}


	public function mostrarTipoDestinoControl($super_empresa)
	{
		$mostrar = ModeloTipoCategorias::mostrarTipoDestinoModel($super_empresa);
		return $mostrar;
	}

	public function mostrarTipoTratamientoControl($super_empresa)
	{
		$mostrar = ModeloTipoCategorias::mostrarTipoTratamientoModel($super_empresa);
		return $mostrar;
	}

	public function mostrarListadoTratamientoControl($super_empresa)
	{
		$mostrar = ModeloTipoCategorias::mostrarListadoTratamientoModel($super_empresa);
		return $mostrar;
	}

	public function mostrarListadoDisposicionControl($super_empresa)
	{
		$mostrar = ModeloTipoCategorias::mostrarListadoDisposicionModel($super_empresa);
		return $mostrar;
	}

	public function mostrarListadoDestinoControl($super_empresa)
	{
		$mostrar = ModeloTipoCategorias::mostrarListadoDestinoModel($super_empresa);
		return $mostrar;
	}

	public function mostrarListadoDestinoFinalControl($super_empresa)
	{
		$mostrar = ModeloTipoCategorias::mostrarListadoDestinoFinalModel($super_empresa);
		return $mostrar;
	}

	public function mostrarTipoCategoriaIdControl($id)
	{
		$mostrar = ModeloTipoCategorias::mostrarTipoCategoriaIdModel($id);
		return $mostrar;
	}


	public function guardarTipoCategoriaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['super_empresa']) &&
			!empty($_POST['super_empresa']) &&
			isset($_POST['id_log']) &&
			!empty($_POST['id_log']) &&
			isset($_POST['descripcion']) &&
			!empty($_POST['descripcion'])
		) {

			$categoria = ($_POST['opcion'] == 1) ? 1 : 2;

			$datos = array(
				'nombre' => $_POST['descripcion'],
				'user_log' => $_POST['id_log'],
				'categoria' => $categoria,
				'super_empresa' => $_POST['super_empresa']
			);

			$guardar = ModeloTipoCategorias::guardarTipoCategoriaModel($datos);

			if ($guardar == TRUE) {
				echo '
					<script>
					ohSnap("Registrado correctamente!", {color: "green", "duration": "1000"});
					setTimeout(recargarPagina,1050);

					function recargarPagina(){
						window.location.replace("index");
					}
					</script>
					';
			} else {
				echo '
					<script>
					ohSnap("Ha ocurrido un error!", {color: "red"});
					</script>
					';
			}
		} else {
		}
	}



	public function editarTipoCategoriaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_tipo_edit']) &&
			!empty($_POST['id_tipo_edit']) &&
			isset($_POST['descripcion_edit']) &&
			!empty($_POST['descripcion_edit'])
		) {

			$datos = array(
				'id' => $_POST['id_tipo_edit'],
				'nombre' => $_POST['descripcion_edit']
			);

			$editar = ModeloTipoCategorias::editarTipoCategoriaModel($datos);

			if ($editar == TRUE) {
				echo '
				<script>
				ohSnap("Editado correctamente!", {color: "green", "duration": "1000"});
				setTimeout(recargarPagina,1050);

				function recargarPagina(){
					window.location.replace("index");
				}
				</script>
				';
			} else {
				echo '
					<script>
					ohSnap("Ha ocurrido un error!", {color: "red"});
					</script>
					';
			}
		}
	}





	public function inactivarTipoCategoriaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_tipo']) &&
			!empty($_POST['id_tipo'])
		) {


			$id_tipo = filter_input(INPUT_POST, 'id_tipo', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloTipoCategorias::inactivarTipoCategoriaModel($id_tipo);

			if ($result == TRUE) {
				$r = "ok";
			} else {
				$r = "No";
			}
			return $r;
		}
	}


	public function activarTipoCategoriaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_tipo']) &&
			!empty($_POST['id_tipo'])
		) {


			$id_tipo = filter_input(INPUT_POST, 'id_tipo', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloTipoCategorias::activarTipoCategoriaModel($id_tipo);

			if ($result == TRUE) {
				$r = "ok";
			} else {
				$r = "No";
			}
			return $r;
		}
	}


	public function eliminarTipoCategoriaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_tipo']) &&
			!empty($_POST['id_tipo'])
		) {


			$id_tipo = filter_input(INPUT_POST, 'id_tipo', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloTipoCategorias::eliminarTipoCategoriaModel($id_tipo);

			if ($result == TRUE) {
				$r = "ok";
			} else {
				$r = "No";
			}
			return $r;
		}
	}
}
