<?php
date_default_timezone_get('America/Bogota');
require_once MODELO_PATH . 'categorias' . DS . 'ModeloListadoCategorias.php';

class ControlListadoCategorias
{

    private static $instancia;

    public static function singleton_listado_categorias()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }


    public function mostrarDestinosControl($super_empresa)
    {
        $mostrar = ModeloListadoCategorias::mostrarDestinosModel($super_empresa);
        return $mostrar;
    }

    public function mostrarTratamientosControl($super_empresa)
    {
        $mostrar = ModeloListadoCategorias::mostrarTratamientosModel($super_empresa);
        return $mostrar;
    }

    public function guardarListadoCategoriaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['super_empresa']) &&
            !empty($_POST['super_empresa']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['descripcion']) &&
            !empty($_POST['descripcion']) &&
            isset($_POST['id_tipo']) &&
            !empty($_POST['id_tipo'])
        ) {

            $datos = array(
                'nombre' => $_POST['descripcion'],
                'id_tipo' => $_POST['id_tipo'],
                'nit' => $_POST['nit'],
                'resolucion' => $_POST['resolucion'],
                'user_log' => $_POST['id_log'],
                'super_empresa' => $_POST['super_empresa']
            );

            $guardar = ModeloListadoCategorias::guardarListadoCategoriaModel($datos);

            if ($guardar == TRUE) {
                echo '
			<script>
			ohSnap("Registrado correctamente!", {color: "green", "duration": "1000"});
			setTimeout(recargarPagina,1050);

			function recargarPagina(){
				window.location.replace("index");
			}
			</script>
			';
            } else {
                echo '
			<script>
			ohSnap("Ha ocurrido un error!", {color: "red"});
			</script>
			';
            }
        } else {
        }
    }


    public function editarListadoCategoriaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_edit']) &&
            !empty($_POST['id_edit']) &&
            isset($_POST['descripcion_edit']) &&
            !empty($_POST['descripcion_edit']) &&
            isset($_POST['id_tipo_edit']) &&
            !empty($_POST['id_tipo_edit'])
        ) {


            $datos = array(
                'id_destino' => $_POST['id_edit'],
                'nombre' => $_POST['descripcion_edit'],
                'id_tipo' => $_POST['id_tipo_edit'],
                'nit' => $_POST['nit_edit'],
                'resolucion' => $_POST['resolucion_edit']
            );

            $editar = ModeloListadoCategorias::editarListadoCategoriaModel($datos);

            if ($editar == TRUE) {
                echo '
                    <script>
                    ohSnap("Modificado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("index");
                    }
                    </script>
                    ';
            } else {
                echo '
                    <script>
                    ohSnap("Ha ocurrido un error!", {color: "red"});
                    </script>
                    ';
            }
        } else {
        }
    }




    public function inactivarListadoCategoriaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_listado']) &&
            !empty($_POST['id_listado'])
        ) {


            $id_listado = filter_input(INPUT_POST, 'id_listado', FILTER_SANITIZE_NUMBER_INT);
            $result = ModeloListadoCategorias::inactivarListadoCategoriaModel($id_listado);

            if ($result == TRUE) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }



    public function activarListadoCategoriaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_listado']) &&
            !empty($_POST['id_listado'])
        ) {


            $id_listado = filter_input(INPUT_POST, 'id_listado', FILTER_SANITIZE_NUMBER_INT);
            $result = ModeloListadoCategorias::activarListadoCategoriaModel($id_listado);

            if ($result == TRUE) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }



    public function eliminarListadoCategoriaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_listado']) &&
            !empty($_POST['id_listado'])
        ) {


            $id_listado = filter_input(INPUT_POST, 'id_listado', FILTER_SANITIZE_NUMBER_INT);
            $result = ModeloListadoCategorias::eliminarListadoCategoriaModel($id_listado);

            if ($result == TRUE) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }
}
