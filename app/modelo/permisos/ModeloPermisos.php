<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloPermiso extends conexion
{

    public static function consultarPermisoModel($datos)
    {
        $tabla  = 'eco_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT 1 FROM " . $tabla . " p WHERE
        p.id_user IN(SELECT u.id_user FROM usuarios u WHERE u.id_user = :iu AND u.estado IN('activo'))
        AND p.id_modulo IN(SELECT m.id FROM eco_modulos m WHERE m.id = :im AND m.activo = 1)
        AND p.id_opcion IN(SELECT o.id FROM eco_opciones o WHERE o.id = :io AND o.activo = 1)
        AND p.id_accion IN(SELECT a.id FROM eco_accion a WHERE a.id = :ia AND a.activo = 1) AND p.activo = 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':iu', $datos['id_user']);
            $preparado->bindValue(':ia', $datos['id_accion']);
            $preparado->bindValue(':im', $datos['id_modulo']);
            $preparado->bindValue(':io', $datos['id_opcion']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function datosSuperEmpresaModel($id)
    {
        $tabla  = 'super_empresa';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id = '$id'";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
