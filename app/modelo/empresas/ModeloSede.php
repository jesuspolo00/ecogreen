<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloSede extends conexion
{

    public function agregarSedeModel($datos)
    {
        $tabla = 'sucursales';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, ciudad, direccion, nom_contacto, telefono, nit, id_empresa, tiempo, email, id_user, estado, fechareg, fechaupdate) 
        VALUES (:n,:c,:d,:nc,:t,:nt,:id,:ti,:e,:iu,:es,:f,:fu)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':c', $datos['ciudad']);
            $preparado->bindParam(':d', $datos['direccion']);
            $preparado->bindParam(':nc', $datos['nom_contacto']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':nt', $datos['nit']);
            $preparado->bindParam(':id', $datos['id_empresa']);
            $preparado->bindParam(':ti', $datos['tiempo']);
            $preparado->bindParam(':e', $datos['email']);
            $preparado->bindParam(':iu', $datos['id_user']);
            $preparado->bindValue(':es', 'activo');
            $preparado->bindParam(':f', $datos['fechareg']);
            $preparado->bindParam(':fu', $datos['fechaupdate']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function editarSedeModelo($datos)
    {
        $tabla = 'sucursales';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET nombre = :n, ciudad = :c, direccion = :d, nom_contacto = :nc, telefono = :t, nit = :nt, tiempo = :ti, email = :e, id_user = :iu, fechaupdate = :fu WHERE id_sucursal = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':c', $datos['ciudad']);
            $preparado->bindParam(':d', $datos['direccion']);
            $preparado->bindParam(':nc', $datos['nom_contacto']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':nt', $datos['nit']);
            $preparado->bindParam(':id', $datos['id_empresa']);
            $preparado->bindParam(':ti', $datos['tiempo']);
            $preparado->bindParam(':e', $datos['email']);
            $preparado->bindParam(':iu', $datos['id_user']);
            $preparado->bindParam(':fu', $datos['fechaupdate']);
            $preparado->bindValue(':id', (int) trim($datos['id_sede']), PDO::PARAM_INT);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarSedesModel($super_empresa)
    {
        $tabla = 'sucursales';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_empresa IN(SELECT e.id_empresa FROM empresas e WHERE e.id_super_empresa = :is)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarSedeEmpresaIdModel($id)
    {
        $tabla = 'empresas';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_empresa = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarNitEmpresaModel($id)
    {
        $tabla = 'empresas';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT nit FROM " . $tabla . " WHERE id_empresa = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function EliminarSedeModel($id)
    {
        $tabla = 'sucursales';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id_sucursal = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function InactivarSedeModel($id)
    {
        $tabla = 'sucursales';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 'inactivo' WHERE id_sucursal = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarSucursalesEmpresaModel($id)
    {
        $tabla = 'sucursales';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT s.id_sucursal, s.nombre, s.ciudad, (SELECT e.nit FROM empresas e WHERE e.id_empresa IN(s.id_empresa)) AS nit FROM " . $tabla . " s WHERE s.id_empresa = :id AND estado IN('activo')";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }





    public function activarSedeModel($id)
    {
        $tabla = 'sucursales';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 'activo' WHERE id_sucursal = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function MostrarSedesEmpresa($id)
    {
        $tabla = 'sucursales';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_empresa = :id AND estado IN('activo')";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarSedeIdModel($id)
    {
        $tabla = 'sucursales';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " s WHERE s.id_sucursal = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
