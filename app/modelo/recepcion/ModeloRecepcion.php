<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloRecepcion extends conexion
{

	public function guardarRecepcionModel($datos)
	{
		$tabla = 'reservas';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "INSERT INTO " . $tabla . " (id_user, id_vehiculo, id_conductor, id_coopiloto, id_empresa, id_sucursal, hora_inicio, hora_fin, fecha_apartado, observacion, fechareg, confirmado, recepcion) 
		VALUES (:id,:v,:c,:co,:e,:is,:hi,:hf,:fa,:ob,:fr,:con,:r)";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindParam(':id', $datos['id_user']);
			$preparado->bindParam(':v', $datos['id_vehiculo']);
			$preparado->bindParam(':c', $datos['id_conductor']);
			$preparado->bindParam(':co', $datos['id_coopiloto']);
			$preparado->bindParam(':e', $datos['id_empresa']);
			$preparado->bindParam(':is', $datos['id_sucursal']);
			$preparado->bindParam(':hi', $datos['hora_inicio']);
			$preparado->bindParam(':hf', $datos['hora_fin']);
			$preparado->bindParam(':fa', $datos['fecha_apartado']);
			$preparado->bindParam(':ob', $datos['observacion']);
			$preparado->bindParam(':fr', $datos['fechareg']);
			$preparado->bindValue(':con', 'si');
			$preparado->bindValue(':r', 1);
			if ($preparado->execute()) {
				$id = $cnx->ultimoIngreso($tabla);
				$resultado = array('guardar' => TRUE, 'id' => $id);
				return $resultado;
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}


	public function guardarResiduosRecepcionModel($datos)
	{
	$tabla = 'apartados_residuos';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "INSERT INTO " . $tabla . " (id_apartado, id_residuo, id_tipo_residuo, cantidad, fechareg) 
		VALUES (:id,:ir,:it,:ca,:fr)";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindParam(':id', $datos['id_apartado']);
			$preparado->bindParam(':ir', $datos['id_residuo']);
			$preparado->bindParam(':it', $datos['id_tipo_residuo']);
			$preparado->bindParam(':ca', $datos['cantidad']);
			$preparado->bindParam(':fr', $datos['fechareg']);
			if ($preparado->execute()) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}



	public function guardarRecepcionDatosModel($datos)
	{
		$tabla = 'recepcion';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "INSERT INTO " . $tabla . " (id_apartado, placa_vehiculo, doc_conductor, nom_conductor, nom_coopiloto, user_log, id_super_empresa, fechareg) 
		VALUES (:id,:p,:dc,:nc,:coo,:ul,:is,:fr)";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindParam(':id', $datos['id_apartado']);
			$preparado->bindParam(':p', $datos['vehiculo']);
			$preparado->bindParam(':dc', $datos['documento']);
			$preparado->bindParam(':nc', $datos['conductor']);
			$preparado->bindParam(':coo', $datos['coopiloto']);
			$preparado->bindParam(':ul', $datos['user_log']);
			$preparado->bindParam(':is', $datos['super_empresa']);
			$preparado->bindParam(':fr', $datos['fechareg']);
			if ($preparado->execute()) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}


	public function mostrarDatosRecepcionModel($id,$super_empresa)
	{
		$tabla = 'recepcion';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "SELECT * FROM " . $tabla . " WHERE id_apartado = :id AND id_super_empresa = :is";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
			$preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return $preparado->fetch();
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}
}
