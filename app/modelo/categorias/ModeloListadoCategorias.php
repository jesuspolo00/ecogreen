<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloListadoCategorias extends conexion
{

    public function guardarListadoCategoriaModel($datos)
    {
        $tabla = 'td_listado';
        $cnx = conexion::singleton_conexion();
        $sql = "INSERT INTO " . $tabla . " (nombre,nit,resolucion,id_td_tipo,id_log,id_super_empresa) 
        VALUES (:n, :nt, :r, :it, :ul, :is)";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':nt', $datos['nit']);
            $preparado->bindParam(':r', $datos['resolucion']);
            $preparado->bindParam(':it', $datos['id_tipo']);
            $preparado->bindParam(':ul', $datos['user_log']);
            $preparado->bindParam(':is', $datos['super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function editarListadoCategoriaModel($datos)
    {
        $tabla = 'td_listado';
        $cnx = conexion::singleton_conexion();
        $sql = "UPDATE " . $tabla . " SET nombre = :n, nit = :nt, resolucion = :r, id_td_tipo = :it WHERE id = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':nt', $datos['nit']);
            $preparado->bindParam(':r', $datos['resolucion']);
            $preparado->bindParam(':it', $datos['id_tipo']);
            $preparado->bindValue(':id', (int) trim($datos['id_destino']), PDO::PARAM_INT);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }




    public function mostrarDestinosModel($super_empresa)
    {
        $tabla = 'td_listado';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_super_empresa = :is AND id_td_tipo IN(SELECT t.id FROM td_tipo t WHERE t.id_categoria = 2)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarTratamientosModel($super_empresa)
    {
        $tabla = 'td_listado';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_super_empresa = :is AND id_td_tipo IN(SELECT t.id FROM td_tipo t WHERE t.id_categoria = 1)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function eliminarListadoCategoriaModel($id)
    {
        $tabla = 'td_listado';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }




    public function activarListadoCategoriaModel($id)
    {
        $tabla = 'td_listado';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 1 WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function inactivarListadoCategoriaModel($id)
    {
        $tabla = 'td_listado';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
