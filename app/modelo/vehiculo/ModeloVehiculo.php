<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloVehiculo extends conexion
{

    public static function registrarVehiculoModel($datos)
    {
        $tabla = 'inventario';
        $cnx   = conexion::singleton_conexion();
        $sql   = "INSERT INTO " . $tabla . " (descripcion, placa, marca, modelo, id_tipo, id_user, id_log, fechareg, estado, estado_vehiculo, id_super_empresa)
        VALUES (:d, :n, :a, :c, :t, :u, :il, :r, :e, :ev, :ids);";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':d', $datos['descripcion']);
            $preparado->bindParam(':n', $datos['placa']);
            $preparado->bindParam(':a', $datos['marca']);
            $preparado->bindParam(':c', $datos['modelo']);
            $preparado->bindParam(':t', $datos['id_tipo']);
            $preparado->bindParam(':u', $datos['id_user']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':r', $datos['fechareg']);
            $preparado->bindValue(':e', 5);
            $preparado->bindValue(':ev', 'activo');
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarVehiculoModelo($datos)
    {
        $tabla = 'inventario';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET descripcion = :d, placa = :p, marca = :m, modelo = :mo, id_log = :il WHERE id_inventario = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':d', $datos['descripcion']);
            $preparado->bindParam(':p', $datos['placa']);
            $preparado->bindParam(':m', $datos['marca']);
            $preparado->bindParam(':mo', $datos['modelo']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindValue(':id', (int) trim($datos['id_vehiculo']), PDO::PARAM_INT);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function reportarVehiculoModelo($datos)
    {
        $tabla = 'reportes';
        $cnx   = conexion::singleton_conexion();
        $sql   = "INSERT INTO " . $tabla . " (id_inventario, tipo_mant, observacion, fecha_reportado, id_log, id_super_empresa, estado)
        VALUES (:id, :t, :ob, :fr, :il, :is, :e)";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':id', $datos['id_inventario']);
            $preparado->bindParam(':t', $datos['tipo_mant']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindValue(':fr', $datos['fecha_reportado']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':is', $datos['super_empresa']);
            $preparado->bindValue(':e', 'abierto');
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarEstadoVehiculoModelo($datos)
    {
        $tabla = 'inventario';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET estado = :e WHERE id_inventario = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':e', $datos['tipo_mant']);
            $preparado->bindValue(':id', (int) trim($datos['id_inventario']), PDO::PARAM_INT);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('editar' => true, 'id' => $datos['id_inventario']);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosVehiculosModel($super_empresa)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_super_empresa = :is";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosVehiculosIdModel($id)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_inventario = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarVehiculoModelo($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado_vehiculo = 'inactivo', fecha_inactivo = :fi WHERE id_inventario = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($datos['id_vehiculo']), PDO::PARAM_INT);
            $preparado->bindParam(':fi', $datos['fecha_inactivo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarVehiculoModelo($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado_vehiculo = 'activo', fecha_activo = :fi WHERE id_inventario = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($datos['id_vehiculo']), PDO::PARAM_INT);
            $preparado->bindParam(':fi', $datos['fecha_activo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function eliminarVehiculoModelo($id)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id_inventario = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
