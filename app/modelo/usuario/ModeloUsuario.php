<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloUsuario extends conexion
{

    public static function registrarUsuarioModel($datos)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "INSERT INTO " . $tabla . " (documento, nombre, apellido, correo, telefono, user, pass, perfil, estado, user_log, id_super_empresa, fechareg, fecha_activo)
        VALUES (:d,:n,:a,:c,:t,:u,:p,:r,:e,:ul,:is,:fr,:fa)";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':d', $datos['documento']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':a', $datos['apellido']);
            $preparado->bindParam(':c', $datos['correo']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':u', $datos['usuario']);
            $preparado->bindParam(':p', $datos['pass']);
            $preparado->bindParam(':r', $datos['perfil']);
            $preparado->bindValue(':e', 'activo');
            $preparado->bindParam(':ul', $datos['id_log']);
            $preparado->bindParam(':is', $datos['super_empresa']);
            $preparado->bindParam(':fr', $datos['fechareg']);
            $preparado->bindParam(':fa', $datos['fecha_activo']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarUsuarioModelo($datos)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET nombre = :n, apellido = :a, telefono = :t, pass = :p, perfil = :r WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':a', $datos['apellido']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':p', $datos['pass']);
            $preparado->bindParam(':r', $datos['perfil']);
            $preparado->bindValue(':id', (int) trim($datos['id_user']), PDO::PARAM_INT);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('editar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosUsuariosModel($super_empresa)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_super_empresa = :is";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosUsuariosIdModel($id)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            //$preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function contarUsuariosActivosModelo($super_empresa)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT count(id_user) as id FROM " . $tabla . " WHERE estado IN('activo') AND perfil NOT IN(4)
        AND id_super_empresa = :is";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function contarUsuariosInactivosModelo($super_empresa)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT count(id_user) as id FROM " . $tabla . " WHERE estado IN('inactivo') AND perfil NOT IN(4)
        AND id_super_empresa = :is";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function contarUsuariosTotalesModelo($super_empresa)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT count(id_user) as id FROM " . $tabla . " WHERE perfil NOT IN(4) AND id_super_empresa = :is";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verififcarUsuarioModelo($dato)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT user FROM " . $tabla . " WHERE user IN('" . $dato . "')";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarDocumentoModelo($dato)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT documento FROM " . $tabla . " WHERE documento IN('" . $dato . "')";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarUsuarioModelo($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 'inactivo', fecha_inactivo = :fi WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':fi', $datos['fecha_inactivo']);
            $preparado->bindValue(':id', (int) trim($datos['id_user']), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarUsuarioModelo($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 'activo', fecha_activo = :fa WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':fa', $datos['fecha_activo']);
            $preparado->bindValue(':id', (int) trim($datos['id_user']), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function eliminarUsuarioModelo($id)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
