<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloApartar extends conexion
{

	public function guardarApartadoModel($datos)
	{
		$tabla = 'reservas';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "INSERT INTO " . $tabla . " (id_user, id_vehiculo, id_conductor, id_coopiloto, id_empresa, id_sucursal, hora_inicio, hora_fin, fecha_apartado, observacion, fechareg, confirmado, recepcion) VALUES (:id,:v,:c,:co,:e,:is,:hi,:hf,:fa,:ob,:fr,:con,:r)";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindParam(':id', $datos['id_user']);
			$preparado->bindParam(':v', $datos['id_vehiculo']);
			$preparado->bindParam(':c', $datos['id_conductor']);
			$preparado->bindParam(':co', $datos['id_coopiloto']);
			$preparado->bindParam(':e', $datos['id_empresa']);
			$preparado->bindParam(':is', $datos['id_sucursal']);
			$preparado->bindParam(':hi', $datos['hora_inicio']);
			$preparado->bindParam(':hf', $datos['hora_fin']);
			$preparado->bindParam(':fa', $datos['fecha_apartado']);
			$preparado->bindParam(':ob', $datos['observacion']);
			$preparado->bindParam(':fr', $datos['fechareg']);
			$preparado->bindValue(':con', 'no');
			$preparado->bindValue(':r', 0);
			if ($preparado->execute()) {
				$id = $cnx->ultimoIngreso($tabla);
				$resultado = array('guardar' => TRUE, 'id' => $id);
				return $resultado;
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}




	public function confirmarReservaModel($datos)
	{
		$tabla = 'reservas';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "UPDATE " . $tabla . " SET id_user= :id,id_empresa= :e,id_sucursal= :is,hora_fin= :hf,observacion= :ob,confirmado= :con WHERE id_reserva = :ir";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindParam(':ir', $datos['id_reserva']);
			$preparado->bindParam(':id', $datos['id_user']);
			$preparado->bindParam(':e', $datos['id_empresa']);
			$preparado->bindParam(':is', $datos['id_sucursal']);
			$preparado->bindParam(':hf', $datos['hora_fin']);
			$preparado->bindParam(':ob', $datos['observacion']);
			$preparado->bindValue(':con', 'si');
			if ($preparado->execute()) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}


	public function confirmadoReservaModel($id)
	{
		$tabla = 'reservas';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "SELECT confirmado FROM " . $tabla . " WHERE id_reserva = :id AND confirmado IN('si');";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return $preparado->fetch();
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}



	public function mostrarApartadosFechaModel($datos)
	{
		$tabla = 'reservas';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "SELECT count(id_reserva) as id, max(hora_inicio) as hora_inicio,max(hora_fin) as hora_fin FROM " . $tabla . " WHERE fecha_apartado = :f AND id_vehiculo = :id AND recepcion = 0";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindParam(':f', $datos['fecha']);
			$preparado->bindValue(':id', (int) trim($datos['id_vehiculo']), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return $preparado->fetch();
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}



	public function guardarResiduosApartarModel($datos)
	{
		$tabla = 'apartados_residuos';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "INSERT INTO " . $tabla . " (id_apartado, id_residuo, id_tipo_residuo, cantidad, id_log, fechareg) VALUES (:id,:ir,:it,:ca,:il,:fr)";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindParam(':id', $datos['id_apartado']);
			$preparado->bindParam(':ir', $datos['id_residuo']);
			$preparado->bindParam(':it', $datos['id_tipo_residuo']);
			$preparado->bindParam(':ca', $datos['cantidad']);
			$preparado->bindParam(':il', $datos['id_log']);
			$preparado->bindParam(':fr', $datos['fechareg']);
			if ($preparado->execute()) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}


	public function apartadoResiduosLogModel($datos)
	{
		$tabla = 'apartados_residuos_log';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "INSERT INTO " . $tabla . " (id_apartado, id_residuo, id_tipo_residuo, cantidad, id_log, fechareg) VALUES (:id,:ir,:it,:ca,:il,:fr)";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindParam(':id', $datos['id_apartado']);
			$preparado->bindParam(':ir', $datos['id_residuo']);
			$preparado->bindParam(':it', $datos['id_tipo_residuo']);
			$preparado->bindParam(':ca', $datos['cantidad']);
			$preparado->bindParam(':il', $datos['id_log']);
			$preparado->bindParam(':fr', $datos['fechareg']);
			if ($preparado->execute()) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}


	public function mostrarReservasEmpresaIdModel($id)
	{
		$tabla = 'reservas';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "SELECT 
		IF(r.id_sucursal = 0, (SELECT e.nombre FROM empresas e WHERE e.id_empresa IN(r.id_empresa)),
		(SELECT e.nombre FROM sucursales e WHERE e.id_sucursal IN(r.id_sucursal))) AS empresa,
		IF(r.id_sucursal = 0, (SELECT e.direccion FROM empresas e WHERE e.id_empresa IN(r.id_empresa)),
		(SELECT e.direccion FROM sucursales e WHERE e.id_sucursal IN(r.id_sucursal))) AS direccion,
		IF(r.id_sucursal = 0, (SELECT e.ciudad FROM empresas e WHERE e.id_empresa IN(r.id_empresa)),
		(SELECT e.ciudad FROM sucursales e WHERE e.id_sucursal IN(r.id_sucursal))) AS ciudad,
		(SELECT v.placa FROM inventario v WHERE v.id_inventario IN(r.id_vehiculo)) AS vehiculo,
		(SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(r.id_conductor)) AS conductor,
		(SELECT pr.id FROM prefacturacion pr WHERE pr.id_reserva IN(r.id_reserva) ORDER BY pr.id DESC LIMIT 1) AS prefacturado,
		(SELECT s.estado FROM solicitud_certificados s WHERE s.id_reserva IN(r.id_reserva)) AS certificado,
		r.id_reserva,
		r.id_empresa,
		r.id_sucursal,
		r.recepcion,
		r.fecha_apartado
		FROM " . $tabla . " r WHERE r.id_empresa = :id;";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
			if ($preparado->execute()) {
				return $preparado->fetchAll();
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}



	public function mostrarRecepcionEmpresaIdModel($id)
	{
		$tabla = 'reservas';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "SELECT * FROM " . $tabla . " WHERE id_empresa = :id AND recepcion <> 0";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
			if ($preparado->execute()) {
				return $preparado->fetchAll();
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}


	public function eliminarResiduosApartadoModel($id)
	{
		$tabla = 'apartados_residuos';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "DELETE FROM " . $tabla . " WHERE id_apartado = :id";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}



	public function guardarDocumentosModel($datos)
	{
		$tabla = 'documentos';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "INSERT INTO " . $tabla . " (nombre, id_user, id_log, id_apartado, tipo, fechareg) VALUES (:n,:iu,:il,:ia,:t,:fr)";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindParam(':n', $datos['nombre']);
			$preparado->bindParam(':iu', $datos['id_user']);
			$preparado->bindParam(':il', $datos['id_log']);
			$preparado->bindParam(':ia', $datos['id_apartado']);
			$preparado->bindParam(':t', $datos['tipo']);
			$preparado->bindParam(':fr', $datos['fechareg']);
			if ($preparado->execute()) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}




	public function mostrarArchivosApartadoModel($id)
	{
		$tabla = 'documentos';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "SELECT * FROM " . $tabla . " WHERE id_apartado = :id";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return $preparado->fetchAll();
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}
}
